package com.mobapply.taxitter.interfaces;

import com.mobapply.taxitter.model.Order;

public interface DetailsOrderModuleListener {
    public void openDetailsOrder(Order item);
}