package com.mobapply.taxitter.interfaces;

import se.walkercrou.places.Place;

public interface CountCoordinatesListener {
    public void onCountCoordinatesListener(Place addresses);
}