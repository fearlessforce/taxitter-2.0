package com.mobapply.taxitter.interfaces;

public interface SwipeButtonClickListener {
    public void onAcceptClickButton(int position);
    public void onRejectClickButton(int position);
}
