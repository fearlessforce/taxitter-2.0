package com.mobapply.taxitter.interfaces;

public interface ResponseListener {
    public void onResponceCode(int code);
    public void onResponceMessage(String str);
}