package com.mobapply.taxitter.interfaces;

public interface TransmissionResponseCode {
    public void onMessageSuccess(String str);
    public void onMessageError(int errorCode);
}