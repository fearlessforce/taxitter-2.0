package com.mobapply.taxitter.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * It's item for ArrayList orders
 *
 * @see com.mobapply.taxitter.ui.passenger.ListOfOwnOrders
 * @see com.mobapply.taxitter.ui.driver.ListOrdersFragment
 */
public class Order {
    private ArrayList<AdditionalRoute> additionalRoutes;
    private String order_uid;
    private String when;
    private String latitude;
    private String longitude;
    private String departure;
    private String cash;
    private String cash_d;
    private String comment;
    private String comment_d;
    private String length;
    private String status_offer;
    private String id;
    private String number;
    private String client;
    private String uid_client;
    private int babyseat;
    private int ac;
    private int quantity;
    private int nonsmoking;
    private int rating;

    public Order(JSONObject object) {
        additionalRoutes = new ArrayList<AdditionalRoute>();
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.yyyy, HH:mm");

        try {
            order_uid = object.getString("order_uid").equals("") ? "" : object.getString("order_uid");
            number = object.getString("number");
            when = object.getString("when").equals("") ? "" : timeFormat.format(new Date(Math.abs(object.getLong("when")*1000)));
            rating = object.getInt("rating");
            id = object.getString("id");
            latitude = object.getString("latitude").equals("") ? "" : object.getString("latitude");
            longitude = object.getString("longitude").equals("") ? "" : object.getString("longitude");
            departure = object.getString("departure").equals("") ? "" : object.getString("departure");
            cash = object.getString("cash").equals("") ? "" : object.getString("cash");
            cash_d = object.getString("cash_d").equals("") ? "" : object.getString("cash_d");
            babyseat = object.getInt("babyseat");
            ac = object.getInt("ac");
            quantity = object.getInt("quantity");
            comment = object.getString("comment").equals("") ? "" : object.getString("comment");
            comment_d = object.getString("comment_d").equals("") ? "" : object.getString("comment_d");
            length = object.getString("length").equals("") ? "" : object.getString("length");
            nonsmoking = object.getInt("nonsmoking");
            status_offer = object.getString("status_offer").equals("") ? "" : object.getString("status_offer");
            client = object.getString("client").equals("") ? "" : object.getString("client");
            uid_client = object.getString("uid_client").equals("") ? "" : object.getString("uid_client");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = object.getJSONArray("waypoints");

            if (jsonArray!= null) {
                for (int n = 0; n < jsonArray.length(); n++) {
                    JSONObject objectDots = jsonArray.getJSONObject(n);
                    AdditionalRoute additionalRoute = new AdditionalRoute(objectDots);
                    additionalRoutes.add(additionalRoute);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getUid_client() {
        return uid_client;
    }

    public String getClient() {
        return client;
    }

    public String getCash_d() {
        return cash_d;
    }

    public String getStatus_offer() {
        return status_offer;
    }

    public String getComment_d() {
        return comment_d;
    }

    public String getStatusOffer() {
        return status_offer;
    }

    public int getRating() {
        return rating;
    }

    public int getNonsmoking() {
        return nonsmoking;
    }

    public String getLength() {
        return length;
    }

    public String getComment() {
        return comment;
    }

    public String getOrder_uid() {
        return order_uid;
    }

    public String getNumber() {
        return number;
    }

    public String getWhen() {
        return when;
    }

    public String getId() {
        return id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String  getLongitude() {
        return longitude;
    }

    public String getDeparture() {
        return departure;
    }

    public String getCash() {
        return cash;
    }

    public int getBabyseat() {
        return babyseat;
    }

    public int getAc() {
        return ac;
    }

    public int getQuantity() {
        return quantity;
    }

    public ArrayList<AdditionalRoute> getAdditionalRoutes() {
        return additionalRoutes;
    }
}