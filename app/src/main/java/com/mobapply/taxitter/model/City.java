package com.mobapply.taxitter.model;

import com.mobapply.taxitter.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Misha Nester on 08.06.15.
 */
public class City {
    private String region;
    private String cityName;
    private String mincash;

    public City(JSONObject jsObject) {
        try {
            region = jsObject.getString(Constants.REGION);
            cityName = jsObject.getString(Constants.NAME);
            mincash = jsObject.getString(Constants.MINCASHE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public  City() {}

    public String getCityAddress() {
        return region + ", " + cityName;
    }


    public String getMincash() {
        return mincash;
    }

    public String getCityName() {
        return cityName;
    }

    public String getRegion() {
        return region;
    }

    public void setNameCity(String string) {
        cityName = string;
    }
}
