package com.mobapply.taxitter.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * It's item for ArrayList passenger proposals
 *
 * @see com.mobapply.taxitter.ui.passenger.ListOfOwnOrders
 */

public class Proposal {
    private ArrayList<AdditionalRoute> additionalRoutes;
    private ArrayList<Driver> itemOfferDrivers;
    private String order_uid;
    private String when;
    private String departure;
    private String latitude;
    private String longitude;
    private String city;
    private String cash;
    private String comment;
    private int babyseat;
    private int nonsmoking;
    private int ac;
    private int quantity;
    private int status;
    private String length;

    public Proposal(JSONObject object) {
        additionalRoutes = new ArrayList<AdditionalRoute>();
        itemOfferDrivers = new ArrayList<Driver>();

        try {
            order_uid = object.getString("order_uid");
            when = object.getString("when");
            departure = object.getString("departure");
            latitude = object.getString("latitude");
            longitude = object.getString("longitude");
            city = object.getString("city");
            cash = object.getString("cash");
            comment = object.getString("comment");
            babyseat = object.getInt("babyseat");
            ac = object.getInt("ac");
            nonsmoking = object.getInt("nonsmoking");
            quantity = object.getInt("quantity");
            status = object.getInt("status");
            length = object.getString("length");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (object.has("waypoints")) {
                JSONArray jsonArray = object.getJSONArray("waypoints");

                if (jsonArray != null) {
                    for (int n = 0; n < jsonArray.length(); n++) {
                        JSONObject objectDots = jsonArray.getJSONObject(n);
                        AdditionalRoute additionalRoute = new AdditionalRoute(objectDots);
                        additionalRoutes.add(additionalRoute);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (object.has("offers")) {
                JSONArray jsonArray = object.getJSONArray("offers");

                if (jsonArray != null) {
                    for (int n = 0; n < jsonArray.length(); n++) {
                        JSONObject objectOffers = jsonArray.getJSONObject(n);
                        Driver itemOfferDriver = new Driver(objectOffers);
                        itemOfferDrivers.add(itemOfferDriver);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getComment() {
        return comment;
    }

    public int getNonsmoking() {
        return nonsmoking;
    }

    public ArrayList<Driver> getItemOfferDrivers() {
        return itemOfferDrivers;
    }

    public String getCity() {
        return city;
    }

    public int getStatus() {
        return status;
    }

    public String getLength() {
        return length;
    }

    public String getOrder_uid() {
        return order_uid;
    }

    public String getWhen() {
        return when;
    }

    public String getLatitude() {
        return latitude;
    }

    public String  getLongitude() {
        return longitude;
    }

    public String getDeparture() {
        return departure;
    }

    public String getCash() {
        return cash;
    }

    public int getBabyseat() {
        return babyseat;
    }

    public int getAc() {
        return ac;
    }

    public int getQuantity() {
        return quantity;
    }

    public ArrayList<AdditionalRoute> getAdditionalRoutes() {
        return additionalRoutes;
    }
}