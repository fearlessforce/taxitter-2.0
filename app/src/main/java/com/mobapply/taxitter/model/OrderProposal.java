package com.mobapply.taxitter.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * It's item for ArrayList orders
 *
 * @see com.mobapply.taxitter.ui.driver.ListOrdersOnTabTwoFragment
 */

public class OrderProposal {
    private ArrayList<AdditionalRoute> additionalRoutes;
    private String order_uid;
    private String when;
    private String latitude;
    private String longitude;
    private String departure;
    private String cash;
    private String comment;
    private String comment_d;
    private String length;
    private String status_offer;
    private int id;
    private int number;
    private int babyseat;
    private int ac;
    private int quantity;
    private int nonsmoking;
    private int rating;

    public OrderProposal(JSONObject object) {
        additionalRoutes = new ArrayList<AdditionalRoute>();

        try {
            order_uid = object.getString("order_uid");
            number = object.getInt("number");
            when = object.getString("when");
            rating = object.getInt("rating");
            id = object.getInt("id");
            latitude = object.getString("latitude");
            longitude = object.getString("longitude");
            departure = object.getString("departure");
            cash = object.getString("cash");
            babyseat = object.getInt("babyseat");
            ac = object.getInt("ac");
            quantity = object.getInt("quantity");
            comment = object.getString("comment");
            comment_d = object.getString("comment_d");
            length = object.getString("length");
            nonsmoking = object.getInt("nonsmoking");
            status_offer = object.getString("status_offer");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = object.getJSONArray("waypoints");

            if (jsonArray!= null) {
                for (int n = 0; n < jsonArray.length(); n++) {
                    JSONObject objectDots = jsonArray.getJSONObject(n);
                    AdditionalRoute additionalRoute = new AdditionalRoute(objectDots);
                    additionalRoutes.add(additionalRoute);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getComment_d() {
        return comment_d;
    }

    public String getStatusOffer() {
        return status_offer;
    }

    public int getRating() {
        return rating;
    }

    public int getNonsmoking() {
        return nonsmoking;
    }

    public String getLength() {
        return length;
    }

    public String getComment() {
        return comment;
    }

    public String getOrder_uid() {
        return order_uid;
    }

    public int getNumber() {
        return number;
    }

    public String getWhen() {
        return when;
    }

    public int getId() {
        return id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String  getLongitude() {
        return longitude;
    }

    public String getDeparture() {
        return departure;
    }

    public String getCash() {
        return cash;
    }

    public int getBabyseat() {
        return babyseat;
    }

    public int getAc() {
        return ac;
    }

    public int getQuantity() {
        return quantity;
    }

    public ArrayList<AdditionalRoute> getAdditionalRoutes() {
        return additionalRoutes;
    }
}