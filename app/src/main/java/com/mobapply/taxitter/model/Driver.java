package com.mobapply.taxitter.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Item ArrayList<ItemproposalList> that contain data about offer drivers.
 *
 * @see Proposal
 */
public class Driver {
    private String uid_d;
    private String status_d;
    private String cash_d;
    private String id_d;
    private String carnumber_d;
    private String carmodel_d;
    private String carcolor_d;
    private String car_d;
    private String caryear_d;
    private String cartype_d;
    private String comment_d;
    private String rating_d;
    private String nickname_d;
    private String distance_d;
    private String trips_d;
    private int babyseat_d;
    private int ac_d;
    private int nonsmoking_d;

    public Driver(JSONObject object) {
        try {
            uid_d = object.getString("uid_d");
            status_d = object.getString("status_d");
            cash_d = object.getString("cash_d");
            id_d = object.getString("id_d");
            car_d = object.getString("car_d");
            caryear_d = object.getString("caryear_d");
            cartype_d = object.getString("cartype_d");
            comment_d = object.getString("comment_d");
            carnumber_d = object.getString("carnumber_d");
            carmodel_d = object.getString("carmodel_d");
            carcolor_d = object.getString("carcolor_d");
            distance_d = object.getString("distance_d");
            babyseat_d = object.getInt("babyseat_d");
            ac_d = object.getInt("ac_d");
            nonsmoking_d = object.getInt("nonsmoking_d");
            rating_d = object.getString("rating_d");
            nickname_d = object.getString("nickname_d");
            trips_d = object.getString("trips_d");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTrips_d() {
        return trips_d;
    }

    public String getDistance_d() {
        return distance_d;
    }

    public String getNickname_d() {
        return nickname_d;
    }

    public String getRating_d() {
        return rating_d;
    }

    public int getNonsmoking_d() {
        return nonsmoking_d;
    }

    public String getCar_d() {
        return car_d;
    }

    public String getCaryear_d() {
        return caryear_d;
    }

    public String getCartype_d() {
        return cartype_d;
    }

    public String getComment_d() {
        return comment_d;
    }

    public String getUid_d() {
        return uid_d;
    }

    public String getStatus_d() {
        return status_d;
    }

    public String getCash_d() {
        return cash_d;
    }

    public String getId_d() {
        return id_d;
    }

    public String getCarnumber_d() {
        return carnumber_d;
    }

    public String getCarmodel_d() {
        return carmodel_d;
    }

    public String getCarcolor_d() {
        return carcolor_d;
    }

    public int getBabyseat_d() {
        return babyseat_d;
    }

    public int getAc_d() {
        return ac_d;
    }
}