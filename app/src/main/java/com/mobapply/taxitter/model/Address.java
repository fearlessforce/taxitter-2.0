package com.mobapply.taxitter.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by Misha Nester on 27.04.15.
 */
public class Address implements Parcelable {
    private int id = 0;
    private String street = "";
    private String house = "";
    private String door = "";
    private String fullAddress = "";
    private String halfAddress = "";
    private double longitude = 0;
    private double latitude = 0;
    private int isFavorite = 0;
    private int isEdited;
    private int isObject;

    public Address() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(street);
        dest.writeString(house);
        dest.writeString(door);
        dest.writeString(fullAddress);
        dest.writeString(halfAddress);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
        dest.writeInt(isFavorite);
        dest.writeInt(isEdited);
        dest.writeInt(isObject);
        dest.writeInt(id);
    }

    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Address> CREATOR
            = new Parcelable.Creator<Address>() {
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    private Address(Parcel in) {
        street = in.readString();
        house = in.readString();
        door = in.readString();
        fullAddress = in.readString();
        halfAddress = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
        isFavorite = in.readInt();
        isEdited = in.readInt();
        isObject = in.readInt();
        id = in.readInt();
    }

    public int isEdited() {
        return isEdited;
    }

    public void setEdited(int isEdited) {
        this.isEdited = isEdited;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsFavorite() {
        return isFavorite;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getDoor() {
        return door;
    }

    public void setDoor(String door) {
        this.door = door;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int isFavorite() {
        return isFavorite;
    }

    public int getIsObject() {
        return isObject;
    }

    public void setIsObject(int isObject) {
        this.isObject = isObject;
    }

    public void setFavorite(int isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getHalfAddress() {
        return halfAddress;
    }

    public String  getHalfAddressForServer(){
        String result = null;
        if (isObject == 1) {
            String str = null;
            String entrance = null;
            if (TextUtils.isEmpty(door)) {
                str = street.split("\\(")[1].replace(")", "").trim();
                entrance = street.split("\\(")[0].trim();
            } else {
                str = street;
                entrance = door;
            }
            result = str + ", " + house + " (" + entrance + ")";
        } else{
            result = getHalfAddress();
        }

        return result;
    }

    public void setHalfAddress(String halfAddress) {
        this.halfAddress = halfAddress;
    }
}