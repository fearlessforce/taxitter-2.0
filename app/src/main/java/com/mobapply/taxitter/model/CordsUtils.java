package com.mobapply.taxitter.model;

import java.io.Serializable;

/**
 * Container for adress array item
 */
public class CordsUtils implements Serializable {
    public int id;
    public double latitude;
    public double longitude;
    public String address;

    public CordsUtils() {
        super();
    }

    public void setLatitude(double lat) {
        this.latitude = lat;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }
}