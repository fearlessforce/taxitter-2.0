package com.mobapply.taxitter.model;

import java.util.Collection;

/**
 * Created by apelipets on 10/26/15.
 */
public class AddressComponent extends se.walkercrou.places.AddressComponent {

    public AddressComponent(){ }

    @Override
    public se.walkercrou.places.AddressComponent setLongName(String longName) {
        return super.setLongName(longName);
    }

    @Override
    public se.walkercrou.places.AddressComponent setShortName(String shortName) {
        return super.setShortName(shortName);
    }

    @Override
    public se.walkercrou.places.AddressComponent addType(String type) {
        return super.addType(type);
    }

    @Override
    public se.walkercrou.places.AddressComponent addTypes(Collection<String> types) {
        return super.addTypes(types);
    }
}
