package com.mobapply.taxitter.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Misha Nester on 05.05.15.
 */
public class FeedBackItem {
    private String date;
    private String user;
    private String rating;
    private String avatar;
    private String feedback;

    public FeedBackItem(JSONObject jsObject) {
        try {
            date = jsObject.getString("date");
            user = jsObject.getString("user");
            rating = jsObject.getString("rating");
            avatar = jsObject.getString("avatar");
            feedback = jsObject.getString("feedback");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getFeedback() {
        return feedback;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public String getRating() {
        return rating;
    }
}
