package com.mobapply.taxitter.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Misha Nester on 23.04.15.
 */
public class FavoriteAddress implements Parcelable {
    private String id;
    private String name;
    private String street;
    private String house_num;
    private String door;
    private String longitude;
    private String latitude;
    private String halfAddress;
    private String fullAddress;
    private int isEditing;

    public FavoriteAddress (JSONObject object) {
        try {
            id = object.getString("id");
            name = object.getString("name");
            street = object.getString("street");
            house_num = object.getString("house_num");
            door = object.getString("door");
            longitude = object.getString("longitude");
            latitude = object.getString("latitude");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public FavoriteAddress() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(street);
        dest.writeString(house_num);
        dest.writeString(door);
        dest.writeString(longitude);
        dest.writeString(latitude);
        dest.writeString(halfAddress);
        dest.writeString(fullAddress);
        dest.writeInt(isEditing);
    }

    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<FavoriteAddress> CREATOR
            = new Parcelable.Creator<FavoriteAddress>() {
        public FavoriteAddress createFromParcel(Parcel in) {
            return new FavoriteAddress(in);
        }

        public FavoriteAddress[] newArray(int size) {
            return new FavoriteAddress[size];
        }
    };

    private FavoriteAddress(Parcel in) {
        id = in.readString();
        name = in.readString();
        street = in.readString();
        house_num = in.readString();
        door = in.readString();
        longitude = in.readString();
        latitude = in.readString();
        halfAddress = in.readString();
        fullAddress = in.readString();
        isEditing = in.readInt();
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getDoor() {
        return door;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse_num() {
        return house_num;
    }

    public void setEditing(int isEditing) {
        this.isEditing = isEditing;
    }

    public int isEditing() {
        return isEditing;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getHalfAddress() {
        String halfAddress;
        if (house_num.equals("")) {
            halfAddress = street;

        } else if (!door.equals("")) {
            halfAddress = street + ", " + house_num + ", " + door;

        } else {
            halfAddress = street + ", " + house_num;
        }
        return halfAddress;
    }

    public void setHalfAddress(String halfAddress) {
        this.halfAddress = halfAddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouse_num(String house_num) {
        this.house_num = house_num;
    }

    public void setDoor(String door) {
        this.door = door;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}