package com.mobapply.taxitter.model;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Item ArrayList<ItemDotsList> that contain data about adreess.
 *
 * @see Order
 * @see OrderProposal
 */

public class AdditionalRoute {
    private String waylat;
    private String  waylong;
    private String wayaddress;

    public AdditionalRoute(JSONObject object) {
        try {
            wayaddress = object.getString("wayaddress");
            waylat = object.getString("waylat");
            waylong = object.getString("waylong");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getWayaddress() {
        return wayaddress;
    }

    public String getWaylong() {
        return waylong;
    }

    public String getWaylat() {
        return waylat;
    }
}