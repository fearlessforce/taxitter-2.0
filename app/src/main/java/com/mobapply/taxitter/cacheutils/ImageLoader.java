package com.mobapply.taxitter.cacheutils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Misha Nester on 14.05.15.
 */
public class ImageLoader {
    final int stub_id = R.drawable.round67;
    private String size;
    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(
            new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Handler handler = new Handler();
    private Context context;

    private static ImageLoader imageLoader;

    public ImageLoader() {
    }

    public static ImageLoader getInstance() {
        if(imageLoader == null) {
            imageLoader = new ImageLoader();
        }
        return imageLoader;
    }

    public void initCache(Context context) {
        this.context = context;
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
    }

    public void DisplayImage(String url, ImageView imageView, String size) {
        this.size = size;
        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);

        if (bitmap != null) {
            if (size.equals(Constants.IMG_LARGE_SIZE)) {
                imageView.setTag(124);
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setImageBitmap(Utils.getRoundedCornerBitmap(bitmap,
                        Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, context)));
            }

        } else {
            queuePhoto(url, imageView);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad photoToLoad = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(photoToLoad));
    }

    private Bitmap getBitmap(String url) {
        File f = fileCache.getFile(url);

        Bitmap b = decodeFile(f);
        if (b != null)
            return b;

        try {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            conn.disconnect();
            bitmap = decodeFile(f);
            return bitmap;

        } catch (Throwable ex){
            ex.printStackTrace();

            if(ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    private Bitmap decodeFile(File f) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1 = new FileInputStream(f);
            BitmapFactory.decodeStream(stream1, null, o);
            stream1.close();

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class PhotoToLoad {
        public String url;
        public ImageView imageView;
        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                if(imageViewReused(photoToLoad))
                    return;

                Bitmap bmp = getBitmap(photoToLoad.url);
                if (size.equals(Constants.IMG_LARGE_SIZE)) {
                    memoryCache.put(photoToLoad.url, bmp);

                } else {
                    bmp = Utils.getRoundedCornerBitmap(bmp,
                            Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, context));
                    memoryCache.put(photoToLoad.url, bmp);
                }

                if (imageViewReused(photoToLoad))
                    return;

                BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
                handler.post(bd);

            } catch(Throwable th) {
                th.printStackTrace();
            }
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);

        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            if (bitmap != null) {
                if (size.equals(Constants.IMG_LARGE_SIZE)) {
                    photoToLoad.imageView.setTag(124);
                    photoToLoad.imageView.setImageBitmap(bitmap);
                } else {
                    photoToLoad.imageView.setImageBitmap(Utils.getRoundedCornerBitmap(bitmap,
                            Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, context)));
                }
            } else {
                photoToLoad.imageView.setImageResource(stub_id);
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }
}