package com.mobapply.taxitter.webconnect;

import android.content.Context;

import com.mobapply.taxitter.interfaces.ResponseListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.RequestMethod;

import org.json.JSONException;
import org.json.JSONObject;

public class CommonRequest {
    private Context context;
    private TransmissionResponseCode transmissionResponseCode;
    private final String URL_SET_AVATAR = AuthenticationRequester.URL_HOST + "/base32/hs/registration/setavatar";
    private final String URL_GET_AVATAR = AuthenticationRequester.URL_HOST + "/base32/hs/registration/getavatar";

    public CommonRequest(Context context) {
        this.context = context;
    }

    public void setAvatar(String cookie, String size, String base64String) {
        JSONObject json = new JSONObject();
        String url = URL_SET_AVATAR;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.SIZE, size);
            json.put(Constants.BASE64_STRING, base64String);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getOwnAvatar(String cookie, String size) {
        JSONObject json = new JSONObject();
        String url = URL_GET_AVATAR;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.SIZE, size);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getOtherAvatar(String cookie, String size, String id) {
        JSONObject json = new JSONObject();
        String url = URL_GET_AVATAR;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.SIZE, size);
            json.put(Constants.UID, id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    private void sendRequest(String url, JSONObject json) {
        HttpRequestAsyncTask httpRequestAsyncTask = new HttpRequestAsyncTask(context, url, json);
        httpRequestAsyncTask.setHttpMethod(RequestMethod.POST);
        httpRequestAsyncTask.setResponseListener(new ResponseListener() {
            @Override
            public void onResponceCode(int code) {
                if (code == Constants.CODE_SUCCESS) {
                    transmissionResponseCode.onMessageSuccess(Constants.OK);

                } else {
                    transmissionResponseCode.onMessageError(code);
                }
            }

            @Override
            public void onResponceMessage(String str) {
                transmissionResponseCode.onMessageSuccess(str);
            }
        });
        httpRequestAsyncTask.execute();
    }

    public void setTransmissionResponseCode(TransmissionResponseCode transmissionResponseCode) {
        this.transmissionResponseCode = transmissionResponseCode;
    }
}