package com.mobapply.taxitter.webconnect;

import android.content.Context;
import android.os.AsyncTask;

import com.mobapply.taxitter.interfaces.ResponseListener;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.RequestMethod;
import com.mobapply.taxitter.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Send all HTTP POST request on server, and transfer back jsonString or response code.
 */
public class HttpRequestAsyncTask extends AsyncTask<String, String, Integer> {
    private final int NO_NETWORK_CONNECTION = -1;
    private ResponseListener responseListener;
    private IOException error;
    private Context context;
    private JSONObject jsonObject;
    private String url;
    private RequestMethod httpMethod;
    private String jsonString;
    private int responseCode = NO_NETWORK_CONNECTION;
    private boolean isConnectionAvailable;

    public HttpRequestAsyncTask(Context context, String url, JSONObject jsonObject) {
        super();
        this.context = context;
        this.url = url;
        this.jsonObject = jsonObject;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isConnectionAvailable = Utils.isConnectionAvailable(context);
    }

    @Override
    protected Integer doInBackground(String... urls) {
        if (isConnectionAvailable) {

            if (httpMethod == RequestMethod.POST) {
                HttpsURLConnection con = null;
                try {
                    URL ur = new URL(url);
//                    SSLContext sslcontext = SSLContext.getInstance("TLSv1");
//
//                    sslcontext.init(null, null, null);
//                    SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());
//
//                    HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);

//                    SSLContext sslcontext = SSLContext.getInstance("TLS");
//                    sslcontext.init(null,
//                            new TrustManager[] { new CustomTrustManager()},
//                            new java.security.SecureRandom());
//                    HttpsURLConnection.setDefaultHostnameVerifier(new CustomHostnameVerifier());
//                    HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext
//                            .getSocketFactory());

                   // trustAllHosts();

                    con = (HttpsURLConnection) ur.openConnection();
                    con.setRequestMethod(Constants.POST);

                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setUseCaches(false);

                    OutputStream os = con.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(String.valueOf(jsonObject));
                    writer.flush();
                    writer.close();

                    responseCode = con.getResponseCode();

                    if (responseCode == Constants.CODE_SUCCESS) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                        StringBuilder total = new StringBuilder();
                        String line;

                        while ((line = reader.readLine()) != null) {
                            total.append(line);
                        }

                        jsonString = total.toString();

                        reader.close();
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    error = e;
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        con.disconnect();
                    }
                }


            } else if (httpMethod == RequestMethod.GET) {
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(url);

                try {
                    request.addHeader(Constants.ACCESS_TOKEN, jsonObject.getString(Constants.ACCESS_TOKEN));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                HttpResponse response = null;
                try {
                    response = client.execute(request);
                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(response.getEntity().getContent()));

                    StringBuffer result = new StringBuffer();
                    String line = "";
                    while ((line = rd.readLine()) != null) {
                        result.append(line);
                    }

                    jsonString = result.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer res) {
        super.onPostExecute(res);

        if (error == null) {
            if (jsonString != null && jsonString.length() != 0) {
                responseListener.onResponceMessage(jsonString);

            } else {
                responseListener.onResponceCode(responseCode);
            }

        } else {
            if (responseCode == 400 || responseCode == 500)
                responseListener.onResponceCode(responseCode);
        }
    }

    public void setHttpMethod(RequestMethod method) {
        httpMethod = method;
    }

    protected void setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
    }

    private static void trustAllHosts() {

        X509TrustManager easyTrustManager = new X509TrustManager() {

            public void checkClientTrusted(
                    X509Certificate[] chain,
                    String authType) throws CertificateException {
                // Oh, I am easy!
            }

            public void checkServerTrusted(
                    X509Certificate[] chain,
                    String authType) throws CertificateException {
                // Oh, I am easy!
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

        };

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {easyTrustManager};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}