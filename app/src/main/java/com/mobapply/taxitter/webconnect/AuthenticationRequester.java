package com.mobapply.taxitter.webconnect;

import android.content.Context;

import com.mobapply.taxitter.interfaces.ResponseListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.RequestMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Formation json for all enter/registration methods
 * Json-data transfer to HttpRequestAsyncTask for sending request, and response back.
 *
 * @see HttpRequestAsyncTask
 */

public class AuthenticationRequester {
    //protected static final String URL_HOST = "https://tranko.borisskin.com";
    protected static final String URL_HOST = "https://app.taxitter.com";
    private static final String URL_PRE_REGISTRATION = URL_HOST + "/base32/hs/registration/preregister";
    private static final String URL_NEW_SMS_CODE = URL_HOST + "/base32/hs/registration/newcode";
    private static final String URL_REGISTRATION = URL_HOST + "/base32/hs/registration/register";
    private static final String URL_RESET_PASSWORD = URL_HOST + "/base32/hs/registration/newpassword";
    public static final String URL_AUTHORIZATION = URL_HOST + "/base32/hs/registration/auth";

    private Context context;
    private TransmissionResponseCode transmissionResponseCode;



    public AuthenticationRequester(Context context) {
        this.context = context;
    }

    public void preRegister(String number) {
        getSmsCode(URL_PRE_REGISTRATION, number);
    }

    public void register(String number, String code) {
        JSONObject json = new JSONObject();
        String url = URL_REGISTRATION;

        try {
            json.put(Constants.ID, number);
            json.put(Constants.CODE, code);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void authorize(String number, String password, String registration_id) {
        JSONObject json = new JSONObject();
        String url = URL_AUTHORIZATION;

        try {
            json.put(Constants.ID, number);
            json.put(Constants.PASSWORD, password);
            json.put(Constants.REGISTRATION_ID, registration_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void ressetPassword(String number) {
        JSONObject json = new JSONObject();
        String url = URL_RESET_PASSWORD;

        try {
            json.put(Constants.ID, number);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getNewSmsCode(String number) {
        getSmsCode(URL_NEW_SMS_CODE, number);
    }

    private void getSmsCode(String url1, String number) {
        JSONObject json = new JSONObject();
        String url = url1;

        try {
            json.put(Constants.ID, number);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    private void sendRequest(String url, JSONObject json) {
        HttpRequestAsyncTask httpRequestAsyncTask = new HttpRequestAsyncTask(context, url, json);
        httpRequestAsyncTask.setHttpMethod(RequestMethod.POST);
        httpRequestAsyncTask.setResponseListener(new ResponseListener() {
            @Override
            public void onResponceCode(int code) {
                if (code == Constants.CODE_SUCCESS) {
                    transmissionResponseCode.onMessageSuccess(Constants.OK);

                } else {
                    transmissionResponseCode.onMessageError(code);
                }
            }

            @Override
            public void onResponceMessage(String str) {
                transmissionResponseCode.onMessageSuccess(str);
            }
        });
        httpRequestAsyncTask.execute();
    }

    public void setTransmissionResponseCode(TransmissionResponseCode transmissionResponseCode) {
        this.transmissionResponseCode = transmissionResponseCode;
    }
}