package com.mobapply.taxitter.webconnect;

import android.content.Context;
import android.content.SharedPreferences;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.ResponseListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.CordsUtils;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.RequestMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Formation json for all Passenger HTTP request methods
 * Json-data transfer to HttpRequestAsyncTask for sending request,
 * and response(code, jsonString, message about error) back.
 *
 * @see HttpRequestAsyncTask
 */
public class PassengerRequester {
    private static final String WHEN = "when";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String DEPARTURE = "departure";
    private static final String CITY = "city";
    private static final String CASH = "cash";
    private static final String LENGTH = "length";
    private static final String COMMENT = "comment";
    private static final String NONSMOKING = "nonsmoking";
    private static final String BABYSEAT = "babyseat";
    private static final String AC = "ac";
    private static final String QUANTITY = "quantity";
    private static final String WAYLAT = "waylat";
    private static final String WAYLONG = "waylong";
    private static final String WAYADDRESS = "wayaddress";
    private static final String WAYPOINTS = "waypoints";
    private static final String STATUS_P = "status_p";

    private final String URL_ORDER_TAXI = AuthenticationRequester.URL_HOST + "/base32/hs/orders/neworder";
    private final String URL_SAVE_PROFILE = AuthenticationRequester.URL_HOST + "/base32/hs/registration/setsettings";
    private final String URL_GET_PROPOSALS = AuthenticationRequester.URL_HOST + "/base32/hs/orders/getofferslist";
    private final String URL_CHANGE_OFFER = AuthenticationRequester.URL_HOST + "/base32/hs/orders/changeoffer";
    private final String URL_CANCEL_ORDER_P = AuthenticationRequester.URL_HOST + "/base32/hs/orders/cancelorder";
    private final String URL_GET_FAVORITE_ADDRESS = AuthenticationRequester.URL_HOST + "/base32/hs/registration/getfavaddress";
    private final String URL_SET_FAVORITE_ADDRESS = AuthenticationRequester.URL_HOST + "/base32/hs/registration/setfavaddress";
    private final String URL_SET_RATING = AuthenticationRequester.URL_HOST + "/base32/hs/orders/setrating";
    private final String URL_GET_FEADBACKS = AuthenticationRequester.URL_HOST + "/base32/hs/registration/getfeedbacks";
    private final String URL_GET_USER_POSITION = AuthenticationRequester.URL_HOST + "/base32/hs/registration/getuserposition";

    private SharedPreferences sharedPreferences;
    public static Context context;
    private TransmissionResponseCode transmissionResponseCode;

    public PassengerRequester(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), 0);
    }

    public void sendOrder(String cookie, String when, String latitude, String longitude, String departure, String city,
                          String cash, int nonsmoking, int babyseat, int ac, String quantity, ArrayList<CordsUtils> waypoints,
                          String waylat, String waylong, String comment, String distance) {

        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        String url = URL_ORDER_TAXI;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);

            if(!when.equals("")) {
                SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMddHHmm");
                json.put(WHEN, timeFormat.parse(when).getTime()/1000);
            }

            json.put(LATITUDE, latitude);
            json.put(LONGITUDE, longitude);
            json.put(DEPARTURE, departure);
            json.put(CITY, city);
            json.put(CASH, cash);
            json.put(LENGTH, distance);

            if(!comment.equals(""))
                json.put(COMMENT, comment);

            if(nonsmoking != 0)
                json.put(NONSMOKING, nonsmoking);

            if(babyseat != 0)
                json.put(BABYSEAT, babyseat);

            if(ac != 0)
                json.put(AC, ac);

            json.put(QUANTITY, quantity);

            for(int i = 1; i < waypoints.size(); i++) {
                JSONObject pnObj = new JSONObject();
                pnObj.put(WAYLAT, String.valueOf(waypoints.get(i).getLatitude()));
                pnObj.put(WAYLONG, String.valueOf(waypoints.get(i).getLongitude()));
                pnObj.put(WAYADDRESS, String.valueOf(waypoints.get(i).getAddress()));
                jsonArray.put(pnObj);
            }
            json.put(WAYPOINTS, jsonArray);
            json.put(WAYLAT, waylat);
            json.put(WAYLONG, waylong);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void savePassengerProfile(String cookie, String nickname, String quantity_d, String city_d) {
        JSONObject json = new JSONObject();
        String url = URL_SAVE_PROFILE;

        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            json.put(Constants.ACCESS_TOKEN, cookie);

            if (!sharedPreferences.getString(Constants.NICKNAME, "").equals(nickname)) {
                json.put(Constants.NICKNAME, nickname);
                editor.putString(Constants.NICKNAME, nickname);
            }

            json.put(Constants.QUANTITY_D, quantity_d);

            if (!sharedPreferences.getString(Constants.AREA_AND_CITY, "").equals(city_d)) {
                json.put(Constants.AREA_AND_CITY, city_d);

                editor.putString(Constants.AREA_AND_CITY, city_d);
                String address[] = city_d.split(",");
                String area = address[0];
                String city = "";
                if (address.length == 2)
                    city = address[1];

                json.put(Constants.CITY_D, city.replace(" ", ""));

                editor.putString(Constants.CITY_D, city.replace(" ", ""));
                editor.putString(Constants.REGION_D, area);
            }

            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getListOfProposals(String cookie) {
        JSONObject json = new JSONObject();
        String url = URL_GET_PROPOSALS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void changeOffer(String cookie, String order_uid, String uid_d, String cash_d, String status_p) {
        JSONObject json = new JSONObject();
        String url = URL_CHANGE_OFFER;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);
            json.put(Constants.UID_D, uid_d);
            json.put(Constants.CASH_D, cash_d);
            json.put(STATUS_P, status_p);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void cancelOrder_p(String cookie, String order_uid) {
        JSONObject json = new JSONObject();
        String url = URL_CANCEL_ORDER_P;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getFavoriteAddress(String cookie) {
        JSONObject json = new JSONObject();
        String url = URL_GET_FAVORITE_ADDRESS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void setFavoriteAddress(String cookie, String id, String name, String street, String house_num,
                                   String door, String longitude, String latitude, String delete) {
        JSONObject json = new JSONObject();
        String url = URL_SET_FAVORITE_ADDRESS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ID, id);
            json.put(Constants.NAME_ADDRESS, name);
            json.put(Constants.STREET, street);
            json.put(Constants.HOUSE_NUM, house_num);
            json.put(Constants.DOOR, door);
            json.put(Constants.LONGITUDE, longitude);
            json.put(Constants.LATITUDE, latitude);
            json.put(Constants.DELETE, delete);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void setRating(String cookie, String order_uid, String rating, String comment) {
        JSONObject json = new JSONObject();
        String url = URL_SET_RATING;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);
            json.put(Constants.RATING, rating);
            json.put(Constants.COMMENT, comment);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getFeedbacks(String cookie, String uid) {
        JSONObject json = new JSONObject();
        String url = URL_GET_FEADBACKS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.UID, uid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    public void getUserPosition(String cookie, String uid_d) {
        JSONObject json = new JSONObject();
        String url = URL_GET_USER_POSITION;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.UID, uid_d);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json);
    }

    private void sendRequest(String url, JSONObject json) {
        HttpRequestAsyncTask httpRequestAsyncTask = new HttpRequestAsyncTask(context, url, json);
        httpRequestAsyncTask.setHttpMethod(RequestMethod.POST);
        httpRequestAsyncTask.setResponseListener(new ResponseListener() {
            @Override
            public void onResponceCode(int code) {
                if (code == Constants.CODE_SUCCESS) {
                    transmissionResponseCode.onMessageSuccess(Constants.OK);

                } else {
                    transmissionResponseCode.onMessageError(code);
                }
            }

            @Override
            public void onResponceMessage(String str) {
                transmissionResponseCode.onMessageSuccess(str);
            }
        });
        httpRequestAsyncTask.execute();
    }

    public void setTransmissionResponseCode(TransmissionResponseCode transmissionResponseCode) {
        this.transmissionResponseCode = transmissionResponseCode;
    }
}