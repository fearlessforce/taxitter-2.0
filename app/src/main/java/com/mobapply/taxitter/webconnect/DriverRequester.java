package com.mobapply.taxitter.webconnect;

import android.content.Context;
import android.content.SharedPreferences;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.ResponseListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.RequestMethod;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Formation json for all Driver HTTP request methods
 * Json-data transfer to HttpRequestAsyncTask for sending request,
 * and response(code, jsonString, message about error) back.
 *
 * @see HttpRequestAsyncTask
 */

public class DriverRequester {
    private static Context context;
    private TransmissionResponseCode transmissionResponseCode;
    private SharedPreferences sharedPreferences;

    private final String URL_GET_CITIES = AuthenticationRequester.URL_HOST + "/base32/hs/registration/getcities";
    private final String URL_GET_LIST_ORDERS = AuthenticationRequester.URL_HOST + "/base32/hs/orders/getorderslist";
    private final String URL_SET_OFFERS = AuthenticationRequester.URL_HOST + "/base32/hs/orders/setoffer";
    private final String URL_SAVE_PROFILE = AuthenticationRequester.URL_HOST + "/base32/hs/registration/setsettings";
    private final String URL_GET_PROFILE_DATA = AuthenticationRequester.URL_HOST + "/base32/hs/registration/getsettings";
    private final String URL_CONFIRM_REFUSE_OFFER = AuthenticationRequester.URL_HOST + "/base32/hs/orders/acceptorder";
    private final String URL_CURRENT_ORDER_STATUS = AuthenticationRequester.URL_HOST + "/base32/hs/orders/setorderstatus";
    private final String URL_CANCEL_ORDER = AuthenticationRequester.URL_HOST + "/base32/hs/orders/cancelorder";
    private final String URL_CHANGE_ROLE = AuthenticationRequester.URL_HOST + "/base32/hs/registration/changerole";
    private final String URL_SETUP_USER_POSITION = AuthenticationRequester.URL_HOST + "/base32/hs/registration/setuserposition";

    public DriverRequester(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), 0);
    }

    public void changeRole(String cookie, String status_user) {
        JSONObject json = new JSONObject();
        String url = URL_CHANGE_ROLE;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.STATUS_USER, status_user);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }

    public void setOffers(String cookie, String order_uid, String cash_d, String status_d,
                          String comment_d, String distance_d) {
        JSONObject json = new JSONObject();
        String url = URL_SET_OFFERS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);
            json.put(Constants.CASH_D, cash_d);
            json.put(Constants.STATUS_D, status_d);
            json.put(Constants.COMMENT_D, comment_d);
            json.put(Constants.DISTANCE_D, distance_d);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }

    public void getListOrders(String cookie, String status_offer, String city_d) {
        JSONObject json = new JSONObject();
        String url = URL_GET_LIST_ORDERS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.LATITUDE_D, "65,5646");
            json.put(Constants.LONGITUDE_D, "65,5646");
            json.put(Constants.CITY_D, city_d);
            json.put(Constants.STATUS_OFFER, status_offer);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }

    public void confirmRefuseOffers(String cookie, String order_uid, String action_d, String cash_d) {
        JSONObject json = new JSONObject();
        String url = URL_CONFIRM_REFUSE_OFFER;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);
            json.put(Constants.ACTION_D, action_d);
            json.put(Constants.CASH_D, cash_d);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }


    public void currentOrderStatus(String cookie, String order_uid, String status_d, String cash_d) {
        JSONObject json = new JSONObject();
        String url = URL_CURRENT_ORDER_STATUS;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);
            json.put(Constants.STATUS_D, status_d);
            json.put(Constants.CASH_D, cash_d);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }

    public void cancelOrder(String cookie, String order_uid) {
        JSONObject json = new JSONObject();
        String url = URL_CANCEL_ORDER;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.ORDER_UID, order_uid);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }


    public void getCities(String cookie) {
        JSONObject json = new JSONObject();
        String url = URL_GET_CITIES;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.GET);
    }

    private void sendRequest(String url, JSONObject json, RequestMethod requestMethod) {
        HttpRequestAsyncTask httpRequestAsyncTask = new HttpRequestAsyncTask(context, url, json);
        httpRequestAsyncTask.setHttpMethod(requestMethod);
        httpRequestAsyncTask.setResponseListener(new ResponseListener() {
            @Override
            public void onResponceCode(int code) {
                if (transmissionResponseCode != null) {
                    if (code == Constants.CODE_SUCCESS) {
                        transmissionResponseCode.onMessageSuccess(Constants.OK);
                    } else {
                        transmissionResponseCode.onMessageError(code);
                    }
                }
            }

            @Override
            public void onResponceMessage(String str) {
                if (transmissionResponseCode != null) {
                    transmissionResponseCode.onMessageSuccess(str);
                }
            }
        });
        httpRequestAsyncTask.execute();
    }

    public void saveProfile(String cookie, String nickname, String car_d, String carmodel_d, String cartype_d, String carcolor_d,
                            String caryear_d, String carnumber_d, int babyseat_d, int ac_d, int nosmoker, String quantity_d, String maxradius_d, String area_and_city) {

        JSONObject json = new JSONObject();
        String url = URL_SAVE_PROFILE;

        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            json.put(Constants.ACCESS_TOKEN, cookie);

            if (!sharedPreferences.getString(Constants.NICKNAME, "").equals(nickname)) {
                json.put(Constants.NICKNAME, nickname);
                editor.putString(Constants.NICKNAME, nickname);
            }

            if (!sharedPreferences.getString(Constants.CAR_D, "").equals(car_d)) {
                json.put(Constants.CAR_D, car_d);
                editor.putString(Constants.CAR_D, car_d);
            }

            if (!sharedPreferences.getString(Constants.CARMODEL_D, "").equals(carmodel_d)) {
                json.put(Constants.CARMODEL_D, carmodel_d);
                editor.putString(Constants.CARMODEL_D, carmodel_d);
            }

            if (!sharedPreferences.getString(Constants.CARTYPE_D, "").equals(cartype_d)) {
                json.put(Constants.CARTYPE_D, cartype_d);
                editor.putString(Constants.CARTYPE_D, cartype_d);
            }

            if (!sharedPreferences.getString(Constants.CARCOLOR_D, "").equals(carcolor_d)) {
                json.put(Constants.CARCOLOR_D, carcolor_d);
                editor.putString(Constants.CARCOLOR_D, carcolor_d);
            }

            if (sharedPreferences.getString(Constants.CARYEAR_D, "0") != caryear_d) {
                json.put(Constants.CARYEAR_D, caryear_d);
                editor.putString(Constants.CARYEAR_D, caryear_d);
            }

            if (!sharedPreferences.getString(Constants.CARNUMBER_D, "").equals(carnumber_d)) {
                json.put(Constants.CARNUMBER_D, carnumber_d);
                editor.putString(Constants.CARNUMBER_D, carnumber_d);
            }

            if (sharedPreferences.getInt(Constants.BABYSEAT_D, 0) != babyseat_d) {
                json.put(Constants.BABYSEAT_D, babyseat_d);
                editor.putInt(Constants.BABYSEAT_D, babyseat_d);
            }

            if (sharedPreferences.getInt(Constants.AC_D, 0) != ac_d) {
                json.put(Constants.AC_D, ac_d);
                editor.putInt(Constants.AC_D, ac_d);
            }

            if (sharedPreferences.getInt(Constants.NOSMOKING_D, 0) != nosmoker) {
                json.put(Constants.NOSMOKING_D, nosmoker);
                editor.putInt(Constants.NOSMOKING_D, nosmoker);
            }

            if (!sharedPreferences.getString(Constants.QUANTITY_D, "").equals(quantity_d)) {
                json.put(Constants.QUANTITY_D, quantity_d);
                editor.putString(Constants.QUANTITY_D, quantity_d);
            }

            if (!sharedPreferences.getString(Constants.MAXRADIUS_D, "").equals(maxradius_d)) {
                json.put(Constants.MAXRADIUS_D, maxradius_d);
                editor.putString(Constants.MAXRADIUS_D, maxradius_d);
            }

            if (!sharedPreferences.getString(Constants.AREA_AND_CITY, "").equals(area_and_city)) {
                json.put(Constants.AREA_AND_CITY, area_and_city);
                editor.putString(Constants.AREA_AND_CITY, area_and_city);
                String address[] = area_and_city.split(",");
                String area = address[0];
                String city = "";
                if (address.length == 2)
                    city = address[1];

                json.put(Constants.CITY_D, city.replace(" ", ""));

                editor.putString(Constants.CITY_D, city.replace(" ", ""));
                editor.putString(Constants.REGION_D, area);
            }

            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }

    public void setupUserPosition(String cookie, double latitude, double longitude) {
        JSONObject json = new JSONObject();
        String url = URL_SETUP_USER_POSITION;

        try {
            json.put(Constants.ACCESS_TOKEN, cookie);
            json.put(Constants.LATITUDE, latitude);
            json.put(Constants.LONGITUDE, longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendRequest(url, json, RequestMethod.POST);
    }

    public void setTransmissionResponseCode(TransmissionResponseCode transmissionResponseCode) {
        this.transmissionResponseCode = transmissionResponseCode;
    }

    public static String checkGetListStatementsErrorCode(Context context, int errorCode) {
        if (errorCode == Constants.NOT_FIND_DB_DRIVER_NAME_CITY) {
            return context.getString(R.string.not_find_db_driver_name_sity);

        } else if (errorCode == Constants.GET_ORDERS_FAILED_INCORRECT_VALUE_STATUS_D) {
            return context.getString(R.string.incorrect_value_status_d);

        } else  {
            return otherErrors(context, errorCode);
        }
    }

    public String checkAcceptOrderErrorCode(Context context, int errorCode) {
        if (errorCode == Constants.FAILED_TO_WRITE_IN_DB) {
            return context.getString(R.string.failed_to_write_in_db);

        } else if (errorCode == Constants.INCORRECT_STATUS_USER) {
            return context.getString(R.string.incorrect_value_status_user);

        } else  {
            return otherErrors(context, errorCode);
        }
    }

    public static String otherErrors(Context context, int errorCode) {

        if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            return context.getString(R.string.not_all_fields);

        } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
            return context.getString(R.string.json_error);

        } else if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            return context.getString(R.string.not_find_user_with_access_token);

        } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
            return context.getString(R.string.no_network);

        }
        return "";
    }
}