package com.mobapply.taxitter.utils;

public class Constants {
    public static final String AUTOCOMPLETE_KEY = "AIzaSyBNmfxKEVOeyh6Ysgk4WTs7t5KhaBgkcy0";
    public static final String ACCESS_TOKEN = "access-token";
    public static final String POST = "POST";
    public static final String GET = "GET";

    public static final String CURRENT_ORDER = "current_order";
    public static final String CURRENT_ORDER_ACCEPT = "current_order_accept";
    public static final String CURRENT_ORDER_UID = "current_order_uid";

    public static final String DRIVER_MENU_FRAGMENT = "driver_menu_fragment";
    public static final String DRIVER_DETAILE_OF_ORDER_TAG = "driver_detaile_of_order";
    public static final String DRIVER_ROLE_STATUS = "2";
    public static final String DRIVER_DETAILE_OF_OFFER_TAG = "details_offer_tag";
    public static final String DRIVER_ORDERS_ACCEPTED_BY_THE_PASSENGER = "2";

    public static final String PASSENGER_CONFIRM_ORDER_STATE = "confirm";
    public static final String PASSENGER_DETAILE_OF_PROPOSALS = "detale_of_proposals";
    public static final String PASSENGER_REJECT_ORDER = "0";
    public static final String PASSENGER_ACCEPT_DRIVER_PROPOSAL = "1";
    public static final String PASSENGER_EXPENSIVE_DRIVER_PROPOSAL = "2";
    public static final String PASSENGER_DRIVER_ACCEPT_ORDER = "3";
    public static final String PASSENGER_ACCEPT_ORDER = "2";
    public static final String PASSENGER_LIST_OF_DRIVER_PROPOSALS = "list_of_driver_proposal";

    public static final String FULLNAME_DOTS_ARRAY = "full_dots_array";
    public static final String TIME = "time";
    public static final String PASSENGERS = "passengers";
    public static final String PRICE = "price";
    public static final String COMMENT = "comment";
    public static final String FROM_WHERE = "from_where";
    public static final String WHERE_TO = "where_to";
    public static final String BABY_SEAT = "babySet";
    public static final String NO_SMOKING = "noSmoking";
    public static final String AIR_CONDITION = "airCondition";
    public static final String STREET = "street";
    public static final String HOUSE = "house";
    public static final String ENTRANCE = "entrance";

    public static final String PROFILE_COUNTRY = "Россия, ";
    public static final String DATA = "data";

    public static final String DRIVER_ROLE = "2";
    public static final String PASSENGER_ROLE = "1";
    public static final String ROLE_SATE = "role_state";

    public static final String CHOICE_ROLE_TAG = "choice_roles_tag";

    public static final String REGISTER_STACK = "register_stack";
    public static final String REGISTER_TAG = "register_tag";
    public static final String ENTER_TAG = "enter_tag";
    public static final String ENTER_STACK = "enter_stack";

    public static final String PROPERTY_REG_ID = "registration_id";

    public static final int WRONG_PASSWORD = 1;
    public static final int NO_USER_IN_DB = 11;
    public static final int FAILED_TO_SEND_SMS_PASSWORD = 1;
    public static final int NO_RECORDS_PASS_FOR_USER = 11;
    public static final int CODE_SUCCESS = 200;
    public static final int INCORRECT_STATUS_USER = 11;
    public static final int GET_ORDERS_FAILED_INCORRECT_VALUE_STATUS_D = 14;
    public static final int NOT_ALL_REQUIRED_FIELDS_ARE_FILLED = 12;
    public static final int NO_NEW_ORDERS = 2;
    public static final int NOT_FIND_DB_DRIVER_NAME_CITY = 11;
    public static final int NO_NETWORK_CONNECTION = 1019;
    public static final int FAILED_TO_WRITE_IN_DB = 1;
    public static final int NO_REQUEST_IN_DB = 11;
    public static final int ERROR_JSON_FORMAT = 13;
    public static final int CASH_EQUALS_ZERO = 14;
    public static final int NOT_FIND_USER_WITH_ACCESS_TOKEN = 41;
    public static final int ERROR_ZERO_CASH = 14;
    public static final int FAILED_INCORRECT_VALUE_STATUS_P = 15;
    public static final int PROFILE_IS_NOT_FILLED = 13;

    public static final String OK = "ok";
    public static final String ID = "id";
    public static final String CODE = "code";
    public static final String PASSWORD = "password";
    public static final String REGISTRATION_ID = "registration_id";

    public static final String STATUS_USER = "status_user";
    public static final String NICKNAME = "nickname";
    public static final String CAR_D = "car_d";
    public static final String CARMODEL_D = "carmodel_d";
    public static final String CARTYPE_D = "cartype_d";
    public static final String CARCOLOR_D = "carcolor_d";
    public static final String CARYEAR_D = "caryear_d";
    public static final String CARNUMBER_D = "carnumber_d";
    public static final String BABYSEAT_D = "babyseat_d";
    public static final String AC_D = "ac_d";
    public static final String NOSMOKING_D = "nonsmoking_d";
    public static final String QUANTITY_D = "quantity_d";
    public static final String MAXRADIUS_D = "maxradius_d";
    public static final String ORDER_UID = "order_uid";
    public static final String CASH_D = "cash_d";
    public static final String STATUS_D = "status_d";
    public static final String COMMENT_D = "comment_d";
    public static final String LATITUDE_D = "latitude_d";
    public static final String LONGITUDE_D = "longitude_d";
    public static final String AREA_AND_CITY = "area_and_city";
    public static final String STATUS_OFFER = "status_offer";
    public static final String ACTION_D = "action_d";
    public static final String UID_D = "uid_d";
    public static final String CITY_LONGITUDE = "city_longitude";
    public static final String CITY_LATITUDE = "city_latitude";
    public static final String CITY_RADIUS = "city_radius";

    public static final String PUSH_DATA = "push_data";
    public static final String DRIVER_ACTION_ONE_OR_THREE = "driver_action_one_or_three";
    public static final String DRIVER_PROPOSAL_ACCEPT = "driver_proposal_accept";
    public static final String BROADCAST_ACTION = "com.codenest.taxitter.action.CONNECTION";
    public static final String ORDER_ID = "order_id";
    public static final String PASSENGER_CANCEL_ORDER = "passenger_cancel_order";
    public static final String NOTIFICATION_PASSENGER_CANCEL_ORDER = "notification_passenger_cancel_order";
    public static final String APP_OPENED = "app_opened";
    public static final String SIZE = "size";
    public static final String BASE64_STRING = "base64string";

    public static final String IMG_LARGE_SIZE = "2";
    public static final String IMG_SMALL_SIZE = "1";
    public static final String CROP_IMAGE = "crop_image";
    public static final String ORIGINAL_IMAGE = "original_size";
    public static final String OWN_CROP_IMAGE = "own_crop_image";
    public static final String OWN_ORIGINAL_IMAGE = "own_original_size";
    public static final int PICK_FROM_CAMERA = 1;
    public static final int PICK_FROM_GALLERY = 2;
    public static final int PIC_CROP = 3;
    public static final float DP_IMAGE_SIZE = 100;

    public static final String UID = "uid";
    public static final String GO_ROUTE = "go_route";
    public static final String DRIVER_ACTION_GO_ROUTE = "driver_action_go_route";
    public static final int NOTIFICATION_ID = 0;
    public static final String CAR_INFO = "car_info";
    public static final String PASS_SEET_IN_CAR = "pass_seet_in_car";
    public static final String NEW_ORDER_PUSH = "push_new_order";
    public static final String NEW_ORDER = "new_order";
    public static final String KILOMETERS = "kilometers";
    public static final String LENGTH = "length";
    public static final String CASH = "cash";
    public static final String FIRST_ENTER = "0";
    public static final String CHANGE_ROLE_STATE = "3";
    public static final String DISTANCE_D = "distance_d";
    public static final String HOUSE_NUM = "house_num";
    public static final String DOOR = "door";
    public static final String RATING = "rating";
    public static final String CITY_D = "city_d";
    public static final String REGION_D = "region_d";
    public static final String LONGITUDE = "longitude";
    public static final String LATITUDE = "latitude";
    public static final String DELETE = "delete";
    public static final String ADDRESS = "address";
    public static final String ADDRESS_ARRAY = "address_array";
    public static final String NAME_ADDRESS = "name";

    public static final String MAPS_GEOCODE = "http://maps.googleapis.com/maps/api/geocode/json?";
    public static final String OWN_MAP_FRAGMENT = "fragmentOwnMap";
    public static final String EXIT = "EXIT";
    public static final String FEEDBACK = "feedback";
    public static final String IMG_OWN_SMALL_SIZE = "image_own_small_size";
    public static final String LOADED = "loaded";
    public static final String PASSENGER_CANCEL_ORDER_DIALOG = "passenger_cancel_order_dialog";
    public static final String NEED_HOME_SCREEN = "need_home_screen";
    public static final String NEW_DRIVER_PROPOSAL = "new_driver_proposal";
    public static final String EXTRAS = "extras";
    public static final String MINCASHE = "mincash";
    public static final String NAME = "name";
    public static final String REGION = "region";

}