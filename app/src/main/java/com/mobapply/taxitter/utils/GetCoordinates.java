package com.mobapply.taxitter.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.CountCoordinatesListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import se.walkercrou.places.AddressComponent;
import se.walkercrou.places.GooglePlaces;
import se.walkercrou.places.Place;
import se.walkercrou.places.exception.GooglePlacesException;

import static se.walkercrou.places.GooglePlaces.*;

/**
 * Created by Misha Nester on 27.04.15.
 */
public class GetCoordinates extends AsyncTask<String, Void, Void> {

    private final String GEOCODE_API_BASE = "https://maps.googleapis.com/maps/api/geocode";
    private final String OUT_JSON = "/json";

    private CountCoordinatesListener countCoordinatesListener;
    private SharedPreferences sharedPreferences;

    public GetCoordinates(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), 0);
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            String param = params[0].replace("\"","'");
            GooglePlaces client = new GooglePlaces(Constants.AUTOCOMPLETE_KEY);
            List<Place> places = client.getPlacesByQuery(param, 1,
                    Param.name("language").value("ru"));
            Place addresses = null;

            for (Place place : places) {
                addresses = place;
            }

            if (addresses != null) {
                if (params.length >1) {
                    List<AddressComponent> addressComponents = geocode(params[1]);

                    Class<?> c = Class.forName("se.walkercrou.places.Place");
                    Method method = c.getDeclaredMethod("addAddressComponents", Collection.class);
                    method.setAccessible(true);
                    method.invoke(addresses, addressComponents);
                }

                String d = sharedPreferences.getString(Constants.CITY_D, "");

                if (addresses.getAddress()!= null && addresses.getAddress().contains(d + ",")) {
                    countCoordinatesListener.onCountCoordinatesListener(addresses);
                } else {
                    countCoordinatesListener.onCountCoordinatesListener(null);
                }
            } else{
                countCoordinatesListener.onCountCoordinatesListener(null);
            }

        } catch (GooglePlacesException ex) {
            Log.e("", "GooglePlacesException yoo have fufilled the maximum amount of queries permitted by your API key");
            countCoordinatesListener.onCountCoordinatesListener(null);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            countCoordinatesListener.onCountCoordinatesListener(null);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            countCoordinatesListener.onCountCoordinatesListener(null);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            countCoordinatesListener.onCountCoordinatesListener(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            countCoordinatesListener.onCountCoordinatesListener(null);
        }

        return null;
    }


    public void setTransmissionMessage(CountCoordinatesListener countCoordinatesListener) {
        this.countCoordinatesListener = countCoordinatesListener;
    }


    public ArrayList<AddressComponent> geocode(String placeId) {
        ArrayList<AddressComponent> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(GEOCODE_API_BASE + OUT_JSON);
            sb.append("?place_id=");
            sb.append(placeId);
            sb.append("&language=ru");
            sb.append("&key=" + Constants.AUTOCOMPLETE_KEY);

            Log.d("TAG", sb.toString());

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }

        } catch (MalformedURLException e) {
            Log.e("TAG", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("TAG", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            AddressComponent addressComponent = null;


            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray addressComponents =jsonObj.getJSONArray("results").getJSONObject(0).getJSONArray("address_components");

            resultList =  new ArrayList<AddressComponent>(addressComponents.length());
            for (int i = 0; i < addressComponents.length(); i++) {
                JSONObject object = addressComponents.getJSONObject(i);
                final String ln = object.getString("long_name");
                final String sn = object.getString("short_name");
                JSONArray typesArray = object.getJSONArray("types");
                final ArrayList<String> ts = new ArrayList<String>(typesArray.length());
                for (int j = 0; j < typesArray.length(); j++){
                    ts.add(typesArray.getString(j));
                }
                addressComponent = new AddressComponent(){
                    {
                        setLongName(ln);
                        setShortName(sn);
                        addTypes(ts);
                    }
                };


               resultList.add(addressComponent);

            }
        } catch (JSONException e) {
            Log.e("TAG", "Cannot process JSON results", e);
        }
        return resultList;
    }
}