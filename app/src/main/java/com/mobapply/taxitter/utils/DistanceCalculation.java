package com.mobapply.taxitter.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.mobapply.taxitter.interfaces.TransmissionText;
import com.mobapply.taxitter.service.GPSTracker;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
* Created by Misha Nester on 19.04.15.
*/
public class DistanceCalculation {
    private Context context;
    private TransmissionText transmissionTextListener;

    public void setTransmissionTextListener(TransmissionText transmissionTextListener) {
        this.transmissionTextListener = transmissionTextListener;
    }

    public DistanceCalculation(Context context, LatLng latLngA, LatLng latLngB) {
        this.context = context;
        getDistance(latLngA, latLngB);
    }

    public DistanceCalculation(Context context, double fromWhereLat, double fromWhereLon, TransmissionText transmissionTextListener) {
        this.context = context;
        this.transmissionTextListener = transmissionTextListener;
        GPSTracker gps = new GPSTracker(context);
        LatLng latLngA = new LatLng(gps.getLatitude(), gps.getLongitude());
        LatLng latLngB = new LatLng(fromWhereLat, fromWhereLon);
        getDistance(latLngA, latLngB);
    }

    public DistanceCalculation(Context context, LatLng latLngA, LatLng latLngB, TransmissionText transmissionTextListener) {
        this.context = context;
        this.transmissionTextListener = transmissionTextListener;
        getDistance(latLngA, latLngB);
    }

    public DistanceCalculation(Context context, double fromWhereLat, double fromWhereLon) {
        this.context = context;
        GPSTracker gps = new GPSTracker(context);
        LatLng latLngA = new LatLng(gps.getLatitude(), gps.getLongitude());
        LatLng latLngB = new LatLng(fromWhereLat, fromWhereLon);
        getDistance(latLngA, latLngB);
    }

    private void getDistance(LatLng latLngA, LatLng latLngB) {
        String url = getDirectionsUrl(latLngA, latLngB);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.setTransmissionTextListener(new TransmissionText() {
            @Override
            public void transmissionText(String str) {
                transmissionTextListener.transmissionText(str);
            }
        });
        downloadTask.execute(url);
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        private TransmissionText transmissionTextListener;

        public void setTransmissionTextListener(TransmissionText transmissionTextListener) {
            this.transmissionTextListener = transmissionTextListener;
        }

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.setTransmissionTextListener(new TransmissionText() {
                @Override
                public void transmissionText(String str) {
                    transmissionTextListener.transmissionText(str);
                }
            });
            parserTask.execute(result);
        }
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();

        } catch (Exception e) {
            Log.d("Excep while down url", e.toString());
        } finally {
            assert iStream != null;
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        private TransmissionText transmissionTextListener;

        public void setTransmissionTextListener(TransmissionText transmissionTextListener) {
            this.transmissionTextListener = transmissionTextListener;
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            super.onPostExecute(result);
            String distance = "";
            if (result == null) return;

            if (result.size() < 1) {
                Utils.hideProgressDialog();
                return;
            }

            for (int i = 0; i < result.size(); i++) {
                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {
                        distance = point.get("distance");
                        continue;
                    }
                }
            }

            if (!distance.equals("")) {
                String resulDistance = String.valueOf((Double.parseDouble(distance)) / 1000);
                String dist = resulDistance.length() > 5 ? resulDistance.substring(0, 5) : resulDistance;
                transmissionTextListener.transmissionText(dist.replace(",", "."));
            }
        }
    }

    public String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }
}