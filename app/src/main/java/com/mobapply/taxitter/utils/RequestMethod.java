package com.mobapply.taxitter.utils;


public enum RequestMethod {
    POST,
    GET,
    PUT,
    DELETE;
}