package com.mobapply.taxitter.custom;

import java.util.Calendar;

public class TimePeriod {
    final private Calendar start;
    final private Calendar end;

    public TimePeriod(Calendar startDate, Calendar endDate) {
        if(startDate != null)
            start = (Calendar) startDate.clone();
        else
            start = null;

        if(endDate != null && startDate.before(endDate))
            end = (Calendar) endDate.clone();
        else
            end = null;
    }

    public boolean isInPeriod(Calendar date) {
        if((start == null || start.before(date) || start.equals(date))
                && (end == null || end.after(date) || end.equals(date)))
            return true;

        return false;
    }

    public Calendar trimDateByPeriod(Calendar date) {
        if(date == null) return date;

        if(start != null && date.before(start))
            return (Calendar) start.clone();

        if(end != null && date.after(end))
            return (Calendar) end.clone();

        return date;
    }
}