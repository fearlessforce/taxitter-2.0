package com.mobapply.taxitter.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import android.widget.ViewSwitcher;

import com.mobapply.taxitter.R;

import java.util.Calendar;

public class CustomDateTimePicker implements OnClickListener {
    private DatePicker datePicker;
    private TimePicker timePicker;
    private ViewSwitcher viewSwitcher;
    private Calendar calendar_date = null;
    private Activity activity;
    private Dialog dialog;
    private ICustomDateTimeListener iCustomDateTimeListener = null;
    private TimePeriod constraints;
    private Button btn_setDate, btn_setTime, btn_set, btn_cancel, btn_now;

    private boolean is24HourView = true;
    private boolean isAutoDismiss = true;
    private final int SET_DATE = 100, SET_TIME = 101, SET = 102, CANCEL = 103, NOW = 104;

    public CustomDateTimePicker(Activity a, ICustomDateTimeListener customDateTimeListener) {
        activity = a;
        iCustomDateTimeListener = customDateTimeListener;
        dialog = new Dialog(activity);
        dialog.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                resetData();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View dialogView = getDateTimePickerLayout();
        dialog.setContentView(dialogView);
    }

    public View getDateTimePickerLayout() {
        LinearLayout.LayoutParams linear_match_wrap = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams linear_wrap_wrap = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        FrameLayout.LayoutParams frame_match_wrap = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams button_params = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);

        LinearLayout linear_main = new LinearLayout(activity);
        linear_main.setLayoutParams(linear_match_wrap);
        linear_main.setOrientation(LinearLayout.VERTICAL);
        linear_main.setGravity(Gravity.CENTER);

        LinearLayout linear_child = new LinearLayout(activity);
        linear_child.setLayoutParams(linear_wrap_wrap);
        linear_child.setOrientation(LinearLayout.VERTICAL);

        LinearLayout linear_top = new LinearLayout(activity);
        linear_top.setLayoutParams(linear_match_wrap);

        btn_setDate = new Button(activity);
        btn_setDate.setLayoutParams(button_params);
        btn_setDate.setText(activity.getString(R.string.picker_date));
        btn_setDate.setId(SET_DATE);
        btn_setDate.setOnClickListener(this);

        btn_setTime = new Button(activity);
        btn_setTime.setLayoutParams(button_params);
        btn_setTime.setText(activity.getString(R.string.picker_time));
        btn_setTime.setId(SET_TIME);
        btn_setTime.setOnClickListener(this);

        linear_top.addView(btn_setDate);
        linear_top.addView(btn_setTime);

        viewSwitcher = new ViewSwitcher(activity);
        viewSwitcher.setLayoutParams(frame_match_wrap);

        datePicker = new DatePicker(activity);

        Calendar tmp = Calendar.getInstance();
        datePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        datePicker.init(tmp.get(Calendar.YEAR), tmp.get(Calendar.MONTH), tmp.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar_date.set(Calendar.YEAR, year);
                calendar_date.set(Calendar.MONTH, monthOfYear);
                calendar_date.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if (constraints != null)
                    calendar_date = constraints.trimDateByPeriod(calendar_date);

                if (year != calendar_date.get(Calendar.YEAR) || monthOfYear != calendar_date.get(Calendar.MONTH) ||
                        dayOfMonth != calendar_date.get(Calendar.DAY_OF_MONTH))
                    updateDate(calendar_date);
            }
        });

        timePicker = new TimePicker(activity);
        timePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);
        timePicker.setOnTimeChangedListener(new OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                calendar_date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar_date.set(Calendar.MINUTE, minute);

                if(constraints != null)
                    calendar_date = constraints.trimDateByPeriod(calendar_date);

                if(hourOfDay != calendar_date.get(Calendar.HOUR_OF_DAY) || minute != calendar_date.get(Calendar.MINUTE))
                    updateTime(calendar_date);
            }
        });

        viewSwitcher.addView(timePicker);
        viewSwitcher.addView(datePicker);

        LinearLayout linear_bottom = new LinearLayout(activity);
        linear_match_wrap.topMargin = 8;
        linear_bottom.setLayoutParams(linear_match_wrap);

        btn_set = new Button(activity);
        btn_set.setLayoutParams(button_params);
        btn_set.setText(activity.getString(R.string.picker_save));
        btn_set.setId(SET);
        btn_set.setOnClickListener(this);

//        btn_now = new Button(activity);
//        btn_now.setLayoutParams(button_params);
//        btn_now.setText(activity.getString(R.string.picker_now));
//        btn_now.setId(NOW);
//        btn_now.setOnClickListener(this);

        btn_cancel = new Button(activity);
        btn_cancel.setLayoutParams(button_params);
        btn_cancel.setText(activity.getString(R.string.picker_cancel));
        btn_cancel.setId(CANCEL);
        btn_cancel.setOnClickListener(this);

        linear_bottom.addView(btn_set);
//        linear_bottom.addView(btn_now);
        linear_bottom.addView(btn_cancel);


        linear_child.addView(linear_top);
        linear_child.addView(viewSwitcher);
        linear_child.addView(linear_bottom);

        linear_main.addView(linear_child);

        return linear_main;
    }

    public void showDialog() {
        if (!dialog.isShowing()) {
            if (calendar_date == null)
                calendar_date = Calendar.getInstance();

            calendar_date.set(Calendar.MILLISECOND, 0);
            calendar_date.set(Calendar.SECOND, 0);

            timePicker.setIs24HourView(is24HourView);
            timePicker.setCurrentHour(calendar_date.get(Calendar.HOUR_OF_DAY));
            timePicker.setCurrentMinute(calendar_date.get(Calendar.MINUTE));

            datePicker.updateDate(calendar_date.get(Calendar.YEAR),calendar_date.get(Calendar.MONTH),
                    calendar_date.get(Calendar.DATE));

            dialog.show();
            btn_setDate.performClick();
        }
    }

    public void setConstraints(TimePeriod period) {
        constraints = period;
    }

    public void setAutoDismiss(boolean isAutoDismiss) {
        this.isAutoDismiss = isAutoDismiss;
    }

    public void dismissDialog() {
        if (!dialog.isShowing())
            dialog.dismiss();
    }

    public void setDate(Calendar calendar) {
        if (calendar != null)
            calendar_date = calendar;
    }

    public void set24HourFormat(boolean is24HourFormat) {
        is24HourView = is24HourFormat;
    }

    public interface ICustomDateTimeListener {
        public void onSet(Dialog dialog, int year, int month, int date, int hour24, int min, int sec);
        public void onNow(Dialog dialog);
        public void onCancel();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case SET_DATE:
                btn_setTime.setEnabled(true);
                btn_setDate.setEnabled(false);
                viewSwitcher.showNext();
                break;

            case SET_TIME:
                updateTime(calendar_date);
                btn_setTime.setEnabled(false);
                btn_setDate.setEnabled(true);
                viewSwitcher.showPrevious();
                break;

            case SET:
                if (iCustomDateTimeListener != null) {
                    iCustomDateTimeListener.onSet(dialog,
                            calendar_date.get(Calendar.YEAR),
                            calendar_date.get(Calendar.MONTH),
                            calendar_date.get(Calendar.DAY_OF_MONTH),
                            calendar_date.get(Calendar.HOUR_OF_DAY),
                            calendar_date.get(Calendar.MINUTE),
                            calendar_date.get(Calendar.SECOND));
                }

                if (dialog.isShowing() && isAutoDismiss)
                    dialog.dismiss();
                break;


            case NOW:
                if (iCustomDateTimeListener != null) {
                    iCustomDateTimeListener.onNow(dialog);
                }
                if (dialog.isShowing() && isAutoDismiss)
                    dialog.dismiss();
                break;
            case CANCEL:
                if (iCustomDateTimeListener != null)
                    iCustomDateTimeListener.onCancel();

                if (dialog.isShowing())
                    dialog.dismiss();
                break;
        }
    }

    private void updateTime(Calendar calendar) {
        timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
        timePicker.invalidate();
    }

    private void updateDate(Calendar calendar) {
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePicker.invalidate();
    }

    private void resetData() {
        calendar_date = null;
        is24HourView = true;
    }
}