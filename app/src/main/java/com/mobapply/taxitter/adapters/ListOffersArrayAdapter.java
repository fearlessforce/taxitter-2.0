package com.mobapply.taxitter.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Proposal;
import com.mobapply.taxitter.utils.Utils;

import java.util.ArrayList;

public class ListOffersArrayAdapter extends ArrayAdapter<Proposal> {
    private Context context;
    private ArrayList<Proposal> proposalListArrayList;
    private ViewHolder viewHolder;

    public ListOffersArrayAdapter(Activity context, ArrayList<Proposal> proposalListArrayList) {
        super(context, R.layout.list_order, proposalListArrayList);
        this.context = context;
        this.proposalListArrayList = proposalListArrayList;
    }

    static class ViewHolder {
        public TextView fromWhereTextView, whereToTextView;
        public TextView commentTextView;
        public TextView costTextView;
        public TextView lengthTextView;
        public TextView expensiveTextView;
        public ImageView imageViewA, imageViewB, imageViewC;
        public ImageView imageViewCondition1, imageViewCondition2, imageViewCondition3;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_order_item, parent,false);
        viewHolder = new ViewHolder();

        if (position >= 0) {
            viewHolder.fromWhereTextView = (TextView) view.findViewById(R.id.autocomplTextStreet);
            viewHolder.whereToTextView = (TextView) view.findViewById(R.id.textViewWhereToTopOrder);
            viewHolder.commentTextView = (TextView) view.findViewById(R.id.textViewComment);
            viewHolder.costTextView = (TextView) view.findViewById(R.id.textViewCost);
            viewHolder.lengthTextView = (TextView) view.findViewById(R.id.textViewLength);
            viewHolder.imageViewA = (ImageView) view.findViewById(R.id.imageViewA);
            viewHolder.imageViewB = (ImageView) view.findViewById(R.id.imageViewB);
            viewHolder.imageViewC = (ImageView) view.findViewById(R.id.imageViewC);
            viewHolder.imageViewCondition1 = (ImageView) view.findViewById(R.id.imageViewCondition1);
            viewHolder.imageViewCondition2 = (ImageView) view.findViewById(R.id.imageViewCondition2);
            viewHolder.imageViewCondition3 = (ImageView) view.findViewById(R.id.imageViewCondition3);
            viewHolder.expensiveTextView = (TextView) view.findViewById(R.id.textViewExpensive);

            view.setTag(viewHolder);

            if (proposalListArrayList.get(position).getDeparture() != null)
                viewHolder.fromWhereTextView.setText(proposalListArrayList.get(position).getDeparture());

            ArrayList<AdditionalRoute> additionalRoutes = proposalListArrayList.get(position).getAdditionalRoutes();

            if (additionalRoutes != null) {
                for (int i = 0; i < additionalRoutes.size(); i++) {
                    viewHolder.whereToTextView.setText(additionalRoutes.get(i).getWayaddress());

                    if (i > 0)
                        viewHolder.imageViewC.setVisibility(View.VISIBLE);
                }
            }

            viewHolder.costTextView.setText(proposalListArrayList.get(position).getCash().length() == 0 ?
                    "0" : proposalListArrayList.get(position).getCash());

            if(proposalListArrayList.get(position).getCash().length() == 0 ){
                viewHolder.costTextView.setVisibility(View.INVISIBLE);
            }else{
                viewHolder.costTextView.setVisibility(View.VISIBLE);
            }

            if (proposalListArrayList.get(position).getLength() != null && !proposalListArrayList.get(position).getLength().equals(""))
                viewHolder.lengthTextView.setText(proposalListArrayList.get(position).getLength().length() != 0 ?
                        proposalListArrayList.get(position).getLength() + " км" : "0 км ");

            if (proposalListArrayList.get(position).getComment() != null && !proposalListArrayList.get(position).getComment().equals("")) {
                viewHolder.commentTextView.setVisibility(View.VISIBLE);
                viewHolder.commentTextView.setText(proposalListArrayList.get(position).getComment());
            }

            Utils.checkConditions(proposalListArrayList.get(position).getBabyseat(), proposalListArrayList.get(position).getAc(),
                    proposalListArrayList.get(position).getNonsmoking(), viewHolder.imageViewCondition1,
                    viewHolder.imageViewCondition2, viewHolder.imageViewCondition3);
        }

        return view;
    }

    @Override
    public int getCount() {
        return proposalListArrayList.size();
    }

    @Override
    public Proposal getItem(int i) {
        return proposalListArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}