package com.mobapply.taxitter.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.utils.Utils;

import java.util.ArrayList;

public class ListOrderArrayAdapter extends ArrayAdapter<Order> {
    private final String EXPENSIVE = "0";
    private Context context;
    private ArrayList<Order> order;
    private ViewHolder viewHolder;

    public ListOrderArrayAdapter(Activity context, ArrayList<Order> order) {
        super(context, R.layout.list_order, order);
        this.context = context;
        this.order = order;
    }

    static class ViewHolder {
        public TextView fromWhereTextView, whereToTextView;
        public TextView commentTextView;
        public TextView costTextView;
        public TextView lengthTextView;
        public TextView expensiveTextView;
        public ImageView imageViewA, imageViewB, imageViewC;
        public ImageView imageViewCondition1, imageViewCondition2, imageViewCondition3;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_order_item, parent,false);
        viewHolder = new ViewHolder();

        if (position >= 0) {
            viewHolder.fromWhereTextView = (TextView) view.findViewById(R.id.autocomplTextStreet);
            viewHolder.whereToTextView = (TextView) view.findViewById(R.id.textViewWhereToTopOrder);
            viewHolder.commentTextView = (TextView) view.findViewById(R.id.textViewComment);
            viewHolder.costTextView = (TextView) view.findViewById(R.id.textViewCost);
            viewHolder.lengthTextView = (TextView) view.findViewById(R.id.textViewLength);
            viewHolder.imageViewA = (ImageView) view.findViewById(R.id.imageViewA);
            viewHolder.imageViewB = (ImageView) view.findViewById(R.id.imageViewB);
            viewHolder.imageViewC = (ImageView) view.findViewById(R.id.imageViewC);
            viewHolder.imageViewCondition1 = (ImageView) view.findViewById(R.id.imageViewCondition1);
            viewHolder.imageViewCondition2 = (ImageView) view.findViewById(R.id.imageViewCondition2);
            viewHolder.imageViewCondition3 = (ImageView) view.findViewById(R.id.imageViewCondition3);
            viewHolder.expensiveTextView = (TextView) view.findViewById(R.id.textViewExpensive);

            view.setTag(viewHolder);

            if (order.get(position).getDeparture() != null)
                viewHolder.fromWhereTextView.setText(order.get(position).getDeparture());

            ArrayList<AdditionalRoute> additionalRoutes = order.get(position).getAdditionalRoutes();

            if (additionalRoutes != null) {
                for (int i = 0; i < additionalRoutes.size(); i++) {
                    viewHolder.whereToTextView.setText(additionalRoutes.get(i).getWayaddress());

                    if (i > 0)
                        viewHolder.imageViewC.setVisibility(View.VISIBLE);
                }
            }

            viewHolder.costTextView.setText(order.get(position).getCash().length() == 0 ?
                    "0" : order.get(position).getCash());

            if (order.get(position).getLength() != null && !order.get(position).getLength().equals(""))
                viewHolder.lengthTextView.setText(order.get(position).getLength().length() != 0 ?
                        order.get(position).getLength() + " км" : "0 км ");

            if (order.get(position).getComment() != null && !order.get(position).getComment().equals("")) {
                viewHolder.commentTextView.setVisibility(View.VISIBLE);
                viewHolder.commentTextView.setText(order.get(position).getComment());
            }

            if (order.get(position).getStatusOffer() != null &&
                    order.get(position).getStatusOffer().equals(EXPENSIVE) ) {
                viewHolder.expensiveTextView.setVisibility(View.VISIBLE);
                viewHolder.costTextView.setTextColor(context.getResources().getColor(R.color.dark_red));
            }

            Utils.checkConditions(order.get(position).getBabyseat(), order.get(position).getAc(),
                    order.get(position).getNonsmoking(), viewHolder.imageViewCondition1,
                    viewHolder.imageViewCondition2, viewHolder.imageViewCondition3);

            viewHolder.costTextView.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public int getCount() {
        return order.size();
    }

    @Override
    public Order getItem(int i) {
        return order.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}