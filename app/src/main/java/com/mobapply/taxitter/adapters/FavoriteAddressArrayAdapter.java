package com.mobapply.taxitter.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.model.FavoriteAddress;

import java.util.ArrayList;

/**
 * Created by Misha Nester on 28.04.15.
 */
public class FavoriteAddressArrayAdapter extends ArrayAdapter<FavoriteAddress> {
    private Activity context;
    private ArrayList<FavoriteAddress> favoriteAddressArrayList;
    private ViewHolder holder;

    public FavoriteAddressArrayAdapter(Activity context, ArrayList<FavoriteAddress> favoriteAddressArrayList) {
        super(context, R.layout.list_layout, favoriteAddressArrayList);
        this.context = context;
        this.favoriteAddressArrayList = favoriteAddressArrayList;
    }

    static class ViewHolder {
        public TextView nameAddressTextView;
        public TextView addressTextView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.favorite_address_layout, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.nameAddressTextView = (TextView) rowView.findViewById(R.id.textViewNameAddress);
            viewHolder.addressTextView = (TextView) rowView.findViewById(R.id.textViewAddress);
            rowView.setTag(viewHolder);
        }

        holder = (ViewHolder) rowView.getTag();

        String street = favoriteAddressArrayList.get(position).getStreet();
        String house = favoriteAddressArrayList.get(position).getHouse_num();
        String door = favoriteAddressArrayList.get(position).getDoor();
        String address = street;

        if (house.length() != 0)
            address = street + ", " + house;

        if (door.length() != 0)
            address += ", " + door;

        holder.nameAddressTextView.setText(favoriteAddressArrayList.get(position).getName());
        holder.addressTextView.setText(address);

        return rowView;
    }

    @Override
    public int getCount() {
        return favoriteAddressArrayList.size();
    }

    @Override
    public FavoriteAddress getItem(int i) {
        return favoriteAddressArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}