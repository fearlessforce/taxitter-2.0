package com.mobapply.taxitter.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.utils.Utils;

import java.util.ArrayList;

public class ListOrdersArrayAdapter extends ArrayAdapter<Order> {
    private final String WAIT_RESPONSE = "2";
    private final String APPROVED = "1";
    private Context context;
    private ArrayList<Order> ordersArrayList;
    private ViewHolder viewHolder;

    public ListOrdersArrayAdapter(Activity context, ArrayList<Order> ordersArrayList) {
        super(context, R.layout.list_order, ordersArrayList);
        this.context = context;
        this.ordersArrayList = ordersArrayList;
    }

    static class ViewHolder {
        public TextView fromWhereTextView, whereToTextView;
        public TextView commentTextView;
        public TextView costTextView;
        public TextView lengthTextView;
        public TextView expensiveTextView;
        public ImageView imageViewA, imageViewB, imageViewC;
        public ImageView imageViewCondition1, imageViewCondition2, imageViewCondition3;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_order_item, parent,false);
        viewHolder = new ViewHolder();

        if (position >= 0 && position <= ordersArrayList.size()) {
            viewHolder.fromWhereTextView = (TextView) view.findViewById(R.id.autocomplTextStreet);
            viewHolder.whereToTextView = (TextView) view.findViewById(R.id.textViewWhereToTopOrder);
            viewHolder.commentTextView = (TextView) view.findViewById(R.id.textViewComment);
            viewHolder.costTextView = (TextView) view.findViewById(R.id.textViewCost);
            viewHolder.lengthTextView = (TextView) view.findViewById(R.id.textViewLength);
            viewHolder.imageViewA = (ImageView) view.findViewById(R.id.imageViewA);
            viewHolder.imageViewB = (ImageView) view.findViewById(R.id.imageViewB);
            viewHolder.imageViewC = (ImageView) view.findViewById(R.id.imageViewC);
            viewHolder.imageViewCondition1 = (ImageView) view.findViewById(R.id.imageViewCondition1);
            viewHolder.imageViewCondition2 = (ImageView) view.findViewById(R.id.imageViewCondition2);
            viewHolder.imageViewCondition3 = (ImageView) view.findViewById(R.id.imageViewCondition3);
            viewHolder.expensiveTextView = (TextView) view.findViewById(R.id.textViewExpensive);
            view.setTag(viewHolder);

            if (ordersArrayList.get(position).getDeparture() != null)
                viewHolder.fromWhereTextView.setText(ordersArrayList.get(position).getDeparture());

            ArrayList<AdditionalRoute> additionalRoutes = ordersArrayList.get(position).getAdditionalRoutes();

            if (additionalRoutes != null) {
                for (int i = 0; i < additionalRoutes.size(); i++) {
                    viewHolder.whereToTextView.setText(additionalRoutes.get(i).getWayaddress());

                    if (i > 0)
                        viewHolder.imageViewC.setVisibility(View.VISIBLE);
                }
            }

            viewHolder.costTextView.setText(ordersArrayList.get(position).getCash_d().length() == 0 ?
                    "0" : ordersArrayList.get(position).getCash_d());

            if (ordersArrayList.get(position).getLength() != null && !ordersArrayList.get(position).getLength().equals(""))
                viewHolder.lengthTextView.setText(ordersArrayList.get(position).getLength().length() != 0 ?
                        ordersArrayList.get(position).getLength() + " км" : "0 км ");

            if (ordersArrayList.get(position).getComment() != null && !ordersArrayList.get(position).getComment().equals("")) {
                viewHolder.commentTextView.setVisibility(View.VISIBLE);
                viewHolder.commentTextView.setText(ordersArrayList.get(position).getComment());
            }

            if (ordersArrayList.get(position).getStatus_offer().equals(APPROVED)) {
                viewHolder.expensiveTextView.setBackgroundResource(R.drawable.confirm_orders_item);

            } else {
                if (ordersArrayList.get(position).getStatus_offer() != null) {
                    viewHolder.expensiveTextView.setBackgroundResource(ordersArrayList.get(position).getStatus_offer()
                            .equals(WAIT_RESPONSE) ? R.drawable.confirm_orders_item : R.drawable.wait_response);
                }
            }

            viewHolder.expensiveTextView.setVisibility(View.VISIBLE);

            Utils.checkConditions(ordersArrayList.get(position).getBabyseat(), ordersArrayList.get(position).getAc(),
                    ordersArrayList.get(position).getNonsmoking(), viewHolder.imageViewCondition1,
                    viewHolder.imageViewCondition2, viewHolder.imageViewCondition3);
        }

        return view;
    }

    @Override
    public int getCount() {
        return ordersArrayList.size();
    }

    @Override
    public Order getItem(int i) {
        return ordersArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}