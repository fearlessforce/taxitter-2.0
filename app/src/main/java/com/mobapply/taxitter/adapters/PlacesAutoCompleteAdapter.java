package com.mobapply.taxitter.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.flurry.sdk.is;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Misha Nester on 30.04.15.
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
    private final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private final String OUT_JSON = "/json";
    private ArrayList<String> resultList = new ArrayList<String>();
    private ArrayList<String> placeIdList = new ArrayList<String>();

    private SharedPreferences sharedPreferences;

    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), 0);
    }

    @Override
    public int getCount() {
        if (resultList == null) {
            return 0;
        } else {
            return resultList.size();
        }
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {



            @Override
            protected Filter.FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultList = autocomplete(constraint.toString());
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }
    public String getPlaceId(int index) {
        return placeIdList.get(index);
    }

    /**
     * request get streets name for autocompleteTextView
     *
     * @param input string for search
     * @return resultList - list of values streets
     */
    public ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?input=");
            sb.append(URLEncoder.encode(Constants.PROFILE_COUNTRY, "UTF-8"));
          //  sb.append(URLEncoder.encode(sharedPreferences.getString(Constants.AREA_AND_CITY, ""), "UTF-8"));
            sb.append(URLEncoder.encode(sharedPreferences.getString(Constants.REGION_D, ""), "UTF-8"));
            sb.append(URLEncoder.encode(",", "UTF-8"));
            sb.append(URLEncoder.encode(input, "UTF-8"));
           // sb.append("&types=geocode");
            float cityLatitude =sharedPreferences.getFloat(Constants.CITY_LATITUDE, 0);
            float cityLongitude =sharedPreferences.getFloat(Constants.CITY_LONGITUDE, 0);
            if(cityLatitude != 0 && cityLongitude != 0) {
                sb.append("&location=" + cityLatitude + "," + cityLongitude);
            }
            int cityRadius =sharedPreferences.getInt(Constants.CITY_RADIUS, 0);
            if(cityRadius >0) {
                sb.append("&radius=" + cityRadius * 1000);
            }
            sb.append("&regions=locality");
            sb.append("&components=country:ru");
            sb.append("&language=ru");
            sb.append("&key=" + Constants.AUTOCOMPLETE_KEY);

            Log.d("TAG", sb.toString());

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }

        } catch (MalformedURLException e) {
            Log.e("TAG", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("TAG", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            resultList = new ArrayList<String>(predsJsonArray.length());
            placeIdList.clear();
            for (int i = 0; i < predsJsonArray.length(); i++) {
                boolean hasCity = false;
                boolean isObject = false;
                JSONObject js = predsJsonArray.getJSONObject(i);

                placeIdList.add(js.getString("place_id"));

                JSONArray typesJsonArray1 = js.getJSONArray("types");
                for (int t = 0; t < typesJsonArray1.length(); t++) {
                    String type = typesJsonArray1.getString(t);
                    if (type.equals("establishment")) {
                        isObject = true;
                        break;
                    }
                }
                JSONArray predsJsonArray1 = js.getJSONArray("terms");
                for (int g = 0; g < predsJsonArray1.length(); g++) {
                    String valueCity = predsJsonArray1.getJSONObject(g).getString("value");
                    if (valueCity.equals(sharedPreferences.getString(Constants.CITY_D, ""))) {
                        hasCity = true;
                    }
                }
                String valueSt = isObject
                        ? predsJsonArray1.getJSONObject(0).getString("value") +" ("+ predsJsonArray1.getJSONObject(1).getString("value")+")" +", " + predsJsonArray1.getJSONObject(2).getString("value")
                        : predsJsonArray1.getJSONObject(0).getString("value") +", " + predsJsonArray1.getJSONObject(1).getString("value");
                if (hasCity) {
                    resultList.add(valueSt);
                }
            }

        } catch (JSONException e) {
            Log.e("TAG", "Cannot process JSON results", e);
        }
        return resultList;
    }
}