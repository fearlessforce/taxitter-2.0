package com.mobapply.taxitter.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.SwipeButtonClickListener;
import com.mobapply.taxitter.model.Driver;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;

import java.util.ArrayList;

public class ListViewSwipeAdapter extends BaseSwipeAdapter {
    private Context context;
    private ArrayList<Driver> offerDriversArrayList;
    private SwipeButtonClickListener swipeButtonClickListener;
    private View view;
    private SwipeLayout swipeLayout;
    private ImageButton acceptImgButton, rejectImgButton;
    private ImageView imageViewCondition1, imageViewCondition2, imageViewCondition3;
    private TextView carTextView;
    private TextView carDetailsTextView;
    private TextView carModelTextView;
    private TextView ratingTextView, countRouteTextView, commentTextView;
    private TextView cashTextView;

    public ListViewSwipeAdapter(Context context, ArrayList<Driver> offerDriversArrayList) {
        this.context = context;
        this.offerDriversArrayList = offerDriversArrayList;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, ViewGroup parent) {
        view = LayoutInflater.from(context).inflate(R.layout.list_proposal_item, null);
        return view;
    }

    @Override
    public void fillValues(final int position, View convertView) {
        swipeLayout = (SwipeLayout)view.findViewById(getSwipeLayoutResourceId(position));
        countRouteTextView = (TextView) swipeLayout.findViewById(R.id.textViewCountComment);
        commentTextView = (TextView) view.findViewById(R.id.textViewComment);
        ratingTextView = (TextView) swipeLayout.findViewById(R.id.textViewRating);
        cashTextView = (TextView) swipeLayout.findViewById(R.id.textViewCost);
        acceptImgButton = (ImageButton) swipeLayout.findViewById(R.id.imageButtonAccept);
        rejectImgButton = (ImageButton) swipeLayout.findViewById(R.id.rejectImgButton);
        carTextView = (TextView) swipeLayout.findViewById(R.id.textViewCarModel);
        carModelTextView = (TextView) swipeLayout.findViewById(R.id.textViewCarModel);
        carDetailsTextView = (TextView) swipeLayout.findViewById(R.id.textViewCarDetail);
        imageViewCondition1 = (ImageView) swipeLayout.findViewById(R.id.imageViewCondition1);
        imageViewCondition2 = (ImageView) swipeLayout.findViewById(R.id.imageViewCondition2);
        imageViewCondition3 = (ImageView) swipeLayout.findViewById(R.id.imageViewCondition3);
        swipeLayout.close();

        countRouteTextView.setText(offerDriversArrayList.get(position).getTrips_d());
        carTextView.setText(offerDriversArrayList.get(position).getCar_d());
        carTextView.setText(offerDriversArrayList.get(position).getCar_d() + " " + offerDriversArrayList.get(position).getCarmodel_d());

        if (offerDriversArrayList.get(position).getStatus_d().equals(Constants.PASSENGER_ACCEPT_ORDER)) {
            acceptImgButton.setBackgroundResource(R.drawable.accept_img_pressed);
            acceptImgButton.setEnabled(false);

        } else if (offerDriversArrayList.get(position).getStatus_d().equals(Constants.PASSENGER_DRIVER_ACCEPT_ORDER)) {
            acceptImgButton.setBackgroundResource(R.drawable.accept_img_pressed);
            acceptImgButton.setEnabled(false);
            rejectImgButton.setEnabled(false);
            carDetailsTextView.setVisibility(View.VISIBLE);
            setCarDetailText(position);
        }

        acceptImgButton.setOnClickListener(new SwipeLayout.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeButtonClickListener.onAcceptClickButton(position);
            }
        });

        rejectImgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swipeButtonClickListener.onRejectClickButton(position);
            }
        });

        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.imageButtonAccept));
            }
        });

        ratingTextView.setText(offerDriversArrayList.get(position).getRating_d().equals("") ? "0"
                : offerDriversArrayList.get(position).getRating_d());
        cashTextView.setText(offerDriversArrayList.get(position).getCash_d() + "");

        if (offerDriversArrayList.get(position).getComment_d() != null && !offerDriversArrayList.get(position).getComment_d().equals("")) {
            commentTextView.setVisibility(View.VISIBLE);
            commentTextView.setText(offerDriversArrayList.get(position).getComment_d());
        } else {
            commentTextView.setVisibility(View.GONE);
        }

        Utils.checkConditions(offerDriversArrayList.get(position).getBabyseat_d(), offerDriversArrayList.get(position).getAc_d(),
                offerDriversArrayList.get(position).getNonsmoking_d(), imageViewCondition1, imageViewCondition2,
                imageViewCondition3
        );
    }

//    private void setCarDetailText(int position) {
//        if (!offerDriversArrayList.get(position).getCarcolor_d().equals("") && !offerDriversArrayList.get(position).getCarnumber_d().equals("")) {
//            carDetailsTextView.setText(offerDriversArrayList.get(position).getCarcolor_d() + ", " + offerDriversArrayList.get(position).getCarnumber_d());
//
//        } else if (offerDriversArrayList.get(position).getCarcolor_d().equals("") && !offerDriversArrayList.get(position).getCarnumber_d().equals("")) {
//            carDetailsTextView.setText(offerDriversArrayList.get(position).getCarnumber_d());
//
//        } else if (!offerDriversArrayList.get(position).getCarcolor_d().equals("") && offerDriversArrayList.get(position).getCarnumber_d().equals("")) {
//            carDetailsTextView.setText(offerDriversArrayList.get(position).getCarcolor_d());
//        }
//    }


    private void setCarDetailText(int position) {
         if (!offerDriversArrayList.get(position).getCarcolor_d().equals("")) {
            carDetailsTextView.setText(offerDriversArrayList.get(position).getCarcolor_d());
         } else{
            carDetailsTextView.setText("");
         }
    }

    @Override
    public int getCount() {
        return offerDriversArrayList.size();
    }

    @Override
    public SwipeLayout getItem(int position) {
        swipeLayout = (SwipeLayout)view.findViewById(getSwipeLayoutResourceId(position));
        return swipeLayout;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void setButtonsListener(SwipeButtonClickListener swipeButtonClickListener) {
        this.swipeButtonClickListener = swipeButtonClickListener;
    }
}