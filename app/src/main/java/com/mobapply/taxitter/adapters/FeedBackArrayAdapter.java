package com.mobapply.taxitter.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.model.FeedBackItem;
import com.mobapply.taxitter.utils.Constants;

import java.util.ArrayList;

/**
 * Created by Misha Nester on 05.05.15.
 */
public class FeedBackArrayAdapter extends ArrayAdapter<FeedBackItem>  {
    private Activity context;
    private ArrayList<FeedBackItem> feedBackArrayList;
    private ViewHolder holder;
    private ImageLoader imageLoader;

    public FeedBackArrayAdapter(Activity context, ArrayList<FeedBackItem> feedBackArrayList) {
        super(context, R.layout.rating_reviews_dialog, feedBackArrayList);
        this.context = context;
        this.feedBackArrayList = feedBackArrayList;
        imageLoader = ImageLoader.getInstance();
    }

    static class ViewHolder {
        public TextView carNameTextView;
        public TextView commentOrderTextView;
        public ImageView photoImageView;
        public ImageView star0ImageView;
        public ImageView star1ImageView;
        public ImageView star2ImageView;
        public ImageView star3ImageView;
        public ImageView star4ImageView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.rating_reviews_list_item, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.carNameTextView = (TextView) rowView.findViewById(R.id.textViewCarName);
            viewHolder.commentOrderTextView = (TextView) rowView.findViewById(R.id.textViewCommentOrder);
            viewHolder.photoImageView = (ImageView) rowView.findViewById(R.id.imageViewPhoto);
            viewHolder.star0ImageView = (ImageView) rowView.findViewById(R.id.imageViewStar0);
            viewHolder.star1ImageView = (ImageView) rowView.findViewById(R.id.imageViewStar1);
            viewHolder.star2ImageView = (ImageView) rowView.findViewById(R.id.imageViewStar2);
            viewHolder.star3ImageView = (ImageView) rowView.findViewById(R.id.imageViewStar3);
            viewHolder.star4ImageView = (ImageView) rowView.findViewById(R.id.imageViewStar4);

            rowView.setTag(viewHolder);
        }

        holder = (ViewHolder) rowView.getTag();

        String carName = feedBackArrayList.get(position).getUser();
        holder.carNameTextView.setText(carName);

        String url = feedBackArrayList.get(position).getAvatar();
        if (url != null)
            imageLoader.DisplayImage(url, holder.photoImageView, Constants.IMG_SMALL_SIZE);

        String rating = feedBackArrayList.get(position).getRating();

        holder.star0ImageView.setVisibility(View.GONE);
        holder.star1ImageView.setVisibility(View.GONE);
        holder.star2ImageView.setVisibility(View.GONE);
        holder.star3ImageView.setVisibility(View.GONE);
        holder.star4ImageView.setVisibility(View.GONE);

        int i = Integer.parseInt(rating);
        ArrayList<ImageView> imageViews = new ArrayList<ImageView>();
        imageViews.add(holder.star0ImageView);
        imageViews.add(holder.star1ImageView);
        imageViews.add(holder.star2ImageView);
        imageViews.add(holder.star3ImageView);
        imageViews.add(holder.star4ImageView);

        for (int n = 0; n < i; n++) {
            imageViews.get(n).setVisibility(View.VISIBLE);
        }

        holder.commentOrderTextView.setVisibility(View.GONE);

        if (feedBackArrayList.get(position).getFeedback().length() != 0) {
            holder.commentOrderTextView.setVisibility(View.VISIBLE);
            holder.commentOrderTextView.setText(feedBackArrayList.get(position).getFeedback());
        }

        Log.d("TAG", "rating: " + feedBackArrayList.get(position).getRating());
        Log.d("TAG", "feedback: " + feedBackArrayList.get(position).getFeedback());
        Log.d("TAG", " ");
        rowView.setOnClickListener(null);

        return rowView;
    }

    @Override
    public int getCount() {
        return feedBackArrayList.size();
    }

    @Override
    public FeedBackItem getItem(int i) {
        return feedBackArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}
