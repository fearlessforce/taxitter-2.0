package com.mobapply.taxitter.gcm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import java.util.ArrayList;

/**
 * Created by Misha Nester on 01.05.15.
 */
public class RatingPopupActivity extends Activity implements View.OnClickListener{
    private ArrayList<ImageButton> starButtonArray;
    private SharedPreferences sharedPreferences;
    private ImageButton star0;
    private ImageButton star1;
    private ImageButton star2;
    private ImageButton star3;
    private ImageButton star4;
    private EditText commentEditText;
    private String rating = "0";
    private String order_uid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialog_layout = inflater.inflate(R.layout.popup_rate_route_dialog, null);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        commentEditText = (EditText)dialog_layout.findViewById(R.id.editTextCommentTrip);
        star0 = (ImageButton)dialog_layout.findViewById(R.id.buttonStar0);
        star1 = (ImageButton)dialog_layout.findViewById(R.id.buttonStar1);
        star2 = (ImageButton)dialog_layout.findViewById(R.id.buttonStar2);
        star3 = (ImageButton)dialog_layout.findViewById(R.id.buttonStar3);
        star4 = (ImageButton)dialog_layout.findViewById(R.id.buttonStar4);

        starButtonArray = new ArrayList<ImageButton>();
        starButtonArray.add(star0);
        starButtonArray.add(star1);
        starButtonArray.add(star2);
        starButtonArray.add(star3);
        starButtonArray.add(star4);

        for (int i = 0; i < starButtonArray.size(); i ++) {
            starButtonArray.get(i).setOnClickListener(this);
        }

        Intent intent = getIntent();
        if (intent.hasExtra(Constants.ORDER_UID)) {
            order_uid = intent.getStringExtra(Constants.ORDER_UID);
        }

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setView(dialog_layout);
        final AlertDialog alert = alertDialog.create();
        alert.show();

        ((Button)dialog_layout.findViewById(R.id.buttonOk)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rating.equals("0")) {
                    Utils.showToast(getApplicationContext(), getString(R.string.rate_route));
                } else {
                    PassengerRequester passengerRequester = new PassengerRequester(RatingPopupActivity.this);
                    passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                        @Override
                        public void onMessageSuccess(String str) {
                            Log.d("TAG", "SUCCESS");
                        }

                        @Override
                        public void onMessageError(int errorCode) {
                            Log.d("TAG", "ERROR");
                        }
                    });
                    passengerRequester.setRating(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), order_uid,
                            rating, commentEditText.getText().toString());
                    cancelDialog();
                    alert.cancel();

                }
            }
        });

        ((Button)dialog_layout.findViewById(R.id.buttonRefuse)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDialog();
                alert.cancel();
            }
        });
    }

    private void cancelDialog() {
        sharedPreferences.edit().putBoolean(Constants.DRIVER_ACTION_ONE_OR_THREE, true).apply();
        Intent intentM = new Intent(RatingPopupActivity.this, MainFragmentActivity.class);
        intentM.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentM);
        RatingPopupActivity.this.finish();
    }

    @Override
    public void onClick(View view) {
        int intTag = Integer.parseInt(view.getTag().toString());
        rating = String.valueOf(intTag + 1);

        for (int i = 0; i < starButtonArray.size(); i ++) {
            ((ImageButton)starButtonArray.get(i)).setBackgroundDrawable(getResources().getDrawable(R.drawable.star_rating));
        }

        for (int i = 0; i <= intTag; i ++) {
            ((ImageButton)starButtonArray.get(i)).setBackgroundDrawable(getResources().getDrawable(R.drawable.star_rating_pressed));
        }
    }
}