package com.mobapply.taxitter.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.TransmissionText;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.ui.driver.DriverFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.DistanceCalculation;
import com.mobapply.taxitter.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;

public class GcmIntentService extends IntentService {
    private SharedPreferences sharedPreferences, sharedPref;
    private final String DRIVER_ON_THE_PLACE = "1";
    private final String PAS_ACCEPT_DRIVER_PROPOSAL = "2";
    private final String GO_ROUTE = "3";
    private final String PASSENGER_CANCEL_ORDER = "4";
    private final String NEW_ORDER = "5";
    private final String RATING_TRIP = "6";
    private final String NEW_DRIVER_PROPOSAL = "7";

    String carnumber_d = "";
    String carmodel_d = "";
    String carcolor_d = "";
    String carInfo = "";
    String whereTo = "";
    String fromWhere = "";
    String length = "";
    String cash = "";
    double latitude = 0;
    double longitude = 0;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getSharedPreferences(getString(R.string.pref_role_key), 0);
        Bundle extras = intent.getExtras();

        if (!extras.isEmpty()) {
            String action_id  = "";
            try {
                if (extras.containsKey("action_id"))
                    action_id = URLDecoder.decode(extras.getString("action_id"), "UTF-8");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (action_id.equals(DRIVER_ON_THE_PLACE)) {
                Utils.playAlertMedia(getApplicationContext());
                Intent intentDialog = new Intent(this, ServiceDialog.class);
                intentDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentDialog.putExtra(Constants.PUSH_DATA, extras);
                startActivity(intentDialog);

            } else if (action_id.equals(RATING_TRIP)) {
                Intent intentDialog = new Intent(this, RatingPopupActivity.class);
                intentDialog.putExtra(Constants.ORDER_UID, extras.getString("order_uid"));
                intentDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentDialog);

            } else if (action_id.equals(PAS_ACCEPT_DRIVER_PROPOSAL)) {
                sharedPreferences.edit().putBoolean(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL, true).apply();
                Intent intentP = new Intent();
                intentP.setAction(Constants.BROADCAST_ACTION);
                intentP.putExtra(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL, true);
                sharedPreferences.edit().putString(Constants.ORDER_ID, extras.getString("order_id")).apply();

                if (sharedPreferences.getBoolean(Constants.APP_OPENED, false))
                    sendBroadcast(intentP);
                else
                    Utils.sendNotification(getApplicationContext(), getString(R.string.passenger_accept_driver_proposal), "");

            } else if (action_id.equals(GO_ROUTE)) {
                sharedPreferences.edit().putBoolean(Constants.DRIVER_ACTION_ONE_OR_THREE, true).apply();
                try {
                    carmodel_d = URLDecoder.decode(extras.getString(Constants.CARMODEL_D), "UTF-8");
                    carnumber_d = URLDecoder.decode(extras.getString(Constants.CARNUMBER_D), "UTF-8");
                    carcolor_d = URLDecoder.decode(extras.getString(Constants.CARCOLOR_D), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                carInfo = carmodel_d.isEmpty() ? "" : getString(R.string.car_push) + " " + carmodel_d;

                if (carmodel_d.isEmpty())
                    carInfo = getString(R.string.color_lower_case) + " " + carcolor_d;
                else
                    carInfo = carInfo.concat(carcolor_d.isEmpty() ? "" : ", " + getString(R.string.color_lower_case) + " " + carcolor_d);

                carInfo = carInfo.concat(carnumber_d.isEmpty() ? "" : ", " + getString(R.string.state_number_push) + " " + carnumber_d);

                Utils.sendNotification(getApplicationContext(), getString(R.string.driver_accept_pass_order), carInfo);
                Intent intentGo = new Intent();
                intentGo.setAction(Constants.BROADCAST_ACTION);
                intentGo.putExtra(Constants.GO_ROUTE, true);
                intentGo.putExtra(Constants.CAR_INFO, carInfo);
                sendBroadcast(intentGo);

            } else if (action_id.equals(PASSENGER_CANCEL_ORDER)) {
                sharedPreferences.edit().putBoolean(Constants.PASSENGER_CANCEL_ORDER, true).apply();
                Intent intentP = new Intent();
                intentP.setAction(Constants.BROADCAST_ACTION);
                intentP.putExtra(Constants.PASSENGER_CANCEL_ORDER, true);
                sendBroadcast(intentP);

                Intent intentDialog = new Intent(this, CancelOrderPopupActivity.class);
                intentDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentDialog);

                String from_a = "";
                String to_a = "";
                try {
                    from_a = URLDecoder.decode(extras.getString("from_a"), "UTF-8");
                    to_a = URLDecoder.decode(extras.getString("to_a"), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                String route = getString(R.string.from_where_push) + " " + from_a + ", " + getString(R.string.where_to_push) + " " + to_a;

                sharedPreferences.edit().putBoolean(Constants.NOTIFICATION_PASSENGER_CANCEL_ORDER, true).apply();
                sharedPreferences.edit().putBoolean(Constants.PASS_SEET_IN_CAR, false).apply();
                Utils.sendNotification(getApplicationContext(), getString(R.string.passenger_cancel_order), route);

            } else if (action_id.equals(NEW_ORDER)) {
                if (sharedPreferences.getBoolean(Constants.NEW_ORDER_PUSH, false)) {
                    if (!DriverFragmentActivity.ACTIVE && sharedPref.getString(Constants.ROLE_SATE, "")
                            .equals(Constants.PASSENGER_ROLE)) {
                        try {
                            cash = URLDecoder.decode(extras.getString("cash"), "UTF-8");
                            latitude = Double.parseDouble(URLDecoder.decode(extras.getString("latitude"), "UTF-8").replace(",","."));
                            longitude = Double.parseDouble(URLDecoder.decode(extras.getString("longitude"), "UTF-8").replace(",","."));
                            length = URLDecoder.decode(extras.getString("length"), "UTF-8");
                            whereTo = URLDecoder.decode(extras.getString("arrival"), "UTF-8");
                            fromWhere = URLDecoder.decode(extras.getString("departure"), "UTF-8");

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        DistanceCalculation distanceCalculation = new DistanceCalculation(getApplicationContext(), latitude, longitude);
                        distanceCalculation.setTransmissionTextListener(new TransmissionText() {
                            @Override
                            public void transmissionText(String distance) {
                                sharedPreferences.edit().putBoolean(Constants.NEW_ORDER, true).apply();
                                double d = Double.parseDouble(distance);
                                distance = new DecimalFormat("##.##").format(d);
                                sendNotification(getApplicationContext(), distance, fromWhere, whereTo, length, cash);
                            }
                        });
                    }
                }
            } else if (action_id.equals(NEW_DRIVER_PROPOSAL)) {
                sharedPreferences.edit().putBoolean(Constants.NEW_DRIVER_PROPOSAL, true).apply();
                Utils.sendNotification(getApplicationContext(), getString(R.string.new_driver_proposal), carInfo);
                Intent intentP = new Intent();
                intentP.setAction(Constants.BROADCAST_ACTION);
                intentP.putExtra(Constants.NEW_DRIVER_PROPOSAL, true);
                sendBroadcast(intentP);
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public static void sendNotification(Context context, String kilometrs, String fromWhere, String whereTo,
                                        String length, String cash) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_taxitter_ico)
                .setPriority(Notification.FLAG_HIGH_PRIORITY)
                .setContentIntent(getPendingIntent(context))
                .setDefaults(Notification.DEFAULT_SOUND);

        Notification notification = new NotificationCompat.InboxStyle(builder)
                .addLine(context.getString(R.string.request_distance_from_you) + " " + kilometrs
                        + " " + context.getString(R.string.km_with_two_points))
                .addLine(context.getString(R.string.from) + " " + fromWhere + ",")
                .addLine(context.getString(R.string.to) + " " + whereTo + ",")
                .addLine(length + " " + context.getString(R.string.km) + ", " + context.getString(R.string.price)
                        + " " +cash + ".")
                .build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        NotificationManager notificationManager = Utils.getNotificationManager(context);
        notificationManager.notify(Constants.NOTIFICATION_ID, notification);
    }

    public static PendingIntent getPendingIntent(Context context) {
        final Intent notificationIntent = new Intent(context, MainFragmentActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        return resultPendingIntent;
    }
}