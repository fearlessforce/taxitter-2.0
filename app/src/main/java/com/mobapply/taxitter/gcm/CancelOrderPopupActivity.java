package com.mobapply.taxitter.gcm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.ui.driver.DriverFragmentActivity;
import com.mobapply.taxitter.utils.Constants;

/**
 * Created by Misha Nester on 21.05.15.
 */
public class CancelOrderPopupActivity extends Activity {
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPreferences.edit().putBoolean(Constants.PASSENGER_CANCEL_ORDER_DIALOG, true).apply();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.setMessage(getString(R.string.passenger_cancel_order_dialog));

        alertDialog.setPositiveButton(getString(R.string.new_orders), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (!DriverFragmentActivity.ACTIVE) {
                    Intent intentM = new Intent(CancelOrderPopupActivity.this, MainFragmentActivity.class);
                    intentM.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentM);
                }
                sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER).apply();
                sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
                CancelOrderPopupActivity.this.finish();
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER).apply();
                sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
                CancelOrderPopupActivity.this.finish();
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}