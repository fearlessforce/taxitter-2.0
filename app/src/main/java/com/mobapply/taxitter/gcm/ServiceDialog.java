package com.mobapply.taxitter.gcm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.Window;
import android.view.WindowManager;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ServiceDialog extends Activity {
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);

        Intent intent = getIntent();
        String carnumber_d = "";
        String carmodel_d = "";
        String carcolor_d = "";
        String carInfo = "";

        if(intent.hasExtra(Constants.PUSH_DATA)) {
            Bundle extras = intent.getBundleExtra(Constants.PUSH_DATA);

            try {
                carmodel_d = URLDecoder.decode(extras.getString(Constants.CARMODEL_D), "UTF-8");
                carnumber_d = URLDecoder.decode(extras.getString(Constants.CARNUMBER_D), "UTF-8");
                carcolor_d = URLDecoder.decode(extras.getString(Constants.CARCOLOR_D), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            carInfo = carmodel_d.isEmpty() ? "" : getString(R.string.car_push) + " " + carmodel_d;

            if (carmodel_d.isEmpty())
                carInfo = getString(R.string.color_lower_case) + " " + carcolor_d;
            else
                carInfo = carInfo.concat(carcolor_d.isEmpty() ? "" : ", " + getString(R.string.color_lower_case) + " " + carcolor_d);

            carInfo = carInfo.concat(carnumber_d.isEmpty() ? "" : ", " + getString(R.string.state_number_push) + " " + carnumber_d);

        }

        KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("My_App");
        kl.disableKeyguard();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_launcher);
        alertDialog.setMessage(getString(R.string.driver_on_the_place).concat("\n" + carInfo));
        alertDialog.setPositiveButton(getString(R.string.view_info), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Utils.stopAlertMedia(ServiceDialog.this);
                sharedPreferences.edit().putBoolean(Constants.DRIVER_ACTION_ONE_OR_THREE, true).apply();
                Intent intentM = new Intent(ServiceDialog.this, MainFragmentActivity.class);
                intentM.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentM);
                ServiceDialog.this.finish();
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Utils.stopAlertMedia(ServiceDialog.this);
                ServiceDialog.this.finish();
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void onAttachedToWindow() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }
}