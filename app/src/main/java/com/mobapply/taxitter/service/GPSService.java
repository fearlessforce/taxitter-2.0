package com.mobapply.taxitter.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.webconnect.DriverRequester;


/**
 * Created by apelipets on 9/11/15.
 */
public class GPSService extends Service implements android.location.LocationListener{


    private static final String TAG = "GPSService";

    private final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private final long MIN_TIME_BW_UPDATES = 20*1000 ;
    private Location location;
    private LocationManager locationManager;

    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean canGetLocation = false;
    private double latitude;
    private double longitude;

    private SharedPreferences sharedPreferences;
    private DriverRequester driverRequester;

    private Handler handler;


    Runnable stopServiceCallback = new Runnable() {
        @Override
        public void run() {
            stopSelf();
        }
    };

    private void init(){

        try {
            handler = new Handler();
            handler.postDelayed(stopServiceCallback, 30*60*1000);
            sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
            driverRequester = new DriverRequester(this);
            driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                @Override
                public void onMessageSuccess(String str) {
                    Log.d(TAG, "setupUserPosition : succes - " +str);
                }

                @Override
                public void onMessageError(int errorCode) {
                    Log.d(TAG, "setupUserPosition : error - " +errorCode);
                }
            });
            locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                this.canGetLocation = false;
            } else {
                this.canGetLocation = true;

                locationManager.requestLocationUpdates(isNetworkEnabled ? LocationManager.NETWORK_PROVIDER : LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void release(){
        handler.removeCallbacks(stopServiceCallback);
        locationManager.removeUpdates(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }


    @Override
    public void onDestroy() {
        release();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, location.toString());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        driverRequester.setupUserPosition(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), latitude, longitude);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
