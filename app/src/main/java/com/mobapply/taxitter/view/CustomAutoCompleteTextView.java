package com.mobapply.taxitter.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

/**
 * Created by apelipets on 3/23/16.
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView{

    public CustomAutoCompleteTextView(Context context) {
        super(context);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        ListAdapter adapter =  getAdapter();
        if(adapter != null && adapter instanceof BaseAdapter){
            ( (BaseAdapter)adapter).notifyDataSetChanged();
        }
        super.performFiltering(text, keyCode);
    }
}
