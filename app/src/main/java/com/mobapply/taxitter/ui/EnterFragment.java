package com.mobapply.taxitter.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.custom.ExtendedEditText;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.AuthenticationRequester;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Fragment authorization. Save profileData when authorization responseCode success
 */
public class EnterFragment extends Fragment {
    private final int PHONE_LENGTH = 27;
    private final int MAX_PHONE_LENGTH = 26;

    private SharedPreferences sharedPreferences, sharedPref;
    private FragmentManager fragmentManager;
    private InputFilter[] inputFiltersArray;
    private View view;
    private ExtendedEditText phoneNumberEditText;
    private ExtendedEditText passwordEditText;
    private Button enterButton;
    private Button resetPassButton;
    private String phone;
    private String password;
    private String phoneNumber = "";
    private boolean isRegister;
    private int maxLength;
    private String reg_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.enter_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getActivity().getSharedPreferences(getString(R.string.pref_role_key), 0);
        fragmentManager = getFragmentManager();
        sharedPref.edit().putString(Constants.ROLE_SATE, "0").apply();
        setupListener(view);
        initUI(view);

        phoneNumberEditText.setText("+7 (_ _ _) _ _ _  _ _  _ _");
        phoneNumberEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        passwordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        maxLength = MAX_PHONE_LENGTH;
        inputFiltersArray = new InputFilter[1];
        inputFiltersArray[0] = new InputFilter.LengthFilter(maxLength);

        if (phoneNumber.length() != 0)
            phoneNumberEditText.setText(phoneNumber);

        if (isRegister) {
            passwordEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_NORMAL);
            phoneNumberEditText.setText(phoneNumber);
            passwordEditText.setText(password);
        }

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;

                if (result == EditorInfo.IME_ACTION_DONE) {
                    clearKeyboardAndFocus(enterButton);

                } else if (result == EditorInfo.IME_ACTION_NEXT) {
                    clearKeyboardAndFocus(enterButton);

                }
                return false;
            }
        });

        passwordEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    clearKeyboardAndFocus(enterButton);
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    clearKeyboardAndFocus(enterButton);

                    if (isRegister)
                        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    return true;
                }
                return false;
            }
        });

        phoneNumberEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocused) {
                if (isFocused)
                    setSelecionForPhoneNumber();
            }
        });

        phoneNumberEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelecionForPhoneNumber();
            }
        });

        phoneNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (phoneNumberEditText.toString().contains("_")) {
                    maxLength = PHONE_LENGTH;
                } else {
                    maxLength = MAX_PHONE_LENGTH;
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!phoneNumberEditText.getText().toString().contains("_")) {
                    maxLength = MAX_PHONE_LENGTH;
                    setFilter();

                } else {
                    maxLength = PHONE_LENGTH;
                    setFilter();
                }

//              Replacement symbol on number
                int p = 0;
                if (s.length() >= PHONE_LENGTH) {
                    String edittext = s.toString();

                    while (p < s.length()) {
                        if (s.charAt(p) == '_') {

                            if (p == 9 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 2);

                            } else if (p == 12 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 1);

                            } else if (p == 16 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 2);

                            } else if (p == 21 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 2);

                            } else if (edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);

                                if (p != 26)
                                    phoneNumberEditText.setSelection(p + 1);
                            }

                            if (phoneNumberEditText.getText().toString().contains("_"))
                                phoneNumberEditText.setSelection(phoneNumberEditText.getText().toString().indexOf("_"));

                            break;
                        } else {
                            p++;
                        }
                    }
                }
            }
        });

        phoneNumberEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

//              Replacement number on symbol if was KEYCODE_DEL
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    int selection = phoneNumberEditText.getSelectionEnd();

                    if (selection >= 5) {
                        String phoneNumber = phoneNumberEditText.getText().toString();
                        String str1 = phoneNumber.substring(0, selection - 1);
                        String subChar = phoneNumber.substring(selection - 1, selection);
                        String str2 = phoneNumber.substring(selection);

                        if (isDigit(subChar)) {
                            phoneNumberEditText.setText(str1 + "_" + str2);
                            phoneNumberEditText.setSelection(str1.length());

                        } else if (subChar.equals(" ")) {
                            if (isDigit(phoneNumber.substring(selection - 2, selection - 1))) {
                                phoneNumberEditText.setText(phoneNumber.substring(0, selection - 2) + "_ " + str2);
                                phoneNumberEditText.setSelection(phoneNumber.substring(0, selection - 2).length());

                            } else if (phoneNumber.substring(selection - 2, selection - 1).equals(" ")) {
                                phoneNumberEditText.setText(phoneNumber.substring(0, selection - 3) + "_  " + str2);
                                phoneNumberEditText.setSelection(phoneNumber.substring(0, selection - 3).length());

                            } else if (phoneNumber.substring(selection - 2, selection - 1).equals(")")) {
                                phoneNumberEditText.setText(phoneNumber.substring(0, selection - 3) + "_) " + str2);
                                phoneNumberEditText.setSelection(phoneNumber.substring(0, selection - 3).length());
                            }
                        }

                        if (phoneNumberEditText.getText().toString().contains("_")) {
                            maxLength = PHONE_LENGTH;
                            setFilter();
                        }
                    }

                    if (phoneNumberEditText.getText().toString().contains("_"))
                        phoneNumberEditText.setSelection(phoneNumberEditText.getText().toString().indexOf("_"));

                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    clearKeyboardAndFocus(passwordEditText);
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    clearKeyboardAndFocus(enterButton);
                    return true;
                }
                return false;
            }
        });

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reg_id = sharedPref.getString(Constants.PROPERTY_REG_ID, "");
                getTextFromField();

                if (reg_id != null && !reg_id.isEmpty()) {
                    authorization();
                } else {
                    RegistrGCMAsynckTask registrGCMAsynckTask = new RegistrGCMAsynckTask();
                    registrGCMAsynckTask.execute();
                    Utils.showProgressDialog(getActivity(), getString(R.string.loading));
                }
            }
        });

        resetPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTextFromField();

                AuthenticationRequester authenticationRequester = new AuthenticationRequester(getActivity());
                authenticationRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        Utils.hideProgressDialog();
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        checkResetPassErrorCode(errorCode);
                    }
                });
                authenticationRequester.ressetPassword(phone);
                Utils.showProgressDialog(getActivity(), getString(R.string.loading));
            }
        });

        return view;
    }

    private void authorization() {
        AuthenticationRequester authenticationRequester = new AuthenticationRequester(getActivity());
        authenticationRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                    Utils.hideProgressDialog();

                    if (!str.equals(Constants.OK)) {
                        try {
                            JSONObject jsonObject = new JSONObject(str);

                            if (jsonObject.length() != 0) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Constants.ACCESS_TOKEN, jsonObject.getString(Constants.ACCESS_TOKEN));
                                editor.putString(Constants.CITY_D, jsonObject.getString(Constants.CITY_D));
                                editor.putString(Constants.REGION_D, jsonObject.getString(Constants.REGION_D));
                                editor.putString(Constants.AREA_AND_CITY, jsonObject.getString(Constants.REGION_D)
                                        + ", " + jsonObject.getString(Constants.CITY_D));
                                editor.putString(Constants.NICKNAME, jsonObject.getString(Constants.NICKNAME));
                                editor.putString(Constants.MAXRADIUS_D, jsonObject.getString(Constants.MAXRADIUS_D));
                                editor.putString(Constants.CAR_D, jsonObject.getString(Constants.CAR_D));
                                editor.putString(Constants.CARMODEL_D, jsonObject.getString(Constants.CARMODEL_D));
                                editor.putString(Constants.CARTYPE_D, jsonObject.getString(Constants.CARTYPE_D));
                                editor.putString(Constants.CARCOLOR_D, jsonObject.getString(Constants.CARCOLOR_D));
                                editor.putString(Constants.CARYEAR_D, jsonObject.getString(Constants.CARYEAR_D));
                                editor.putString(Constants.CARNUMBER_D, jsonObject.getString(Constants.CARNUMBER_D));
                                editor.putString(Constants.QUANTITY_D, jsonObject.getString(Constants.QUANTITY_D));
                                editor.putInt(Constants.BABYSEAT_D, jsonObject.getInt(Constants.BABYSEAT_D));
                                editor.putInt(Constants.AC_D, jsonObject.getInt(Constants.AC_D));
                                editor.putInt(Constants.NOSMOKING_D, jsonObject.getInt(Constants.NOSMOKING_D));
                                editor.putFloat(Constants.CITY_LATITUDE, (float) jsonObject.getDouble(Constants.CITY_LATITUDE));
                                editor.putFloat(Constants.CITY_LONGITUDE, (float) jsonObject.getDouble(Constants.CITY_LONGITUDE));
                                editor.putInt(Constants.CITY_RADIUS, jsonObject.getInt(Constants.CITY_RADIUS));
                                editor.apply();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    ChoiceOfRolesFragment choiceOfRolesFragment = new ChoiceOfRolesFragment();
                    fragmentManager.beginTransaction()
                            .add(R.id.container, choiceOfRolesFragment, Constants.CHOICE_ROLE_TAG)
                            .addToBackStack(Constants.CHOICE_ROLE_TAG)
                            .commit();

                    if (isRegister)
                        passwordEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);

            }

            @Override
            public void onMessageError(int errorCode) {
                checkAuthorizationErrorCode(errorCode);
            }
        });

        authenticationRequester.authorize(phone, password, reg_id);
        Utils.showProgressDialog(getActivity(), getString(R.string.loading));
    }

    public class RegistrGCMAsynckTask extends AsyncTask<String, String, String> {
        String msg = "";
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getActivity());

        public RegistrGCMAsynckTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(getActivity().getApplicationContext());
                }
                reg_id = gcm.register(((MainFragmentActivity)getActivity()).SENDER_ID);
                msg = "Device registered, registration ID=" + reg_id;
                // Persist the registration ID - no need to register again.

            } catch (IOException ex) {
                msg = ex.getMessage();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            Utils.hideProgressDialog();
            if (reg_id.isEmpty() && msg.equals("SERVICE_NOT_AVAILABLE")) {
                Utils.showToast(getActivity(), getString(R.string.no_network));
            } else {
                storeRegistrationId(reg_id);
                authorization();
            }
        }
    }

    private void storeRegistrationId(String regId) {
        sharedPref.edit().putString(Constants.PROPERTY_REG_ID, regId).apply();
    }

    private void setFilter() {
        inputFiltersArray = new InputFilter[1];
        inputFiltersArray[0] = new InputFilter.LengthFilter(maxLength);
        phoneNumberEditText.setFilters(inputFiltersArray);
    }

    private void initUI(View view) {
        phoneNumberEditText = (ExtendedEditText)view.findViewById(R.id.editTextPhoneNumberForEnter);
        passwordEditText = (ExtendedEditText)view.findViewById(R.id.editTextPasswordForEnter);
        enterButton = (Button)view.findViewById(R.id.buttonEnterInMain);
        resetPassButton = (Button)view.findViewById(R.id.buttonResetPass);
    }

    private void clearKeyboardAndFocus(View v) {
        Utils.hideKeyboard(getActivity());
        passwordEditText.clearFocus();
        v.requestFocus();
    }

    private void setSelecionForPhoneNumber() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String str = phoneNumberEditText.getText().toString();
                int index = str.indexOf("_");

                if (str.contains("_"))
                    phoneNumberEditText.setSelection(index);
            }
        }, 200);
    }

    private boolean isDigit(String oneChar) {
        for (int i = 0; i < 10; i++) {
            if (oneChar.equals(String.valueOf(i)))
                return true;
        }
        return false;
    }

    private void checkResetPassErrorCode(int errorCode) {
        Utils.hideProgressDialog();

        if (errorCode == Constants.FAILED_TO_SEND_SMS_PASSWORD) {
            Utils.showToast(getActivity(), getString(R.string.failed_to_send_sms_password));

        } else if (errorCode == Constants.NO_RECORDS_PASS_FOR_USER) {
            Utils.showToast(getActivity(), getString(R.string.no_records_pass_for_user));

        } else {
            othersError(errorCode);
        }
    }

    private void checkAuthorizationErrorCode(int errorCode) {
        Utils.hideProgressDialog();

        if (errorCode == Constants.WRONG_PASSWORD) {
            Utils.showToast(getActivity(), getString(R.string.wrong_password));

        } else if (errorCode == Constants.NO_USER_IN_DB) {
            Utils.showToast(getActivity(), getString(R.string.no_user_in_db));

        } else {
            othersError(errorCode);
        }
    }

    private void othersError(int errorCode) {
        if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getString(R.string.not_all_fields));

        } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
            Utils.showToast(getActivity(), getString(R.string.json_error));

        } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), getString(R.string.no_network));

        } else {
            Utils.showToast(getActivity(), getString(R.string.code) + errorCode);
        }
    }

    private void getTextFromField() {
        phone = getPhoneNumber(phoneNumberEditText.getText().toString());
        password = passwordEditText.getText().toString();
    }

    /**
     * Get phone number with trim all space and other symbols
     * @param stringFromEditText
     * @return phone number
     */
    private String getPhoneNumber(String stringFromEditText) {
        return stringFromEditText.replaceAll("[^a-zA-Z0-9]","");
    }

    public void setPhone(String phone) {
        phoneNumber = phone;
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(getActivity());
                    passwordEditText.clearFocus();
                    phoneNumberEditText.clearFocus();
                    enterButton.requestFocus();
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }

    public void setUserData(String phoneNumber, String password, boolean isRegister) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.isRegister = isRegister;
    }


}