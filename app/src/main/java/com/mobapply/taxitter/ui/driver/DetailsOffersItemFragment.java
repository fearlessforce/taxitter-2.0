package com.mobapply.taxitter.ui.driver;


import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.service.GPSService;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.DriverRequester;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

/**
 * The screen that displays detailed information about offerItem
 * from listViewListOrders on TabWidget-#2 with status_offer 2 or 3.
 *
 * @see ListOrdersOnTabTwoFragment
 */
public class DetailsOffersItemFragment extends Fragment {
    private final String REFUSE_OFFER = "2";
    private final String CONFIRM_OFFER = "1";

    private FragmentManager fragmentManager;
    private View view;
    private Order itemOffer;
    private ArrayList<AdditionalRoute> additionalRoutes;
    private SharedPreferences sharedPreferences;
    private CircularProgressBar circularProgressBar;
    private ImageLoader imageLoader;
    private Dialog dialog;

    private TextView titleWhenTextView, whenGoTextView;
    private TextView fromWhereTopTextView;
    private TextView whereToTopTextView;
    private TextView nickNameTextView;
    private TextView ratingTextView;
    private TextView countPassengerTopTextView, countPassengerTextView;
    private TextView commentTopTextView, commentTextView;
    private TextView streetTextView, houseTextView, entranceTextView;
    private ImageView photoOrderImageView;
    private Button refuseButton, confirmButton;
    private Button closeButton;
    private ImageView imageViewLargePhoto;

    private String number;
    private String id;
    private String order_uid;
    private String cash_d;
    private String length;
    private String latitude, longitude;
    private String when, departure;
    private String comment, comment_d;
    private String uid_client;
    private String client;
    private int rating, quantity;
    private int nonsmoking, babyseat, ac;
    private boolean isNeedDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.offer_main_fragment, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        fragmentManager = getFragmentManager();
        imageLoader = ImageLoader.getInstance();

        initUI();
        getValueFromArray(itemOffer);
        createDialog();
        setDataIntoUI();
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);

        if (itemOffer.getStatusOffer().equals(Constants.DRIVER_ORDERS_ACCEPTED_BY_THE_PASSENGER))
            confirmButton.setVisibility(View.VISIBLE);

        refuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DriverRequester driverRequester = new DriverRequester(getActivity());
                driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        hideCircularBar();
                        ListOrdersOnTabTwoFragment.setIsNeedRefresh(true);
                        fragmentManager.popBackStack();
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        hideCircularBar();

                        if (driverRequester.checkAcceptOrderErrorCode(getActivity(), errorCode).length() != 0)
                            Utils.showToast(getActivity(), driverRequester.checkAcceptOrderErrorCode(getActivity(), errorCode));

                        backForAuthorization(errorCode);
                    }
                });
                driverRequester.confirmRefuseOffers(getCookie(), itemOffer.getOrder_uid(), REFUSE_OFFER,
                        itemOffer.getCash_d().equals("") ? "0" : cash_d);
                showCircularBar();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DriverRequester driverRequester = new DriverRequester(getActivity());
                driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        hideCircularBar();
                        getActivity().startService(new Intent(getActivity(), GPSService.class));
                        CurrentDriverOrder currentDriverOrder = new CurrentDriverOrder();
                        currentDriverOrder.setDetailItem(itemOffer);
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragments_container, currentDriverOrder, Constants.CURRENT_ORDER)
                                .addToBackStack(Constants.CURRENT_ORDER)
                                .commit();
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        hideCircularBar();

                        if (driverRequester.checkAcceptOrderErrorCode(getActivity(), errorCode).length() != 0)
                            Utils.showToast(getActivity(), driverRequester.checkAcceptOrderErrorCode(getActivity(), errorCode));

                        backForAuthorization(errorCode);
                    }
                });
                driverRequester.confirmRefuseOffers(getCookie(), itemOffer.getOrder_uid(), CONFIRM_OFFER,
                        itemOffer.getCash_d().equals("") ? "0" : cash_d);
                showCircularBar();
            }
        });

        photoOrderImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });

                if ((Integer)imageViewLargePhoto.getTag() != R.drawable.round67) {
                    dialog.show();

                } else {
                    getAvatarFromServer(Constants.IMG_LARGE_SIZE);
                    isNeedDialog = true;
                    Utils.showProgressDialog(getActivity(), getString(R.string.loading));
                }
            }
        });

        return view;
    }

    private void createDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.large_foto_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialog.findViewById(R.id.button_close);
        imageViewLargePhoto = (ImageView) dialog.findViewById(R.id.imageViewLargePhoto);
        imageViewLargePhoto.setTag(R.drawable.round67);
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, photoOrderImageView, size);
                                        }
                                    } else {
                                        imageLoader.DisplayImage(urlImage, imageViewLargePhoto, size);
                                        if (isNeedDialog && isVisible()) {
                                            Utils.hideProgressDialog();
                                            dialog.show();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.showToast(getActivity(), "" + errorCode);
            }
        });
        commonRequest.getOtherAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size, uid_client);
    }

    private void backForAuthorization(int errorCode) {
        if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            MainFragmentActivity.isAccessTokenGone(true);
            Utils.showToast(getActivity(), getString(R.string.not_find_user_with_access_token));
            getActivity().finish();
        }
    }

    private void hideCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).progressiveStop();
        circularProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).start();
        circularProgressBar.setVisibility(View.VISIBLE);
    }

    private void initUI() {
        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.smoothCircleBar);
        titleWhenTextView = (TextView)view.findViewById(R.id.textViewTitleOffer);
        fromWhereTopTextView = (TextView)view.findViewById(R.id.textViewFromWhereTopOffer);
        whereToTopTextView = (TextView)view.findViewById(R.id.textViewWhereToTopOffer);
        nickNameTextView = (TextView)view.findViewById(R.id.textViewNicknameOrder);
        ratingTextView = (TextView)view.findViewById(R.id.textViewRatingOrder);
        countPassengerTopTextView = (TextView)view.findViewById(R.id.textViewPassengerOrder);
        commentTopTextView = (TextView)view.findViewById(R.id.textViewCommentOrder);
        refuseButton = (Button)view.findViewById(R.id.buttonOfferRefuse);
        confirmButton = (Button)view.findViewById(R.id.buttonOfferGo);
        streetTextView = (TextView)view.findViewById(R.id.textViewStreet);
        houseTextView = (TextView)view.findViewById(R.id.editTextHouse);
        entranceTextView = (TextView)view.findViewById(R.id.editTextEntrance);
        commentTextView = (TextView)view.findViewById(R.id.textViewCommentTextOffer);
        countPassengerTextView = (TextView)view.findViewById(R.id.textViewCountPassengerOffer);
        whenGoTextView = (TextView)view.findViewById(R.id.textViewWhenOffer);
        photoOrderImageView = (ImageView)view.findViewById(R.id.imageViewPhotoOrder);
    }

    private String getCookie() {
        return sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
    }

    private void getValueFromArray(Order item) {
        order_uid = item.getOrder_uid() == null ? "" : item.getOrder_uid();
        when = item.getWhen() == null ? "" : item.getWhen();
        latitude = item.getLatitude() == null ? "" : item.getLatitude();
        longitude = item.getLongitude() == null ? "" : item.getLongitude();
        departure = item.getDeparture() == null ? "" : item.getDeparture();
        cash_d = item.getCash_d() == null ? "": item.getCash_d();
        comment = item.getComment() == null ? "" : item.getComment();
        comment_d = item.getComment_d() == null ? "" : item.getComment_d();
        length = item.getLength() == null ? "" : item.getLength();
        number = item.getNumber();
        rating = item.getRating();
        id = item.getId();
        babyseat = item.getBabyseat();
        ac = item.getAc();
        nonsmoking = item.getNonsmoking();
        quantity = item.getQuantity();
        additionalRoutes = item.getAdditionalRoutes();
        uid_client = item.getUid_client() == null ? "" : item.getUid_client();
        client = item.getClient() == null ? "" : item.getClient();
    }

    private void setDataIntoUI() {
        titleWhenTextView.setText(when.length() != 0 ? when : getString(R.string.soon));
        fromWhereTopTextView.setText(departure);

        String[] addressArray = departure.split(", ");

        if (addressArray.length >= 1)
            streetTextView.setText(addressArray[0]);

        if (addressArray.length >= 2)
            houseTextView.setText(addressArray[1]);

        if (addressArray.length >= 3)
            entranceTextView.setText(addressArray[2]);

        if (streetTextView.getText().toString().equals(""))
            streetTextView.setText("-");

        if (houseTextView.getText().toString().equals(""))
            houseTextView.setText("-");

        if (entranceTextView.getText().toString().equals(""))
            entranceTextView.setText("-");

        whereToTopTextView.setText(additionalRoutes.get(additionalRoutes.size() -1).getWayaddress());
        nickNameTextView.setText(client);
        ratingTextView.setText("" + rating);
        countPassengerTopTextView.setText("" + quantity);

        if (comment.length() != 0) {
            commentTopTextView.setVisibility(View.VISIBLE);
            commentTopTextView.setText(comment);
        }

        if (comment_d.length() != 0) {
            commentTextView.setVisibility(View.VISIBLE);
            commentTextView.setText(comment_d);
        }

        for (int i = 0; i <= additionalRoutes.size(); i++) {

            ViewGroup inclusionViewGroup = (ViewGroup)view.findViewById(R.id.linerDotsOffer);
            View child1 = LayoutInflater.from(getActivity()).inflate(
                    R.layout.offer_dot_item, null);
            inclusionViewGroup.addView(child1);
            TextView left = (TextView)child1.findViewById(R.id.textViewDotLeftNumber);
            TextView right = (TextView)child1.findViewById(R.id.textViewDotText);
            ImageView imageDoterLine = (ImageView)child1.findViewById(R.id.imageViewDoterLineOffer);

            if (i == additionalRoutes.size()) {
                left.setText("");
                right.setText(length.length() == 0 ? "" : length + " км" );
                right.setTextSize(14);
                right.setTextColor(getResources().getColor(R.color.white_gray));
                imageDoterLine.setVisibility(View.INVISIBLE);

            } else {
                left.setText("" + (i + 1));
                right.setText(additionalRoutes.get(i).getWayaddress());
            }
        }
        countPassengerTextView.setText(quantity + " " + getString(R.string.passengers));
        whenGoTextView.setText(when.length() != 0 ? when : getString(R.string.soon));
    }

    public void setDetailItem(Order item) {
        this.itemOffer = item;
    }
}