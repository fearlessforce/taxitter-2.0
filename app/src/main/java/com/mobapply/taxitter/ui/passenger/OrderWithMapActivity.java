package com.mobapply.taxitter.ui.passenger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.android.gms.maps.model.LatLng;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.PlacesAutoCompleteAdapter;
import com.mobapply.taxitter.interfaces.CountCoordinatesListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.Address;
import com.mobapply.taxitter.model.FavoriteAddress;
import com.mobapply.taxitter.service.GPSTracker;
import com.mobapply.taxitter.ui.BaseActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.GetCoordinates;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import se.walkercrou.places.AddressComponent;
import se.walkercrou.places.Place;

public class OrderWithMapActivity extends BaseActivity implements View.OnClickListener {
    private FragmentManager fragmentManager;
    private GPSTracker gps;
    private View mCustomView;
    private RelativeLayout entranceRelativeLayout;
    private TextView getMyLocation;
    private TextView streetTextView, houseTextView, entranceTextView;
    private AutoCompleteTextView streetAutoCompleteText, entranceAutocompleteText, houseAutoCompleteText;
    private Button orderBtn;
    private Button menuButton;
    private Button commentButton;
    private Button addFavoriteAddrImageView;
    private LinearLayout linearFavoriteAddress;
    private SharedPreferences sharedPreferences;
    private ArrayList<FavoriteAddress> favoriteAddressArray;
    private Address address;
    private Place addressListener;
    private TextView fromWhereTextView;

    private boolean isMenuVisible;
    private boolean isWhereTo;
    private boolean isFavorite = false;
    private String street;
    private String house_num;
    private String entrance;
    private double longitude;
    private double latitude;
    private boolean streetFilled = false;
    private boolean houseFilled = false;
    private boolean isObject = false;
    private String placeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passenger_activity_order_with_map);
        fragmentManager = getFragmentManager();
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        favoriteAddressArray = new ArrayList<FavoriteAddress>();
        ActionBar mActionBar = getActionBar();
        setupListener(findViewById(R.id.main));
        initUI();

        LayoutInflater mInflater = LayoutInflater.from(this);

        mCustomView = mInflater.inflate(R.layout.action_bar_custom, null);

       ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
               ActionBar.LayoutParams.MATCH_PARENT);

        menuButton = (Button) mCustomView.findViewById(R.id.buttonMenu);

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isMenuVisible == false) {
                    openMenuFragment();
                    setActionBarDark();

                } else {
                    fragmentManager.popBackStack();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setActionBarYellow();
                        }
                    }, 600);
                }
                isMenuVisible = !isMenuVisible;
            }
        });

        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            mActionBar.setDisplayShowCustomEnabled(true);
            mActionBar.setCustomView(mCustomView);
            View v = mActionBar.getCustomView();
            ViewGroup.LayoutParams lp = v.getLayoutParams();
            lp.width =  ViewGroup.LayoutParams.MATCH_PARENT;
            v.setLayoutParams(lp);
        }

        streetAutoCompleteText.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item));
        streetAutoCompleteText.setThreshold(3);

        final Intent intent = getIntent();

        if(intent != null) {
            address = (Address) intent.getExtras().getParcelable(Constants.ADDRESS);

            if (address != null) {
                streetAutoCompleteText.setText(address.getStreet());
                houseAutoCompleteText.setText(address.getHouse());
                entranceAutocompleteText.setText(address.getDoor());

                if (address.getIsFavorite() == 1) {
                    isFavorite = true;
                    longitude = address.getLongitude();
                    latitude = address.getLatitude();
                }
            }

            if (intent.getBooleanExtra(Constants.WHERE_TO, false))
                isWhereTo = true;
        }

        if (isWhereTo) {
            fromWhereTextView.setText(R.string.where_to);
            entranceRelativeLayout.setVisibility(View.GONE);
            commentButton.setVisibility(View.GONE);
        } else{
            fromWhereTextView.setText(R.string.from_where);
        }

        getTextFromField();

        if (!street.isEmpty() && !house_num.isEmpty()) {
            addFavoriteAddrImageView.setBackgroundResource(R.drawable.button_add_favaddress);
            orderBtn.setBackgroundResource(R.drawable.button_ready);
            addFavoriteAddrImageView.setEnabled(true);
            orderBtn.setEnabled(true);
        }

        PassengerRequester passengerRequester = new PassengerRequester(OrderWithMapActivity.this);
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                Utils.hideProgressDialog();
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject.length() != 0) {
                            JSONArray jsonArray = jsonObject.getJSONArray("address");

                            for (int i = 0; i < jsonArray.length(); i++){
                                LayoutInflater inflater = OrderWithMapActivity.this.getLayoutInflater();
                                View rowView = inflater.inflate(R.layout.favorite_address_layout, null);
                                TextView nameAddressTextView = (TextView) rowView.findViewById(R.id.textViewNameAddress);
                                TextView addressTextView = (TextView) rowView.findViewById(R.id.textViewAddress);

                                JSONObject jsObject = jsonArray.getJSONObject(i);
                                FavoriteAddress favoriteAddress = new FavoriteAddress(jsObject);
                                favoriteAddressArray.add(favoriteAddress);

                                String name = favoriteAddress.getName();
                                String street = favoriteAddress.getStreet();
                                String house = favoriteAddress.getHouse_num();
                                String door = favoriteAddress.getDoor();
                                String address = street;

                                if (house.length() != 0)
                                    address = street + ", " + house;

                                if (door.length() != 0)
                                    address += ", " + door;

                                nameAddressTextView.setText(name);
                                addressTextView.setText(address);
                                rowView.setTag(i);
                                rowView.setOnClickListener(OrderWithMapActivity.this);

                                linearFavoriteAddress.addView(rowView, i);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Log.d("ERROR", ": " + errorCode);
            }
        });
        passengerRequester.getFavoriteAddress(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));

        streetTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                streetAutoCompleteText.requestFocus();
                streetAutoCompleteText.setSelection(streetAutoCompleteText.getText().length());
                Utils.showKeyboard(OrderWithMapActivity.this, streetAutoCompleteText);
            }
        });

        streetAutoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                isFavorite = false;
                placeId = ((PlacesAutoCompleteAdapter)streetAutoCompleteText.getAdapter()).getPlaceId(i);
                final String streetFull = streetAutoCompleteText.getText().toString();

                final String street =streetFull.substring(0,streetFull.lastIndexOf(","));

//                String[] addressParts =streetFull.split(",");
//                String street = null;
//                if(addressParts.length>2) {
//                    street =addressParts[addressParts.length-2];
//                }else{
//                    street =addressParts[0];
//                }

                streetAutoCompleteText.setText(street);
                houseAutoCompleteText.setText(null);
                entranceAutocompleteText.setText(null);
                isObject = streetFull.contains("(") && streetFull.contains(")");
                if (isObject) {
                    GetCoordinates getCoordinates = new GetCoordinates(OrderWithMapActivity.this);
                    getCoordinates.setTransmissionMessage(new CountCoordinatesListener() {
                        @Override
                        public void onCountCoordinatesListener(final Place addresses) {
                            OrderWithMapActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.hideProgressDialog();
                                    if (addresses != null) {
                                        latitude = addresses.getLatitude();
                                        longitude = addresses.getLongitude();
                                        String houseText = null;
                                        String streetText = null;
                                        for (AddressComponent component: addresses.getAddressComponents()){
                                            for(String type : component.getTypes()){
                                                if(type.equals("street_number")){
                                                    houseText = component.getLongName();
                                                    break;
                                                }
                                                if(type.equals("route")){
                                                    streetText = component.getLongName();
                                                    break;
                                                }
                                            }

                                        }

                                        boolean houseCorrect = houseText != null && houseText.length()<5;
                                        if(houseCorrect){
                                            houseAutoCompleteText.setText(houseText);
                                        }else{
                                            houseAutoCompleteText.requestFocus();
                                            houseFilled = true;
                                            enableButtons();
                                            //Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_filds_error));
                                        }
                                        if(isWhereTo){
                                            houseAutoCompleteText.requestFocus();
                                            if(TextUtils.isEmpty(streetText)){
                                                streetAutoCompleteText.setText(addresses.getName() + " ("+street +")");
                                            }else {
                                                streetAutoCompleteText.setText(addresses.getName() + " ("+streetText +")");
                                            }
                                        }else{
                                            if(TextUtils.isEmpty(streetText)){
                                                streetAutoCompleteText.setText(street);
                                            }else {
                                                streetAutoCompleteText.setText(streetText);
                                            }
                                            entranceAutocompleteText.setText(addresses.getName());
                                            if(houseCorrect){
                                                entranceAutocompleteText.requestFocus();
                                            }
                                        }
                                    } else {
                                        houseAutoCompleteText.requestFocus();
                                        Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_filds_error));
                                    }
                                }
                            });

                        }
                    });
                    String halfAddress = getHalfAddress();
                    String fullAddress= Constants.PROFILE_COUNTRY + sharedPreferences.getString(Constants.AREA_AND_CITY, "") + ", " + halfAddress;
                    getCoordinates.execute(fullAddress, placeId);
                    Utils.showProgressDialog(OrderWithMapActivity.this, getString(R.string.loading));
                } else {
                    houseAutoCompleteText.requestFocus();
                }
            }
        });

        streetAutoCompleteText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    streetAutoCompleteText.clearFocus();
                    Utils.hideKeyboard(OrderWithMapActivity.this);
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyboard(OrderWithMapActivity.this);
                    streetAutoCompleteText.clearFocus();
                    return true;
                }

                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                return false;
            }
        });

        streetAutoCompleteText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() != 0) {
                    streetFilled = true;
                    enableButtons();
                } else if (s.toString().length() == 0) {
                    streetFilled = false;
                    disableButtons();
                }
            }
        });

        houseTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                houseAutoCompleteText.requestFocus();
                houseAutoCompleteText.setSelection(houseAutoCompleteText.getText().length());
                Utils.showKeyboard(OrderWithMapActivity.this, houseAutoCompleteText);
            }
        });

        houseAutoCompleteText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {

                    if (!isWhereTo) {
                        entranceAutocompleteText.requestFocus();

                    } else {
                        houseAutoCompleteText.clearFocus();
                        Utils.hideKeyboard(OrderWithMapActivity.this);
                    }

                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyboard(OrderWithMapActivity.this);
                    houseAutoCompleteText.clearFocus();
                    return true;
                }
                return false;
            }
        });

        houseAutoCompleteText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() != 0 || isObject) {
                    houseFilled = true;
                    enableButtons();
                } else if (s.toString().length() == 0) {
                    houseFilled = false;
                    disableButtons();
                }
            }
        });

        entranceTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                entranceAutocompleteText.requestFocus();
                entranceAutocompleteText.setSelection(entranceAutocompleteText.getText().length());
                Utils.showKeyboard(OrderWithMapActivity.this, entranceAutocompleteText);
            }
        });

        entranceAutocompleteText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    Utils.hideKeyboard(OrderWithMapActivity.this);
                    entranceAutocompleteText.clearFocus();
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyboard(OrderWithMapActivity.this);
                    entranceAutocompleteText.clearFocus();
                    return true;
                }

                return false;
            }
        });

        getMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.checkGPSConnection(OrderWithMapActivity.this)) {
                    Utils.gpsDialog(OrderWithMapActivity.this);

                } else {
                    gps = new GPSTracker(OrderWithMapActivity.this);
                    LatLng location = new LatLng(gps.getLatitude(), gps.getLongitude());

                    FragmentManager fragMan = getFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();
                    OwnMapFragment ownMapFragment = new OwnMapFragment();
                    ownMapFragment.setLocation(location);

                    if (fragMan.findFragmentById(R.id.linerMap) == null )
                        fragTransaction.add(R.id.linerMap, ownMapFragment, Constants.OWN_MAP_FRAGMENT).commit();

                    GeoAsyncTask geoAsyncTask = new GeoAsyncTask(OrderWithMapActivity.this, String.valueOf(location.latitude),
                            String.valueOf(location.longitude));
                    geoAsyncTask.execute();
                }
            }
        });

        orderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!streetAutoCompleteText.getText().toString().equals("")) {
                    String halfAddress = getHalfAddress();
                    address.setHalfAddress(halfAddress);
                    address.setFullAddress(Constants.PROFILE_COUNTRY + sharedPreferences.getString(Constants.AREA_AND_CITY, "")
                            + ", " + halfAddress);
                    address.setStreet(streetAutoCompleteText.getText().toString().replaceAll(",", ""));
                    address.setHouse(houseAutoCompleteText.getText().toString().replaceAll(",", ""));
                    address.setDoor(entranceAutocompleteText.getText().toString().replaceAll(",", ""));
                    address.setIsObject(isObject ? 1:0);

                    if (isFavorite || isObject) {
                        address.setFavorite(isFavorite ? 1 : 0);
                        address.setLatitude(latitude);
                        address.setLongitude(longitude);
                        address.setFavorite(1);
                        Log.d("TAG", "  :" + address.getLatitude() + ", " + address.getLongitude());
                        setIntentAndFinish(address);

                    } else {
                        GetCoordinates getCoordinates = new GetCoordinates(OrderWithMapActivity.this);
                        getCoordinates.setTransmissionMessage(new CountCoordinatesListener() {
                            @Override
                            public void onCountCoordinatesListener(final Place addresses) {
                                OrderWithMapActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Utils.hideProgressDialog();
                                        if (addresses != null) {
                                            addressListener = addresses;
                                            address.setLatitude(addresses.getLatitude());
                                            address.setLongitude(addresses.getLongitude());
                                            address.setFavorite(0);
                                            setIntentAndFinish(address);
                                            Log.d("TAG", "  :" + addresses.getLatitude() + ", " + addresses.getLongitude());
                                        } else {
                                            Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_find_error));
                                        }
                                    }
                                });

                            }
                        });
                        getCoordinates.execute(address.getFullAddress());
                        Log.d("TAG", "  :" + address.getFullAddress());
                        Utils.showProgressDialog(OrderWithMapActivity.this, getString(R.string.loading));
                    }
                } else {
                    Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.not_all_fields));
                }
            }
        });
    }

    private void enableButtons() {
        if (streetFilled && houseFilled) {
            addFavoriteAddrImageView.setBackgroundResource(R.drawable.button_add_favaddress);
            orderBtn.setBackgroundResource(R.drawable.button_ready);
            addFavoriteAddrImageView.setEnabled(true);
            orderBtn.setEnabled(true);
        }
    }

    private void disableButtons() {
        addFavoriteAddrImageView.setBackgroundResource(R.drawable.add_favaddress_disable);
        orderBtn.setBackgroundResource(R.drawable.ready_disable);
        addFavoriteAddrImageView.setEnabled(false);
        orderBtn.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        FavoriteAddress favoriteAddress = favoriteAddressArray.get(Integer.parseInt(v.getTag().toString()));
        streetAutoCompleteText.setText(favoriteAddress.getStreet());
        houseAutoCompleteText.setText(favoriteAddress.getHouse_num());
        entranceAutocompleteText.setText(favoriteAddress.getDoor());
        longitude = Double.parseDouble(favoriteAddress.getLongitude());
        latitude = Double.parseDouble(favoriteAddress.getLatitude());
        isFavorite = true;
        isObject = false;
        linearFavoriteAddress.setVisibility(View.GONE);
    }

    private void setIntentAndFinish(Address address) {
        Intent intent = new Intent();
        intent.putExtra(Constants.ADDRESS, address);
        intent.putExtra(Constants.COMMENT, commentButton.getText().toString());
        isWhereTo = false;
        setResult(RESULT_OK, intent);
        finish();
    }

    private String getHalfAddress() {
        String halfAddress;
        if (houseAutoCompleteText.getText().toString().equals("")) {
            halfAddress = streetAutoCompleteText.getText().toString().replaceAll(",", "");

        } else if (!entranceAutocompleteText.getText().toString().equals("")) {
            halfAddress = streetAutoCompleteText.getText().toString().replaceAll(",", "")
                    + ", " + houseAutoCompleteText.getText().toString() + ", " + entranceAutocompleteText.getText().toString();

        } else {
            halfAddress = streetAutoCompleteText.getText().toString().replaceAll(",", "")
                    + ", " + houseAutoCompleteText.getText().toString();
        }
        return halfAddress;
    }

    private void getTextFromField() {
        street = streetAutoCompleteText.getText().toString().replaceAll(",", "");
        house_num = houseAutoCompleteText.getText().toString().replaceAll(",", "");
        entrance = entranceAutocompleteText.getText().toString().replaceAll(",", "");
    }

    public void onAddAddressClick(View view) {
        showAddAddressDialog(view);
    }

    public void onFavoriteAddressClick(View view) {
        if (linearFavoriteAddress.getVisibility() == View.GONE) {
            linearFavoriteAddress.setVisibility(View.VISIBLE);
            ((TextView)view).setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.arrow_orange_down, 0);
        } else {
            linearFavoriteAddress.setVisibility(View.GONE);
            ((TextView)view).setCompoundDrawablesWithIntrinsicBounds(
                    0, 0, R.drawable.arrow_orange, 0);
        }
    }

    public void onAddCommentClick(final View view) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialog_layout = inflater.inflate(R.layout.dialog_write_letter,(ViewGroup) view.findViewById(R.id.root));
        final EditText editTextLetter = (EditText)dialog_layout.findViewById(R.id.editTextLetter);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.write_letter);
        builder.setView(dialog_layout)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.hideKeyboard(OrderWithMapActivity.this);
                            }
                        }, 300);

                        String currentText = editTextLetter.getText().toString();
                        ((TextView)view).setText(currentText.length() != 0 ? editTextLetter.getText().toString()
                                : getString(R.string.comment_for_driver));

                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.hideKeyboard(OrderWithMapActivity.this);
                            }
                        }, 300);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showAddAddressDialog(final View view) {
        getTextFromField();
        if (street.equals("")) {
            Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.not_all_fields));

        } else {
            LayoutInflater inflater = LayoutInflater.from(this);
            View dialog_layout = inflater.inflate(R.layout.addfavorite_dialog, (ViewGroup) view.findViewById(R.id.root));
            final EditText editTextNameAddress = (EditText) dialog_layout.findViewById(R.id.editTextNameAddress);
            final Button addButton = (Button) dialog_layout.findViewById(R.id.buttonAdd);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(dialog_layout);
            final AlertDialog alert = builder.create();
            alert.show();
            editTextNameAddress.requestFocus();

            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editTextNameAddress.getText().toString().equals("")) {
                        Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.name_for_address_null));

                    } else {
                        GetCoordinates getCoordinates = new GetCoordinates(OrderWithMapActivity.this);
                        getCoordinates.setTransmissionMessage(new CountCoordinatesListener() {
                            @Override
                            public void onCountCoordinatesListener(final Place addresses) {
                                OrderWithMapActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (addresses != null) {
                                            addressListener = addresses;
                                            longitude = addresses.getLongitude();
                                            latitude = addresses.getLatitude();
                                            isFavorite = true;

                                            PassengerRequester passengerRequester = new PassengerRequester(OrderWithMapActivity.this);
                                            passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                                                @Override
                                                public void onMessageSuccess(String str) {
                                                    Utils.hideProgressDialog();
                                                    alert.cancel();
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Utils.hideKeyboard(OrderWithMapActivity.this);
                                                        }
                                                    }, 500);
                                                }

                                                @Override
                                                public void onMessageError(int errorCode) {
                                                    Utils.hideProgressDialog();
                                                    alert.cancel();
                                                    new Handler().postDelayed(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Utils.hideKeyboard(OrderWithMapActivity.this);
                                                        }
                                                    }, 500);
                                                    Log.d("ERROR", ": " + errorCode);
                                                }
                                            });
                                            passengerRequester.setFavoriteAddress(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                                                    "0", editTextNameAddress.getText().toString(), street, house_num, entrance,
                                                    String.valueOf(longitude), String.valueOf(latitude), "");
                                        } else {
                                            Utils.hideProgressDialog();
                                            Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_find_error));
                                            alert.cancel();
                                        }
                                    }
                                });
                            }
                        });

                        getCoordinates.execute(Constants.PROFILE_COUNTRY
                                + sharedPreferences.getString(Constants.AREA_AND_CITY, "")
                                + ", " + getHalfAddress());
                        Utils.showProgressDialog(OrderWithMapActivity.this, getString(R.string.saving));
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fr = fragmentManager.findFragmentById(R.id.menu_container);
        orderBtn.requestFocus();

        if (fr instanceof PassengerMenuFragment) {
            isMenuVisible = !isMenuVisible;
            fragmentManager.popBackStack();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setActionBarYellow();
                }
            }, 600);

        } else if (fr instanceof PassengerProfileFragment || fr instanceof PassengerSettingFragment
                || fr instanceof FavoriteAddressFragment) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else {
            super.onBackPressed();
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            finish();
        }
    }

    private void openMenuFragment() {
        PassengerMenuFragment passengerMenuFragment = new PassengerMenuFragment();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close,
                        R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close)
                .add(R.id.menu_container, passengerMenuFragment, Constants.DRIVER_MENU_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    private void initUI() {
        addFavoriteAddrImageView = (Button)findViewById(R.id.imageViewAddAddress);
        linearFavoriteAddress = (LinearLayout)findViewById(R.id.linearFavoriteAddress);
        commentButton = (Button)findViewById(R.id.commentButton);
        getMyLocation = (TextView)findViewById(R.id.getMyLocationTextView);
        streetAutoCompleteText = (AutoCompleteTextView)findViewById(R.id.streetAutoCompleteText);
        entranceAutocompleteText = (AutoCompleteTextView)findViewById(R.id.entranceEditText);
        houseAutoCompleteText = (AutoCompleteTextView)findViewById(R.id.housEditText);
        orderBtn = (Button)findViewById(R.id.orderBtn);
        streetTextView = (TextView)findViewById(R.id.textViewStreet);
        houseTextView = (TextView)findViewById(R.id.editTextHouse);
        entranceTextView = (TextView)findViewById(R.id.editTextEntrance);
        entranceRelativeLayout = (RelativeLayout)findViewById(R.id.entranceRelativeLayout);
        fromWhereTextView =(TextView)findViewById(R.id. textViewOrderTitleFromWhere);
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(OrderWithMapActivity.this);
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }

    public class GeoAsyncTask extends AsyncTask<String, String, Integer> {
        private Context context;
        private String latitude;
        private String longitude;
        boolean isConnectionAvailable;
        String responseText = null;

        public GeoAsyncTask(Context context, String latitude, String longitude) {
            super();
            this.context = context;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public GeoAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isConnectionAvailable = Utils.isConnectionAvailable(context);
        }

        @Override
        protected Integer doInBackground(String... urls) {
            if (isConnectionAvailable) {
                StringBuilder sb = new StringBuilder(Constants.MAPS_GEOCODE);

                sb.append("sensor=false");
                sb.append("&types=street_address");
                sb.append("&latlng=").append(latitude).append(",").append(longitude);
                sb.append("&language=ru");

                try {
                    DefaultHttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(sb.toString());
                    HttpResponse httpresponse = httpclient.execute(httppost);
                    HttpEntity httpentity = httpresponse.getEntity();
                    InputStream is = httpentity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sba = new StringBuilder();
                    String line = null;

                    while((line = reader.readLine()) != null) {
                        sba.append(line + "\n");
                    }
                    is.close();
                    responseText = sba.toString();
                }
                catch(ClientProtocolException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer res) {
            super.onPostExecute(res);
            try {
                if (responseText != null) {
                    JSONObject object = new JSONObject(responseText);

                    if (object.has("results")) {
                        JSONArray jsonArrayResults = object.getJSONArray("results");
                        if (jsonArrayResults.length() != 0) {
                            JSONObject objectResult = jsonArrayResults.getJSONObject(0);

                            if (objectResult.has("address_components")) {
                                JSONArray jsonArrayAddrComponents = objectResult.getJSONArray("address_components");
                                JSONObject objectHouse = jsonArrayAddrComponents.getJSONObject(0);
                                String house = objectHouse.getString("long_name");

                                JSONObject objectStreet = jsonArrayAddrComponents.getJSONObject(1);
                                String street = objectStreet.getString("long_name");

                                if (street.length() == 0 && house.length() == 0) {
                                    Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_filling_error));
                                } else {
                                    streetAutoCompleteText.setText(street);
                                    houseAutoCompleteText.setText(house);
                                }
                            }
                        } else {
                            Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_filling_error));
                        }
                    }
                } else {
                    Utils.showAlertDialog(OrderWithMapActivity.this, getString(R.string.address_filling_error));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void setMenuVisible(boolean state) {
        isMenuVisible = state;
    }

    protected void setActionBarDark() {
        menuButton.setBackgroundResource(R.drawable.ic_action_menu_yellow);
        mCustomView.setBackgroundResource(R.drawable.action_bar_dark);
    }

    protected void setActionBarYellow() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                menuButton.setBackgroundResource(R.drawable.ic_action_menu);
                mCustomView.setBackgroundResource(R.drawable.action_bar);
            }
        }, 600);
    }
}