package com.mobapply.taxitter.ui.driver;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ListOrdersArrayAdapter;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.DriverRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Fragment contain list of orders with status_offer 2 or 3.
 */
public class ListOrdersOnTabTwoFragment extends Fragment {
    private static boolean isNeedRefreshListOffers = false;
    public static final String ORDERS_CONFIRM_AND_ACTIVELY = "4";
    private final String ORDERS_IS_ALREADY_SUBMITTED_A_PROPOSAL = "3";

    private SharedPreferences sharedPreferences;
    private FragmentManager fragmentManager;
    private ListView ordersListView;
    private ArrayList<Order> arrayOrdersList;
    private ArrayList<Order> arrayConfirmOrdersList;
    private ArrayList<Order> arrayActivelyOrdersList;
    private ListOrdersArrayAdapter listOrdersArrayAdapter;
    private SwipeRefreshLayout swipeContainer;
    private boolean isCreated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_order_with_refresh, container, false);
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        ordersListView = (ListView)view.findViewById(R.id.lvItems);

        arrayOrdersList = new ArrayList<Order>();
        arrayConfirmOrdersList = new ArrayList<Order>();
        arrayActivelyOrdersList = new ArrayList<Order>();

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListOrders();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        ordersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemNumber, long l) {
                if (!swipeContainer.isRefreshing()) {
                    Order item = arrayOrdersList.get(itemNumber);

                    if (sharedPreferences.getBoolean(Constants.CURRENT_ORDER_ACCEPT, false) &&
                            sharedPreferences.getString(Constants.CURRENT_ORDER_UID, "").equals(item.getOrder_uid())) {

                        CurrentDriverOrder currentDriverOrder = new CurrentDriverOrder();
                        currentDriverOrder.setDetailItem(item);
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragments_container, currentDriverOrder, Constants.CURRENT_ORDER)
                                .addToBackStack(Constants.CURRENT_ORDER)
                                .commit();

                    } else {
                        DetailsOffersItemFragment detailsOffersItemFragment = new DetailsOffersItemFragment();
                        detailsOffersItemFragment.setDetailItem(item);
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragments_container, detailsOffersItemFragment, Constants.DRIVER_DETAILE_OF_OFFER_TAG)
                                .addToBackStack(Constants.DRIVER_DETAILE_OF_OFFER_TAG)
                                .commit();
                    }
                }
            }
        });

        getFragmentManager().
        addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (isNeedRefreshListOffers) {
                    getListOrders();
                    isNeedRefreshListOffers = false;
                }
            }
        });

        return view;
    }

    public static void setIsNeedRefresh(boolean b) {
        isNeedRefreshListOffers = b;
    }

    private void getListOrders() {
        if (!isVisible()) return;

        final DriverRequester driverRequester = new DriverRequester(getActivity().getApplicationContext());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                swipeContainer.setRefreshing(false);

                if (!str.equals(Constants.OK)) {
                    if (isVisible()) {
                        try {
                            JSONArray jsonArray = new JSONArray(str);

                            if (jsonArray.length() != 0) {
                                arrayOrdersList.clear();
                                arrayConfirmOrdersList.clear();
                                arrayActivelyOrdersList.clear();

                                for (int n = 0; n < jsonArray.length(); n++) {
                                    JSONObject object = jsonArray.getJSONObject(n);
                                    Order order = new Order(object);

                                    if (order.getOrder_uid().equals(sharedPreferences.getString(Constants.ORDER_ID, ""))) {
                                        DetailsOffersItemFragment detailsOffersItemFragment = new DetailsOffersItemFragment();
                                        detailsOffersItemFragment.setDetailItem(order);
                                        fragmentManager.beginTransaction()
                                                .replace(R.id.fragments_container, detailsOffersItemFragment, Constants.DRIVER_DETAILE_OF_OFFER_TAG)
                                                .addToBackStack(Constants.DRIVER_DETAILE_OF_OFFER_TAG)
                                                .commit();

                                        sharedPreferences.edit().remove(Constants.ORDER_ID).apply();
                                        break;
                                    }

                                    if (sharedPreferences.getBoolean(Constants.CURRENT_ORDER_ACCEPT, false) &&
                                            sharedPreferences.getString(Constants.CURRENT_ORDER_UID, "").equals(order.getOrder_uid())) {
                                        arrayOrdersList.add(order);

                                    } else {
                                        if (order.getStatus_offer().equals(Constants.DRIVER_ORDERS_ACCEPTED_BY_THE_PASSENGER))
                                            arrayConfirmOrdersList.add(order);
                                        else if (order.getStatus_offer().equals(ORDERS_IS_ALREADY_SUBMITTED_A_PROPOSAL))
                                            arrayActivelyOrdersList.add(order);
                                    }
                                }
                            }
                            for (int i = 0; i < arrayConfirmOrdersList.size(); i++) {
                                arrayOrdersList.add(arrayConfirmOrdersList.get(i));
                            }
                            for (int a = 0; a < arrayActivelyOrdersList.size(); a++) {
                                arrayOrdersList.add(arrayActivelyOrdersList.get(a));
                            }

                            if (isVisible()) {
                                listOrdersArrayAdapter = new ListOrdersArrayAdapter(getActivity(), arrayOrdersList);
                                ordersListView.setAdapter(listOrdersArrayAdapter);
                                ordersListView.invalidate();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                if (isVisible() && (((DriverFragmentActivity) getActivity()).getTabPosition() == 1)) {
                    swipeContainer.setRefreshing(false);

                    if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
                        Utils.showToast(getActivity(), getString(R.string.not_find_user_with_access_token));
                        MainFragmentActivity.isAccessTokenGone(true);
                        getActivity().finish();

                    } else if (errorCode == Constants.NO_NEW_ORDERS) {
                        arrayOrdersList.clear();
                        arrayConfirmOrdersList.clear();
                        arrayActivelyOrdersList.clear();

                        if (listOrdersArrayAdapter != null)
                            listOrdersArrayAdapter.notifyDataSetChanged();

                    } else {
                        if (DriverRequester.checkGetListStatementsErrorCode(getActivity(), errorCode).length() != 0)
                            Utils.showToast(getActivity(), DriverRequester.checkGetListStatementsErrorCode(getActivity(), errorCode));
                    }

                    ordersListView.invalidate();
                }

            }
        });

        String cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
        String city_d = sharedPreferences.getString(Constants.CITY_D, "");
        driverRequester.getListOrders(cookie, ORDERS_CONFIRM_AND_ACTIVELY, city_d);
        swipeContainer.setRefreshing(true);
    }

    public void reloadList() {
        getListOrders();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sharedPreferences.getBoolean(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL, false)) {
            getListOrders();
            sharedPreferences.edit().remove(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL).apply();
        }

        if (isCreated)
            getListOrders();

        isCreated = true;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if (isCreated)
                getListOrders();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isCreated = false;
    }
}