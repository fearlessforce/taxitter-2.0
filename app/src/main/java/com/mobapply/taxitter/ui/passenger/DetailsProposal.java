package com.mobapply.taxitter.ui.passenger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.FeedBackArrayAdapter;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.SwipeButtonClickListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Driver;
import com.mobapply.taxitter.model.FeedBackItem;
import com.mobapply.taxitter.model.Proposal;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailsProposal extends Fragment {
    private ArrayList<AdditionalRoute> additionalRoutes;
    private SharedPreferences sharedPreferences;
    private Driver driver;
    private Proposal itemProposal;
    private ImageLoader imageLoader;
    private Dialog dialogRating;
    private Dialog dialogPhoto;

    private TextView fromWhereTopTextView;
    private TextView whereToTopTextView;
    private TextView carTextView;
    private TextView ratingTextView;
    private TextView countRouteTextView;
    private TextView lengthTextView;
    private TextView countSeetTextView;
    private TextView carColorAndYearTextView;
    private TextView carNumberTextView;
    private TextView nickNameTextView;
    private TextView commentTextView;
    private TextView distanceTextView;
    private TextView titleRateDialog;
    private LinearLayout linearLoad;
    private EditText cashEditText;
    private Button commentButton;
    private ImageButton acceptButton, rejectButton;
    private ImageView babySeetImageView, noSmokingImageView, airconditionImageView;
    private ImageView driverPhotoImageView;
    private RelativeLayout relativeRating;
    private FragmentManager fragmentManager;
    private SwipeButtonClickListener swipeButtonClickListener;
    private ArrayList<FeedBackItem> feedBackArrayList = new ArrayList<FeedBackItem>();
    private ListView listViewFeedBack;
    private Button closeButton;
    private ImageView imageViewLargePhoto;
    private String cookie;
    private boolean isNeedDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_proposal, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
        imageLoader = ImageLoader.getInstance();
        fragmentManager = getFragmentManager();

        initializationUI(view);
        createDialogPhoto();
        createRateDialog();
        fillingUIData();
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);
        getFeadBackAboutDriver();

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectProposal();
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptProposal();
            }
        });

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createComment(view);
            }
        });
        commentButton.setVisibility(View.GONE);

        driverPhotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLargePhoto();
            }
        });

        ((Button) dialogRating.findViewById(R.id.buttonClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogRating.cancel();
            }
        });

        relativeRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogRating.show();
            }
        });

        return view;
    }

    private void createRateDialog() {
        dialogRating = new Dialog(getActivity());
        dialogRating.setCanceledOnTouchOutside(true);
        dialogRating.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRating.setContentView(R.layout.rating_reviews_dialog);
        dialogRating.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        listViewFeedBack = (ListView) dialogRating.findViewById(R.id.listViewRatingAndReviews);
        titleRateDialog = (TextView) dialogRating.findViewById(R.id.textViewTitle);
        linearLoad = (LinearLayout) dialogRating.findViewById(R.id.linearLoad);
    }

    private void initializationUI(View view) {
        relativeRating = (RelativeLayout) view.findViewById(R.id.relativeRating);
        distanceTextView = (TextView) view.findViewById(R.id.textViewDistanceTop);
        fromWhereTopTextView = (TextView) view.findViewById(R.id.textViewFromWhereTopOffer);
        whereToTopTextView = (TextView) view.findViewById(R.id.textViewWhereToTopOffer);
        carTextView = (TextView) view.findViewById(R.id.textViewDriverCar);
        ratingTextView = (TextView) view.findViewById(R.id.textViewRating);
        countRouteTextView = (TextView) view.findViewById(R.id.textViewCountComment);
        lengthTextView = (TextView) view.findViewById(R.id.textViewLength);
        countSeetTextView = (TextView) view.findViewById(R.id.textViewCountSeet);
        carColorAndYearTextView = (TextView) view.findViewById(R.id.textViewColorYearCar);
        carNumberTextView = (TextView) view.findViewById(R.id.textViewNumberCar);
        nickNameTextView = (TextView) view.findViewById(R.id.textViewNick);
        commentTextView = (TextView) view.findViewById(R.id.textViewCommentOrder);
        cashEditText = (EditText) view.findViewById(R.id.editTextCashEditField);
        commentButton = (Button) view.findViewById(R.id.commentButton);
        babySeetImageView = (ImageView) view.findViewById(R.id.babySeetImageView);
        noSmokingImageView = (ImageView) view.findViewById(R.id.noSmokingImageView);
        airconditionImageView = (ImageView) view.findViewById(R.id.airconditionImageView);
        acceptButton = (ImageButton) view.findViewById(R.id.orderButton);
        rejectButton = (ImageButton) view.findViewById(R.id.backBtn);
        driverPhotoImageView = (ImageView)view.findViewById(R.id.imageViewDriverFoto);
    }

    private void fillingUIData() {
        fromWhereTopTextView.setText(itemProposal.getDeparture());
        additionalRoutes = itemProposal.getAdditionalRoutes();
        whereToTopTextView.setText(additionalRoutes.get(additionalRoutes.size() - 1).getWayaddress());
        if (!driver.getDistance_d().equals("0")) {
            distanceTextView.setText(driver.getDistance_d() + " " + getString(R.string.km));
        }

        countRouteTextView.setText(driver.getTrips_d());
        nickNameTextView.setText(driver.getNickname_d());
        carTextView.setText(driver.getCar_d());
        ratingTextView.setText(driver.getRating_d().equals("") ? "0" : driver.getRating_d());
        lengthTextView.setText(itemProposal.getLength().equals("") ? "" : itemProposal.getLength() + " " + getString(R.string.km));
        countSeetTextView.setText(itemProposal.getQuantity() + " " + getString(R.string.seets));

        if (driver.getCarcolor_d().equals("")) {

            if (!driver.getCaryear_d().equals("")) {
                carColorAndYearTextView.setText(driver.getCaryear_d() + " " + getString(R.string.year_short));
            } else {
                carColorAndYearTextView.setText("");
            }

        } else {

            if (!driver.getCaryear_d().equals("")) {
                carColorAndYearTextView.setText(driver.getCarcolor_d() + ", " + driver.getCaryear_d()
                        + " " + getString(R.string.year_short));

            } else {
                carColorAndYearTextView.setText(driver.getCarcolor_d());
            }

            carColorAndYearTextView.setText(driver.getCarcolor_d());
        }
        if (!TextUtils.isEmpty(driver.getCarnumber_d())){
            carNumberTextView.setText(driver.getCarnumber_d());
        } else{
            carNumberTextView.setText("");
        }

        if (!driver.getComment_d().equals(""))
            commentTextView.setVisibility(View.VISIBLE);

        commentTextView.setText(driver.getComment_d());
        cashEditText.setText(driver.getCash_d());

        if (driver.getBabyseat_d() == 1)
            babySeetImageView.setBackgroundResource(R.drawable.conditions_babyseat_pressed_details_order);

        if (driver.getAc_d() == 1)
            airconditionImageView.setBackgroundResource(R.drawable.conditions_conditioner_pressed_details_order);

        if (driver.getNonsmoking_d() == 1)
            noSmokingImageView.setBackgroundResource(R.drawable.conditions_smoker_pressed_details_order);

        if (driver.getStatus_d().equals(Constants.PASSENGER_ACCEPT_ORDER)) {
            acceptButton.setVisibility(View.GONE);
        }
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, driverPhotoImageView, size);
                                        }
                                    } else {
                                        imageLoader.DisplayImage(urlImage, imageViewLargePhoto, size);
                                        if (isNeedDialog && isVisible()) {
                                            Utils.hideProgressDialog();
                                            dialogPhoto.show();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.showToast(getActivity(), "" + errorCode);
            }
        });
        commonRequest.getOtherAvatar(cookie, size, driver.getUid_d());
    }

    private void getFeadBackAboutDriver() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                linearLoad.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                retrieveDataFromStr(str);
            }

            @Override
            public void onMessageError(int errorCode) {
                linearLoad.setVisibility(View.GONE);
                Utils.hideProgressDialog();
            }
        });
        passengerRequester.getFeedbacks(cookie, driver.getUid_d());
    }

    private void retrieveDataFromStr(String str) {
        if (!str.equals(Constants.OK)) {
            try {
                JSONObject jsObject = new JSONObject(str);
                JSONArray jsArray = jsObject.getJSONArray("feedbacks");

                for (int n = 0; n < jsArray.length(); n++) {
                    JSONObject object = jsArray.getJSONObject(n);
                    FeedBackItem feedBackItem = new FeedBackItem(object);
                    feedBackArrayList.add(feedBackItem);
                }

                if (isVisible()) {
                    FeedBackArrayAdapter feedBackArrayAdapter = new FeedBackArrayAdapter(getActivity(),
                            feedBackArrayList);

                    titleRateDialog.setText(titleRateDialog.getText().toString() + " (" + feedBackArrayList.size()
                            + ") ");
                    listViewFeedBack.setClickable(false);
                    listViewFeedBack.setAdapter(feedBackArrayAdapter);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void rejectProposal() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                Utils.hideProgressDialog();
                fragmentManager.popBackStack();
                swipeButtonClickListener.onRejectClickButton(0);
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });
        passengerRequester.changeOffer(cookie, itemProposal.getOrder_uid(), driver.getUid_d(),
                cashEditText.getText().toString(), Constants.PASSENGER_REJECT_ORDER);
        Utils.showProgressDialog(getActivity(), getString(R.string.loading));
    }

    private void expensiveProposal() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                Utils.hideProgressDialog();
                fragmentManager.popBackStack();
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });
        passengerRequester.changeOffer(cookie, itemProposal.getOrder_uid(), driver.getUid_d(),
                cashEditText.getText().toString(), Constants.PASSENGER_EXPENSIVE_DRIVER_PROPOSAL);
        Utils.showProgressDialog(getActivity(), getString(R.string.loading));
    }

    private void acceptProposal() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                Utils.hideProgressDialog();
                fragmentManager.popBackStack();
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });
        passengerRequester.changeOffer(cookie, itemProposal.getOrder_uid(), driver.getUid_d(),
                cashEditText.getText().toString(), Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL);
        Utils.showProgressDialog(getActivity(), getString(R.string.loading));
    }

    private void createComment(View view) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View dialog_layout = inflater.inflate(R.layout.dialog_write_letter, (ViewGroup) view.findViewById(R.id.root));
        final EditText editTextLetter = (EditText) dialog_layout.findViewById(R.id.editTextLetter);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.write_letter);
        builder.setView(dialog_layout)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.hideKeyboard(getActivity());
                            }
                        }, 300);

                        String currentText = editTextLetter.getText().toString();
                        commentButton.setText(currentText.length() != 0 ? editTextLetter.getText().toString()
                                : getString(R.string.write_letter));

                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.hideKeyboard(getActivity());
                            }
                        }, 300);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void openLargePhoto() {
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPhoto.cancel();
            }
        });

        if ((Integer)imageViewLargePhoto.getTag() != R.drawable.round67) {
            dialogPhoto.show();

        } else {
            getAvatarFromServer(Constants.IMG_LARGE_SIZE);
            isNeedDialog = true;
            Utils.showProgressDialog(getActivity(), getString(R.string.loading));
        }
    }

    public void createDialogPhoto() {
        dialogPhoto = new Dialog(getActivity());
        dialogPhoto.setCanceledOnTouchOutside(true);
        dialogPhoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPhoto.setContentView(R.layout.large_foto_dialog);
        dialogPhoto.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialogPhoto.findViewById(R.id.button_close);
        imageViewLargePhoto = (ImageView) dialogPhoto.findViewById(R.id.imageViewLargePhoto);
        imageViewLargePhoto.setTag(R.drawable.round67);
    }

    public void checkSetOfferErrorCode(int errorCode) {
        if (errorCode == Constants.FAILED_TO_WRITE_IN_DB) {
            Utils.showToast(getActivity(), getResources().getString(R.string.failed_to_write_in_db));

        } else if (errorCode == Constants.NO_REQUEST_IN_DB) {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_request_in_db));

        } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getResources().getString(R.string.not_all_fields));

        } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
            Utils.showToast(getActivity(), getResources().getString(R.string.json_error));

        } else if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            Utils.showToast(getActivity(), getResources().getString(R.string.not_find_user_with_access_token));
            MainFragmentActivity.isAccessTokenGone(true);
            getActivity().finish();

        } else if (errorCode == Constants.CASH_EQUALS_ZERO) {
            Utils.showToast(getActivity(), getResources().getString(R.string.cash_equals_zero));

        }  else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_network));

        } else if (errorCode == Constants.FAILED_INCORRECT_VALUE_STATUS_P) {
            Utils.showToast(getActivity(), getResources().getString(R.string.incorrect_value_status_d));

        } else {
            Utils.showToast(getActivity(), "Code: " + errorCode);
        }
    }

    public void setDetailsDriver(Driver item) {
        driver = item;
    }

    public void setDetailsOffer(Proposal itemProposal) {
        this.itemProposal = itemProposal;
    }

    public void setButtonsListener(SwipeButtonClickListener swipeButtonClickListener) {
        this.swipeButtonClickListener = swipeButtonClickListener;
    }
}