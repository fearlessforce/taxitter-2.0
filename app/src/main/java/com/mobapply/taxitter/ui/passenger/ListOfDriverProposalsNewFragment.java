package com.mobapply.taxitter.ui.passenger;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ListViewSwipeAdapter;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.SwipeButtonClickListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.interfaces.TransmissionText;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Driver;
import com.mobapply.taxitter.model.Proposal;
import com.mobapply.taxitter.service.GPSTracker;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.DistanceCalculation;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListOfDriverProposalsNewFragment extends Fragment implements SwipeButtonClickListener, OnMapReadyCallback {
    private static boolean isNeedRefreshListOffers = false;
    private ImageLoader imageLoader;
    private ArrayList<AdditionalRoute> additionalRoutesArrayList;
    private ArrayList<Driver> driversArrayList;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private ListViewSwipeAdapter listViewSwipeAdapter;
    private ProgressDialog progressDialog;
    private Proposal proposal;
    private Driver driver;

    private TextView carTextView;
    private TextView ratingTextView;
    private TextView countRouteTextView;
    private TextView lengthTextView;
    private TextView countSeetTextView;
    private TextView carColorAndYearTextView;
    private TextView nickNameTextView;
    private TextView commentTextView;
    private EditText cashEditText;
    private ImageView driverPhotoImageView;
    private RelativeLayout relativeRating;
    private ImageView imageViewLargePhoto;
    private FrameLayout mapContainer;
    private TextView distanceTextView;
    private TextView carNumberTextView;

    private Handler handler;
    private Runnable runnable;
    private LinearLayout driverContainer;
    private SwipeRefreshLayout swipeContainer;
    private TextView fromWhereTopTextView;
    private TextView whereToTopTextView;
    private ListView listViewPassProposals;
    private Button refuseButton, callButton;
    private String id_d;
    private String cookie;

    private Marker passengerMarker;
    private Marker driverMarker;
    private GoogleMap mMap;

    private boolean showDriver = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.passenger_list_of_proposals_new, container, false);
        imageLoader = ImageLoader.getInstance();
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
        initUI(view);

        driversArrayList = new ArrayList<Driver>();
        driversArrayList = proposal.getItemOfferDrivers();
        fromWhereTopTextView.setText(proposal.getDeparture());
        additionalRoutesArrayList = proposal.getAdditionalRoutes();
        whereToTopTextView.setText(additionalRoutesArrayList.get(additionalRoutesArrayList.size() - 1).getWayaddress());

        showDriver = driversArrayList.size() > 0 && driversArrayList.get(0).getStatus_d().equals(Constants.PASSENGER_DRIVER_ACCEPT_ORDER);

        refresh(showDriver);


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setTimeRefreshForList();
                getListOffers();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        refuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refuseProposal();
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+" + driver.getId_d(), null));
                startActivity(intent);
            }
        });

        listViewPassProposals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int itemNumber, long l) {
                Driver item = driversArrayList.get(itemNumber);

                if (!item.getStatus_d().equals(Constants.PASSENGER_DRIVER_ACCEPT_ORDER)
                        && !swipeContainer.isRefreshing()) {
                    DetailsProposal detailsProposal = new DetailsProposal();
                    detailsProposal.setButtonsListener(ListOfDriverProposalsNewFragment.this);
                    detailsProposal.setDetailsOffer(proposal);
                    detailsProposal.setDetailsDriver(item);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragments_container, detailsProposal, Constants.PASSENGER_DETAILE_OF_PROPOSALS)
                            .addToBackStack(Constants.PASSENGER_DETAILE_OF_PROPOSALS)
                            .commit();
                }
            }
        });

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(showDriver){

                }else {
                    if (isNeedRefreshListOffers) {
                        setTimeRefreshForList();
                        getListOffers();
                        isNeedRefreshListOffers = false;
                    }
                }
            }
        });

        return view;
    }

    public void refresh(boolean show){
       showDriver = show;

        if (showDriver){
            driverContainer.setVisibility(View.VISIBLE);
            swipeContainer.setVisibility(View.GONE);
            setUpDriver();
            setUpMap();
            //    getDriverLocation();
            //   setTimeRefreshForMap();
        } else{
            driverContainer.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.VISIBLE);

            //setUpAdapter();


        }
    }

    private void refuseProposal() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (isVisible()) {
                    hideProgressDialog();
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ListOfOwnOrders.setIsNeedRefreshOrders(true);
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                if (isVisible()) {
                    hideProgressDialog();

                    if (errorCode != Constants.NO_NETWORK_CONNECTION) {
                        Utils.showToast(getActivity(), "" + errorCode);
                        backForAuthorization(errorCode);
                    }
                }
            }
        });
        passengerRequester.cancelOrder_p(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                proposal.getOrder_uid());
        showProgressDialog();
    }

    private void initUI(View view) {
        driverContainer = (LinearLayout)view.findViewById(R.id.driverContainer);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        fromWhereTopTextView = (TextView) view.findViewById(R.id.textViewFromWhereTop);
        whereToTopTextView = (TextView) view.findViewById(R.id.textViewWhereToTop);
        listViewPassProposals = (ListView) view.findViewById(R.id.listViewPassProposals);
        refuseButton = (Button) view.findViewById(R.id.buttonRefuse);
        callButton = (Button) view.findViewById(R.id.buttonCall);

        relativeRating = (RelativeLayout) view.findViewById(R.id.relativeRating);
        carTextView = (TextView) view.findViewById(R.id.textViewDriverCar);
        ratingTextView = (TextView) view.findViewById(R.id.textViewRating);
        countRouteTextView = (TextView) view.findViewById(R.id.textViewCountComment);
        lengthTextView = (TextView) view.findViewById(R.id.textViewLength);
        countSeetTextView = (TextView) view.findViewById(R.id.textViewCountSeet);
        carColorAndYearTextView = (TextView) view.findViewById(R.id.textViewColorYearCar);
        nickNameTextView = (TextView) view.findViewById(R.id.textViewNick);
        commentTextView = (TextView) view.findViewById(R.id.textViewCommentOrder);
        cashEditText = (EditText) view.findViewById(R.id.editTextCashEditField);
        driverPhotoImageView = (ImageView)view.findViewById(R.id.imageViewDriverFoto);
        mapContainer  = (FrameLayout)view.findViewById(R.id.mapContainer);
        distanceTextView = (TextView)view.findViewById(R.id.textViewDistance);
        carNumberTextView = (TextView) view.findViewById(R.id.textViewNumberCar);

    }

    private void setUpMap(){
        GPSTracker gps = new GPSTracker(getActivity());
        LatLng location = new LatLng(gps.getLatitude(), gps.getLongitude());

        FragmentManager fragMan = getFragmentManager();
        FragmentTransaction fragTransaction = fragMan.beginTransaction();
        MapFragment mapFragment = new MapFragment();
        mapFragment.getMapAsync(this);

        fragTransaction.replace(R.id.mapContainer, mapFragment, Constants.OWN_MAP_FRAGMENT).commit();
    }

    private void setUpDriver(){
        driver = driversArrayList.get(0);

        if(driver == null){
            return;
        }

        getAvatarFromServer(Constants.IMG_SMALL_SIZE);


        if (!driver.getDistance_d().equals("0")) {
            distanceTextView.setText(getString(R.string.driver_from_you) + " "+ driver.getDistance_d() + " " + getString(R.string.km));
        }

        countRouteTextView.setText(driver.getTrips_d());
        nickNameTextView.setText(driver.getNickname_d());
        carTextView.setText(driver.getCar_d() + " " +driver.getCarmodel_d());
        ratingTextView.setText(driver.getRating_d().equals("") ? "0" : driver.getRating_d());
        lengthTextView.setText(proposal.getLength().equals("") ? "" : proposal.getLength() + " " + getString(R.string.km));
        countSeetTextView.setText(proposal.getQuantity() + " " + getString(R.string.seets));

        if (driver.getCarcolor_d().equals("")) {

            if (!driver.getCaryear_d().equals("")) {
                carColorAndYearTextView.setText(driver.getCaryear_d() + " " + getString(R.string.year_short));
            } else {
                carColorAndYearTextView.setText("");
            }

        } else {

            if (!driver.getCaryear_d().equals("")) {
                carColorAndYearTextView.setText(driver.getCarcolor_d() + ", " + driver.getCaryear_d()
                        + " " + getString(R.string.year_short));

            } else {
                carColorAndYearTextView.setText(driver.getCarcolor_d());
            }

            carColorAndYearTextView.setText(driver.getCarcolor_d());
        }

        if (!TextUtils.isEmpty(driver.getCarnumber_d())){
            carNumberTextView.setText(driver.getCarnumber_d());
        } else{
            carNumberTextView.setText("");
        }

        if (!driver.getComment_d().equals(""))
            commentTextView.setVisibility(View.VISIBLE);

        commentTextView.setText(driver.getComment_d());
        cashEditText.setText(driver.getCash_d());
        callButton.setVisibility(View.VISIBLE);

    }

    private void setUpAdapter() {
        listViewSwipeAdapter = new ListViewSwipeAdapter(getActivity(), driversArrayList);
        listViewPassProposals.setAdapter(listViewSwipeAdapter);
        listViewSwipeAdapter.setMode(Attributes.Mode.Single);
        listViewSwipeAdapter.setButtonsListener(this);
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, driverPhotoImageView, size);
                                        }
                                    } else {
                                        imageLoader.DisplayImage(urlImage, imageViewLargePhoto, size);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.showToast(getActivity(), "" + errorCode);
            }
        });
        commonRequest.getOtherAvatar(cookie, size, driver.getUid_d());
    }

    public static void setIsNeedRefresh(boolean b) {
        isNeedRefreshListOffers = b;
    }

    private void setTimeRefreshForList() {
        final int delay = 60000;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    getListOffers();
                    handler.postDelayed(this, delay);
                }
            }
        };
        handler.postDelayed(runnable, delay);
    }

    private void setTimeRefreshForMap() {
        final int delay = 30000;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    getDriverLocation();
                    handler.postDelayed(this, delay);
                }
            }
        };
        handler.postDelayed(runnable, delay);
    }

    private void getDistance(double fromWhereLat , double fromWhereLon) {
        DistanceCalculation distanceCalculation = new DistanceCalculation(getActivity(), fromWhereLat,
                fromWhereLon);
        distanceCalculation.setTransmissionTextListener(new TransmissionText() {
            @Override
            public void transmissionText(String str) {
                if (!str.equals("")) {
                    if (isVisible()) {
                        distanceTextView.setText(getString(R.string.driver_from_you) + " " + str + " " + getString(R.string.km));
                    }
                }

            }
        });
    }

    private void getDistance(double fromWhereLat , double fromWhereLon, double toWhereLat , double toWhereLon) {
        DistanceCalculation distanceCalculation = new DistanceCalculation(getActivity(), new LatLng(fromWhereLat, fromWhereLon),
                new LatLng(toWhereLat, toWhereLon));
        distanceCalculation.setTransmissionTextListener(new TransmissionText() {
            @Override
            public void transmissionText(String str) {
                if (!str.equals("")) {
                    if (isVisible()) {
                        distanceTextView.setText(getString(R.string.driver_from_you) + " " + str + " " + getString(R.string.km));
                    }
                }

            }
        });
    }

    private void getDriverLocation(){
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                mapContainer.setVisibility(View.VISIBLE);
                if(mMap == null){
                    return;
                }

                try {
                    mMap.clear();
                    JSONObject jsonObject = new JSONObject(str);
                    double latitude = jsonObject.getDouble("latitude");
                    double longitude = jsonObject.getDouble("longitude");
                    getDistance(latitude, longitude,  Double.parseDouble(proposal.getLatitude().replace(",", ".")), Double.parseDouble(proposal.getLongitude().replace(",", ".")));
                    passengerMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(proposal.getLatitude().replace(",", ".")), Double.parseDouble(proposal.getLongitude().replace(",", "."))))
                            .title("Passenger")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    driverMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(latitude, longitude))
                            .title("Driver")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(passengerMarker.getPosition());
                    builder.include(driverMarker.getPosition());
                    LatLngBounds bounds = builder.build();
                    int padding = 80; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    mMap.moveCamera(cu);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                //Utils.showToast(getActivity(), "Код ошибки: "+ errorCode );
                mapContainer.setVisibility(View.GONE);
            }
        });
        passengerRequester.getUserPosition(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), driver.getUid_d());
    }

    public void getListOffers() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (isVisible()) {
                    swipeContainer.setRefreshing(false);

                    if (!str.equals(Constants.OK)) {
                        try {
                            JSONArray jsonArray = new JSONArray(str);

                            if (jsonArray.length() != 0) {
                                for (int n = 0; n < jsonArray.length(); n++) {
                                    JSONObject object = jsonArray.getJSONObject(n);
                                    Proposal proposal = new Proposal(object);

                                    if (isVisible() && ListOfDriverProposalsNewFragment.this.proposal.getOrder_uid().equals(proposal.getOrder_uid())) {
                                        for (int i = 0; i < proposal.getItemOfferDrivers().size(); i++) {
                                            if (proposal.getItemOfferDrivers().get(i).getStatus_d().equals(Constants.PASSENGER_DRIVER_ACCEPT_ORDER)) {
                                                id_d = proposal.getItemOfferDrivers().get(i).getId_d();
                                                callButton.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        driversArrayList.clear();
                                        driversArrayList = proposal.getItemOfferDrivers();
                                        setUpAdapter();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                if (isVisible() && (((PassengerFragmentActivity) getActivity()).getTabPosition() == 1)) {
                    swipeContainer.setRefreshing(false);

                    if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
                        MainFragmentActivity.isAccessTokenGone(true);
                        Utils.showToast(getActivity(), getResources().getString(R.string.not_find_user_with_access_token));
                        getActivity().finish();

                    } else if (errorCode == Constants.NO_NEW_ORDERS) {
                        driversArrayList.clear();

                        if (listViewSwipeAdapter != null)
                            setUpAdapter();

                    } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.not_all_fields));

                    } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.json_error));

                    } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.no_network));
                    }
                }
            }
        });
        swipeContainer.setRefreshing(true);
        passengerRequester.getListOfProposals(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));
    }

    private void backForAuthorization(int errorCode) {
        if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            MainFragmentActivity.isAccessTokenGone(true);
            getActivity().finish();
        }
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void setOffersArray(Proposal arrayOffer) {
        this.proposal = arrayOffer;
    }

    public void checkSetOfferErrorCode(int errorCode) {
        if (errorCode == Constants.FAILED_TO_WRITE_IN_DB) {
            Utils.showToast(getActivity(), getResources().getString(R.string.failed_to_write_in_db));

        } else if (errorCode == Constants.NO_REQUEST_IN_DB) {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_request_in_db));

        } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getResources().getString(R.string.not_all_fields));

        } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
            Utils.showToast(getActivity(), getResources().getString(R.string.json_error));

        } else if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            Utils.showToast(getActivity(), getResources().getString(R.string.not_find_user_with_access_token));
            MainFragmentActivity.isAccessTokenGone(true);
            getActivity().finish();

        } else if (errorCode == Constants.CASH_EQUALS_ZERO) {
            Utils.showToast(getActivity(), getResources().getString(R.string.cash_equals_zero));

        } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_network));

        } else if (errorCode == Constants.FAILED_INCORRECT_VALUE_STATUS_P) {
            Utils.showToast(getActivity(), getResources().getString(R.string.incorrect_value_status_p));

        } else {
            Utils.showToast(getActivity(), "Code: " + errorCode);
        }
    }

    @Override
    public void onAcceptClickButton(int position) {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                hideProgressDialog();
                Utils.showToast(getActivity(), getString(R.string.driver_notified));
                getListOffers();
            }

            @Override
            public void onMessageError(int errorCode) {
                hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });

        passengerRequester.changeOffer(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                proposal.getOrder_uid(), driversArrayList.get(position).getUid_d(),
                driversArrayList.get(position).getCash_d(), Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL);
        showProgressDialog();
    }

    @Override
    public void onRejectClickButton(final int position) {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                hideProgressDialog();
                setTimeRefreshForList();
                getListOffers();
            }

            @Override
            public void onMessageError(int errorCode) {
                hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });

        passengerRequester.changeOffer(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                proposal.getOrder_uid(), driversArrayList.get(position).getUid_d(),
                driversArrayList.get(position).getCash_d(), Constants.PASSENGER_REJECT_ORDER);
        showProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible() && fragmentManager.findFragmentById(R.id.fragments_container)
                instanceof ListOfDriverProposalsNewFragment) {
            if (showDriver){

            }else{
                getListOffers();
                setTimeRefreshForList();

            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (handler != null && runnable != null)
            handler.removeCallbacks(runnable);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
         getDriverLocation();
         setTimeRefreshForMap();
    }


}