package com.mobapply.taxitter.ui.passenger;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ViewPagerAdapter;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.ui.BaseFragmentActivity;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.DriverRequester;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class PassengerFragmentActivity extends BaseFragmentActivity implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    private final String PASSENGER_MENU_FRAGMENT = "passenger_menu_fragment";
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences, sharedPref;
    private ImageLoader imageLoader;
    private TabHost tabHost;
    private ViewPager viewPager;
    private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, PassengerFragmentActivity.TabInfo>();
    private ViewPagerAdapter viewPagerAdapter;
    private View mCustomView;
    private Button menuButton;
    private TextView newOrderTextView, myOrdersTextView;
    private boolean isResumed = false;
    private boolean isMenuVisible;
    private boolean hasDetaileOffer;
    private int tabPosition;

    private static boolean isAccessTokenGone = false;
    private static boolean isLogOff = false;
    private static boolean isChangeRole = false;

    private class TabInfo {
        private String tag;
        private OrderTaxiFragment orderTaxiFragment;
        private ListOfOwnOrders listOfOwnOrders;

        public TabInfo(String tab1, OrderTaxiFragment orderTaxiFragment) {
            this.tag = tab1;
            this.orderTaxiFragment = orderTaxiFragment;
        }

        public TabInfo(String tab2, ListOfOwnOrders listOfOwnOrders) {
            this.tag = tab2;
            this.listOfOwnOrders = listOfOwnOrders;
        }
    }

    class TabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_swipe_layout);
        fragmentManager = getFragmentManager();
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getSharedPreferences(getString(R.string.pref_role_key), 0);
        imageLoader = ImageLoader.getInstance();
        ActionBar mActionBar = getActionBar();
        registerReceiver(receiverListOfDriverProposal, new IntentFilter(Constants.BROADCAST_ACTION));
        LayoutInflater mInflater = LayoutInflater.from(this);
        this.initialiseTabHost();

        DriverRequester driverRequester = new DriverRequester(this);
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {}

            @Override
            public void onMessageError(int errorCode) {}
        });

        driverRequester.changeRole(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), Constants.DRIVER_ROLE);
        sharedPref.edit().putString(Constants.ROLE_SATE, Constants.DRIVER_ROLE).apply();
        this.intialiseViewPager();

        mCustomView = mInflater.inflate(R.layout.action_bar_custom, null);
        menuButton = (Button) mCustomView.findViewById(R.id.buttonMenu);

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (!isMenuVisible) {
                    openMenuFragment();
                    setActionBarDark();

                } else {
                    fragmentManager.popBackStack();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setActionBarYellow();
                        }
                    }, 600);
                }

                isMenuVisible = !isMenuVisible;
            }
        });

        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            mActionBar.setCustomView(mCustomView);
            mActionBar.setDisplayShowCustomEnabled(true);
        }

        if (sharedPreferences.getBoolean(Constants.DRIVER_ACTION_ONE_OR_THREE, false)) {
            tabHost.setCurrentTab(1);
            sharedPreferences.edit().remove(Constants.DRIVER_ACTION_ONE_OR_THREE).apply();
        }

        if (sharedPreferences.getBoolean(Constants.NEW_DRIVER_PROPOSAL, false)) {
            tabHost.setCurrentTab(1);
            sharedPreferences.edit().remove(Constants.NEW_DRIVER_PROPOSAL).apply();
        }
    }

    private BroadcastReceiver receiverListOfDriverProposal = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isResumed) {
                if (intent.hasExtra(Constants.GO_ROUTE) && intent.getBooleanExtra(Constants.GO_ROUTE, false)) {
                    if (getFragmentManager().findFragmentById(R.id.fragments_container) instanceof ListOfDriverProposalsNewFragment) {
                        sharedPreferences.edit().remove(Constants.DRIVER_ACTION_ONE_OR_THREE).apply();
                       // ((ListOfDriverProposalsNewFragment)getFragmentManager().findFragmentById(R.id.fragments_container)).getListOffers();
                        ((ListOfDriverProposalsNewFragment)getFragmentManager().findFragmentById(R.id.fragments_container)).refresh(true);
                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.cancelAll();
                        Utils.showAlertDialog(PassengerFragmentActivity.this, getString(R.string.driver_accept_pass_order));
                    }
                } else if (intent.hasExtra(Constants.NEW_DRIVER_PROPOSAL)
                        && intent.getBooleanExtra(Constants.NEW_DRIVER_PROPOSAL, false)) {

                    if (getFragmentManager().findFragmentById(R.id.fragments_container)
                            instanceof ListOfDriverProposalsNewFragment) {

                        ((ListOfDriverProposalsNewFragment)getFragmentManager()
                                .findFragmentById(R.id.fragments_container)).getListOffers();
                        sharedPreferences.edit().remove(Constants.NEW_DRIVER_PROPOSAL).apply();
                    }
                }
            }
        }
    };

    protected void setActionBarDark() {
        menuButton.setBackgroundResource(R.drawable.ic_action_menu_yellow);
        mCustomView.setBackgroundResource(R.drawable.action_bar_dark);
    }

    protected void setActionBarYellow() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                menuButton.setBackgroundResource(R.drawable.ic_action_menu);
                mCustomView.setBackgroundResource(R.drawable.action_bar);
            }
        }, 600);
    }

    public void setMenuVisible(boolean state) {
        isMenuVisible = state;
    }

    private void openMenuFragment() {
        PassengerMenuFragment passengerMenuFragment = new PassengerMenuFragment();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close,
                        R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close)
                .add(R.id.menu_container, passengerMenuFragment, PASSENGER_MENU_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    private void intialiseViewPager() {
        List<Fragment> fragments = new Vector<Fragment>();
        fragments.add(Fragment.instantiate(this, OrderTaxiFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, ListOfOwnOrders.class.getName()));
        this.viewPagerAdapter = new ViewPagerAdapter(super.getFragmentManager(), fragments);
        this.viewPager = (ViewPager)super.findViewById(R.id.viewpager);
        this.viewPager.setAdapter(this.viewPagerAdapter);
        this.viewPager.setOnPageChangeListener(this);
    }

    private void initialiseTabHost() {
        tabHost = (TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();
        TabInfo tabInfo = null;

        OrderTaxiFragment orderTaxiFragment = new OrderTaxiFragment();
        ListOfOwnOrders listOfOwnOrders = new ListOfOwnOrders();

        PassengerFragmentActivity.AddTab(this, this.tabHost, this.tabHost.newTabSpec("Tab1").setIndicator(getString(R.string.new_order)),
                (tabInfo = new TabInfo("Tab1", orderTaxiFragment)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_active);

        newOrderTextView = (TextView) tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
        newOrderTextView.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        newOrderTextView.setTextColor(getResources().getColor(R.color.tab_title_brown));
        newOrderTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_new_order_icon_pressed, 0, 0, 0);
        newOrderTextView.setCompoundDrawablePadding(10);

        PassengerFragmentActivity.AddTab(this, this.tabHost, this.tabHost.newTabSpec("Tab2").setIndicator(getString(R.string.my_orders)),
                (tabInfo = new TabInfo("Tab2", listOfOwnOrders)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_not_active);

        myOrdersTextView = (TextView) tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        myOrdersTextView.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        myOrdersTextView.setTextColor(getResources().getColor(R.color.white));
        myOrdersTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.start_tab, 0, 0, 0);
        myOrdersTextView.setCompoundDrawablePadding(10);

        tabHost.setOnTabChangedListener(this);
    }

    private static void AddTab(PassengerFragmentActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        tabSpec.setContent(activity.new TabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    public void onTabChanged(String tag) {
        tabPosition = this.tabHost.getCurrentTab();
        viewPager.setCurrentItem(tabPosition);

        if (tabPosition == 1) {
            tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_not_active);
            tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_active);

            myOrdersTextView.setTextColor(getResources().getColor(R.color.tab_title_brown));
            newOrderTextView.setTextColor(getResources().getColor(R.color.white));

            newOrderTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_new_order_icon, 0, 0, 0);

        } else {
            tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_active);
            tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_not_active);

            myOrdersTextView.setTextColor(getResources().getColor(R.color.white));
            newOrderTextView.setTextColor(getResources().getColor(R.color.tab_title_brown));

            newOrderTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_new_order_icon_pressed, 0, 0, 0);
        }
    }

    public int getTabPosition() {
        return tabPosition;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        this.tabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        Fragment fr = fragmentManager.findFragmentById(R.id.menu_container);

        if (fr instanceof PassengerMenuFragment) {
            isMenuVisible = !isMenuVisible;
            fragmentManager.popBackStack();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setActionBarYellow();
                }
            }, 600);

        } else if (fr instanceof PassengerProfileFragment || fr instanceof FavoriteAddressFragment) {

            for (int i = 0; i < count; i++) {
                String name = fragmentManager.getBackStackEntryAt(i).getName();

                if (name.equals(Constants.PASSENGER_LIST_OF_DRIVER_PROPOSALS)) {

                    for (int c = 0; c < count; c++) {
                        name = fragmentManager.getBackStackEntryAt(c).getName();

                        if (name.equals(Constants.PASSENGER_DETAILE_OF_PROPOSALS))
                            hasDetaileOffer = true;
                    }

                    if (hasDetaileOffer) {
                        fragmentManager.popBackStack(Constants.PASSENGER_DETAILE_OF_PROPOSALS, 0);
                    } else {
                        fragmentManager.popBackStack(Constants.PASSENGER_LIST_OF_DRIVER_PROPOSALS, 0);
                    }
                    break;

                } else {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    break;
                }
            }

        } else if (fragmentManager.findFragmentById(R.id.fragments_container) instanceof ListOfDriverProposalsNewFragment) {
            ListOfOwnOrders.setIsNeedRefreshOrders(true);
            fragmentManager.popBackStack();

        }  else if (fragmentManager.findFragmentById(R.id.fragments_container) instanceof DetailsProposal) {
            fragmentManager.popBackStack();

        } else {
            super.onBackPressed();
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            finish();
        }
    }

    public static void isChangeRole(boolean state) {
        isChangeRole = state;
    }

    public static void isAccessTokenGone(boolean state) {
        isAccessTokenGone = state;
    }

    public static void isLogOff(boolean state) {
        isLogOff = state;
    }

    @Override
    public void onResume() {
        //  If no accsess_token back to the screen authentication, if log_off back to the first screen
        super.onResume();
        NotificationManager notificationManager = Utils.getNotificationManager(this);
        notificationManager.cancelAll();

        isResumed = true;
        registerReceiver(receiverListOfDriverProposal, new IntentFilter(Constants.BROADCAST_ACTION));

        if (isAccessTokenGone) {
            MainFragmentActivity.isAccessTokenGone(true);
            isAccessTokenGone = false;
            finish();
        }

        if (isLogOff) {
            MainFragmentActivity.isLogOff(true);
            isLogOff = false;
            finish();
        }

        if (isChangeRole){
            isChangeRole = false;
            finish();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        isResumed = false;
        sharedPreferences.edit().putBoolean(Constants.APP_OPENED, false).apply();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imageLoader.clearCache();
        unregisterReceiver(receiverListOfDriverProposal);
    }
}