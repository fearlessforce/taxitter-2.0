package com.mobapply.taxitter.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.ui.driver.DriverFragmentActivity;
import com.mobapply.taxitter.ui.passenger.PassengerFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;

/**
 * FragmentActivity that controls the register/authorization layers.
 */
public class MainFragmentActivity extends BaseFragmentActivity {
    private final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    protected static boolean isAccessTokenGone;
    protected static boolean isLogOff;
    private final String PROPERTY_APP_VERSION = "appVersion";
    protected final String SENDER_ID = "234906631023";
    private final String TAG = "GCMDemo";
    private final String LAUCHING_TAG = "enter_tag";

    private ImageLoader imageLoader;
    private FragmentManager fragmentManager;
    private LaunchingFragment launchingFragment;
    private SharedPreferences sharedPreferences, sharedPref;
    private GoogleCloudMessaging gcm;
    private String regId;
    private GoogleAnalytics analytics;
    private Tracker tracker;

    private static final int REQUEST_PERMISSIONS = 1;

    private static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startService(new Intent(this, GPSService.class));
        Fabric.with(this, new Crashlytics());
        // configure Flurry
        FlurryAgent.setLogEnabled(false);

        // init Flurry
        FlurryAgent.init(this, getString(R.string.flurry_api_key));
        // Google Analytics implementation
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker(getString(R.string.google_analytics_property_id)); // Replace with actual tracker/property Id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);

        setContentView(R.layout.fragment_container);

        verifyPermissions(this);

        imageLoader = ImageLoader.getInstance();
        imageLoader.initCache(this.getApplicationContext());

        if (getIntent().getBooleanExtra(Constants.EXIT, false))
            finish();

        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getSharedPreferences(getString(R.string.pref_role_key), 0);
        fragmentManager = getFragmentManager();
        launchingFragment = new LaunchingFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.container, launchingFragment, LAUCHING_TAG)
                .commit();

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regId = getRegistrationId();

            if (regId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }


    public static void verifyPermissions(Activity activity) {
        // Check if we have write permission
        int permission1 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permission2 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission3 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        int permission4 = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permission1 != PackageManager.PERMISSION_GRANTED ||
                permission2 != PackageManager.PERMISSION_GRANTED ||
                permission3 != PackageManager.PERMISSION_GRANTED ||
                permission4 != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS,
                    REQUEST_PERMISSIONS
            );
        }
    }




    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {

            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();

            } else {
                Utils.showToast(MainFragmentActivity.this, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId() {
        String registrationId = sharedPref.getString(Constants.PROPERTY_REG_ID, "");

        if (registrationId == null || registrationId.length() == 0) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = sharedPref.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion();

        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private int getAppVersion() {
        PackageInfo packageInfo = null;
        PackageManager packageManager;

        try {
            packageManager = getPackageManager();

            if (packageManager != null) {
                packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            }

            return packageInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void registerInBackground() {
        RegistrGCMAsynckTask registrGCMAsynckTask = new RegistrGCMAsynckTask();
        registrGCMAsynckTask.execute();
    }

    public class RegistrGCMAsynckTask extends AsyncTask<String, String, String> {
        String msg = "";

        public RegistrGCMAsynckTask() {}

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                }
                regId = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regId;
                // Persist the registration ID - no need to register again.

            } catch (IOException ex) {
                msg = ex.getMessage();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);

            Utils.hideProgressDialog();
            if (regId.isEmpty() && msg.equals("SERVICE_NOT_AVAILABLE")) {
                Utils.showToast(MainFragmentActivity.this, getString(R.string.no_network));

            } else {
                storeRegistrationId(regId);
            }
        }
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    private void storeRegistrationId(String regId) {
        int appVersion = getAppVersion();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.apply();
    }

    public static void isAccessTokenGone(boolean state) {
        isAccessTokenGone = state;
    }

    public static boolean getAccessTokenState() {
        return isAccessTokenGone;
    }

    public static void isLogOff(boolean state) {
        isLogOff = state;
    }

    public static boolean getIsLogOff() {
        return isLogOff;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment fr = getFragmentManager().findFragmentById(R.id.container);

        if (fr instanceof LaunchingFragment) {
            super.onBackPressed();

        } else if (fr instanceof EnterFragment) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else if (fr instanceof ChoiceOfRolesFragment) {
            finish();

        } else {
            fragmentManager.popBackStack();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();

        if (isAccessTokenGone)
            fragmentManager.popBackStack();

        if (isLogOff) {
            sharedPreferences.edit().clear().apply();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (sharedPreferences.getBoolean(Constants.DRIVER_ACTION_ONE_OR_THREE, false))
            startActivity(new Intent(this, PassengerFragmentActivity.class));

        if (sharedPreferences.getBoolean(Constants.NEW_DRIVER_PROPOSAL, false) && !sharedPref.getString(Constants.ROLE_SATE,"").equals(Constants.CHANGE_ROLE_STATE))
            startActivity(new Intent(this, PassengerFragmentActivity.class));

        if (sharedPreferences.getBoolean(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL, false))
            startActivity(new Intent(this, DriverFragmentActivity.class));

        if (sharedPreferences.getBoolean(Constants.NEW_ORDER, false))
            startActivity(new Intent(this, DriverFragmentActivity.class));

        if (sharedPreferences.getBoolean(Constants.PASSENGER_CANCEL_ORDER_DIALOG, false)) {
            startActivity(new Intent(this, DriverFragmentActivity.class));
            sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER).apply();
            sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
            sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER_DIALOG).apply();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imageLoader.clearCache();
    }

}