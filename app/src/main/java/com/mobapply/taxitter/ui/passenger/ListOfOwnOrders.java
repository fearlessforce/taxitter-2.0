package com.mobapply.taxitter.ui.passenger;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ListOffersArrayAdapter;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.Proposal;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListOfOwnOrders extends Fragment {
    private static boolean isNeedRefreshListOrders = false;
    private SharedPreferences sharedPreferences;
    private FragmentManager fragmentManager;
    private ListView listViewOrders;
    private ArrayList<Proposal> proposalArrayList;
    private ListOffersArrayAdapter listOffersArrayAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isCreated = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_order_with_refresh, container, false);
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        listViewOrders = (ListView)view.findViewById(R.id.lvItems);

        if (proposalArrayList == null)
            proposalArrayList = new ArrayList<Proposal>();

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListOffers();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        listViewOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemNumber, long l) {
                if (!swipeRefreshLayout.isRefreshing()) {
                    Proposal proposal = proposalArrayList.get(itemNumber);
                    ListOfDriverProposalsNewFragment listOfDriverProposalsFragment = new ListOfDriverProposalsNewFragment();
                    listOfDriverProposalsFragment.setOffersArray(proposal);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragments_container, listOfDriverProposalsFragment, Constants.PASSENGER_LIST_OF_DRIVER_PROPOSALS)
                            .addToBackStack(Constants.PASSENGER_LIST_OF_DRIVER_PROPOSALS)
                            .commit();
                }
            }
        });

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (isNeedRefreshListOrders) {
                    getListOffers();
                    isNeedRefreshListOrders = false;
                }
            }
        });

        getListOffers();
        return view;
    }

    public static void setIsNeedRefreshOrders(boolean b) {
        isNeedRefreshListOrders = b;
    }

    private void getListOffers() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                swipeRefreshLayout.setRefreshing(false);

                if (!str.equals(Constants.OK)) {
                    try {
                        JSONArray jsonArray = new JSONArray(str);

                        if (jsonArray.length() != 0) {
                            proposalArrayList.clear();

                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject object = jsonArray.getJSONObject(n);
                                Proposal proposal = new Proposal(object);
                                proposalArrayList.add(proposal);
                            }

                            if (isVisible() && ((PassengerFragmentActivity)getActivity()).getTabPosition() == 1) {
                                listOffersArrayAdapter = new ListOffersArrayAdapter(getActivity(), proposalArrayList);
                                listViewOrders.setAdapter(listOffersArrayAdapter);
                                listOffersArrayAdapter.notifyDataSetChanged();
                                listViewOrders.invalidate();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                swipeRefreshLayout.setRefreshing(false);

                if (isVisible() && (((PassengerFragmentActivity)getActivity()).getTabPosition() == 1)) {

                    if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
                        MainFragmentActivity.isAccessTokenGone(true);
                        Utils.showToast(getActivity(), getResources().getString(R.string.not_find_user_with_access_token));
                        getActivity().finish();

                    } else if (errorCode == Constants.NO_NEW_ORDERS) {
                        proposalArrayList.clear();

                        if (listOffersArrayAdapter != null)
                            listOffersArrayAdapter.notifyDataSetChanged();

                    } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.not_all_fields));

                    } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.json_error));

                    } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.no_network));
                    }
                }
            }
        });
        passengerRequester.getListOfProposals(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isCreated && isVisible() && fragmentManager.findFragmentById(R.id.fragments_container) instanceof ListOfOwnOrders) {
            getListOffers();
        }

        isCreated = true;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if (isCreated)
                getListOffers();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isCreated = false;
    }
}