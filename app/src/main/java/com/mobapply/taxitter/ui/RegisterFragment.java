package com.mobapply.taxitter.ui;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.custom.ExtendedEditText;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.AuthenticationRequester;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterFragment extends Fragment {
    private final int NO_NETWORK_CONNECTION = 1019;
    private final int NOT_ALL_REQUIRED_FIELDS_ARE_FILLED = 12;
    private final int ERROR_JSON_FORMAT = 13;
    private final int NO_PRE_REGISTRATION_DATA = 11;
    private final int REGISTER_ALREADY_HAVE_SUCH_A_USER = 1;
    private final int REGISTER_CODE_DOES_NOT_MATCH = 2;
    private final int FAILED_TO_SEND_SMS_CODE = 1;

    private FragmentManager fragmentManager;
    private ExtendedEditText codeEditText;
    private Button enterButton;
    private ImageView helpImageView;
    private String phoneNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.enter_code_fragment, container, false);
        setupListener(view);
        fragmentManager = getFragmentManager();
        codeEditText = (ExtendedEditText)view.findViewById(R.id.editTextEnterCode);
        enterButton = (Button)view.findViewById(R.id.buttonEnterWithCode);
        helpImageView = (ImageView)view.findViewById(R.id.imageView);

        codeEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                if (result == EditorInfo.IME_ACTION_DONE) {
                    clearKeyboardAndFocus();

                } else if (result == EditorInfo.IME_ACTION_NEXT) {
                    clearKeyboardAndFocus();
                }
                return false;
            }
        });

        codeEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(final View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    clearKeyboardAndFocus();
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    clearKeyboardAndFocus();
                    return true;
                }
                return false;
            }
        });

        helpImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                clearKeyboardAndFocus();
                return false;
            }
        });

        helpImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.need_new_sms))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.get_new_code), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AuthenticationRequester authenticationRequester = new AuthenticationRequester(getActivity());
                                authenticationRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                                    @Override
                                    public void onMessageSuccess(String str) {
                                        Utils.hideProgressDialog();
                                    }

                                    @Override
                                    public void onMessageError(int errorCode) {
                                        checkResponseNewSmsErrorCode(errorCode);
                                    }
                                });
                                authenticationRequester.getNewSmsCode(getPhoneNumber(phoneNumber));
                                dialog.cancel();
                                Utils.showProgressDialog(getActivity(), getString(R.string.loading));
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = codeEditText.getText().toString();

                AuthenticationRequester authenticationRequester = new AuthenticationRequester(getActivity());
                authenticationRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        Utils.hideProgressDialog();
                        String password = "";

                        if (!str.equals(Constants.OK)) {
                            try {
                                JSONObject jsonObject = new JSONObject(str);

                                if (jsonObject != null)
                                    password = jsonObject.getString(Constants.PASSWORD);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            EnterFragment enterFragment = new EnterFragment();
                            enterFragment.setUserData(phoneNumber, password, true);
                            fragmentManager.beginTransaction()
                                    .add(R.id.container, enterFragment, Constants.ENTER_TAG)
                                    .addToBackStack(Constants.ENTER_STACK)
                                    .commit();
                        }
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        checkRegistrationErrorCode(errorCode);
                    }
                });
                authenticationRequester.register(getPhoneNumber(phoneNumber), code);
                Utils.showProgressDialog(getActivity(), getString(R.string.loading));
            }
        });

        return view;
    }

    /**
     * Get phone number with trim all space and other symbols
     * @param stringFromEditText
     * @return phone number
     */
    private String getPhoneNumber(String stringFromEditText) {
        return stringFromEditText.replaceAll("[^a-zA-Z0-9]", "");
    }

    private void checkResponseNewSmsErrorCode(int errorCode) {
        Utils.hideProgressDialog();

        if (errorCode == FAILED_TO_SEND_SMS_CODE) {
            Utils.showToast(getActivity(), getString(R.string.failed_to_send_sms_code));

        } else if (errorCode == NO_PRE_REGISTRATION_DATA) {
            Utils.showToast(getActivity(), getString(R.string.no_data_about_user_id));

        } else {
            othersError(errorCode);
        }
    }

    private void checkRegistrationErrorCode(int errorCode) {
        Utils.hideProgressDialog();

        if (errorCode == REGISTER_ALREADY_HAVE_SUCH_A_USER) {
            Utils.showToast(getActivity(), getString(R.string.already_have_such_a_user));

        } else if (errorCode == REGISTER_CODE_DOES_NOT_MATCH) {
            Utils.showToast(getActivity(), getString(R.string.code_not_match));

        } else if (errorCode == NO_PRE_REGISTRATION_DATA) {
            Utils.showToast(getActivity(), getString(R.string.no_data_about_user_id));

        } else {
            othersError(errorCode);
        }
    }

    private void othersError(int errorCode) {
        if (errorCode == NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getString(R.string.not_all_fields));

        } else if (errorCode == ERROR_JSON_FORMAT) {
            Utils.showToast(getActivity(), getString(R.string.json_error));

        } else if (errorCode == NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), getString(R.string.no_network));

        } else {
            Utils.showToast(getActivity(), getString(R.string.code) + errorCode);
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }

    private void clearKeyboardAndFocus() {
        Utils.hideKeyboard(getActivity());
        codeEditText.clearFocus();
        enterButton.requestFocus();
    }
}