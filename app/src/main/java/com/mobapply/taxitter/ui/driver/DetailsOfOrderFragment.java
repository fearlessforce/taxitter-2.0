package com.mobapply.taxitter.ui.driver;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.interfaces.TransmissionText;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.DistanceCalculation;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.DriverRequester;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

/**
 * The screen that displays detailed information about orderItem
 *
 * @see ListOrdersFragment
 */
public class DetailsOfOrderFragment extends Fragment {
    private final String REFUSE_CODE = "0";
    private final String CONFIRM_CODE = "1";
    private final int MAX_CASH = 99989;
    private final int STEP = 10;

    private View view;
    private Order order;
    private ArrayList<AdditionalRoute> dotsListsArray;
    private SharedPreferences sharedPreferences;
    private CircularProgressBar circularProgressBar;
    private Handler repeatUpdateHandler = new Handler();
    private ImageLoader imageLoader;
    private Dialog dialog;

    private EditText currentCashEditText;
    private TextView titleWhenTopTextView, whereToTopTextView, titleFromWhereTopTextView;
    private TextView whenTextView, fromWhereTextView;
    private TextView countPassengerTopTextView, countPassengerTextView;
    private TextView nickNameTextView;
    private TextView ratingTextView;
    private TextView commentTextView;
    private TextView distanceTopTextView;
    private ImageView smokerImageView, conditionerImageView, babySeatImageView;
    private ImageView photoImageView;
    private Button reduseCashButton, increaseCashButton;
    private Button confirmButton, refuseButton;
    private Button writeLetterButton;
    private Button closeButton;
    private ImageView imageViewLargePhoto;

    private String order_uid;
    private String comment;
    private String length;
    private String when;
    private String latitude, longitude;
    private String departure;
    private String cash;
    private String id;
    private String number;
    private String client;
    private String uid_client;
    private String distanceD = "0";
    private int rating;
    private int babyseat;
    private int ac, quantity, nonsmoking;
    private int cashValue;
    private boolean mAutoIncrement = false;
    private boolean mAutoDecrement = false;
    private double fromWhereLat;
    private double fromWhereLon;
    private boolean isNeedDialog;
    private String minCash;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.details_order_main, container, false);
        imageLoader = ImageLoader.getInstance();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        minCash = sharedPreferences.getString(Constants.MINCASHE, "");

        initUI();
        getDefaultPrice();
        getValueFromArray(order);
        createDialog();
        getDistance();
        setDataIntoUI();
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);

        refuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refuseOrder();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmOrder();
            }
        });

        increaseCashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cash = Integer.parseInt(currentCashEditText.getText().toString());
                if (isCorrectCashForInc()) {
                    if (cash < MAX_CASH)
                        currentCashEditText.setText("" + (cash + STEP));
                } else {
                    currentCashEditText.setText(minCash);
                    Utils.showToast(getActivity(), getString(R.string.min_cash_value) + ": " + minCash);
                }
            }
        });

        reduseCashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cash = Integer.parseInt(currentCashEditText.getText().toString());
                if (isCorrectCashForDec()) {
                    if (cash != 0)
                        currentCashEditText.setText("" + (cash - STEP));
                } else {
                    currentCashEditText.setText(minCash);
                    Utils.showToast(getActivity(), getString(R.string.min_cash_value) + ": " + minCash);
                }
            }
        });

        reduseCashButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                cashValue = Integer.parseInt(currentCashEditText.getText().toString());
                mAutoDecrement = true;
                repeatUpdateHandler.post(new RptUpdater());

                return false;
            }
        });

        reduseCashButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && mAutoDecrement) {
                    mAutoDecrement = false;
                }

                return false;
            }
        });

        increaseCashButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                cashValue = Integer.parseInt(currentCashEditText.getText().toString());
                mAutoIncrement = true;
                repeatUpdateHandler.post(new RptUpdater());

                return false;
            }
        });

        increaseCashButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && mAutoIncrement) {
                    mAutoIncrement = false;
                }

                return false;
            }
        });

        writeLetterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setComment();
            }
        });

        photoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLargePhoto();
            }
        });

        return view;
    }

    private void getDefaultPrice() {
        minCash = sharedPreferences.getString(Constants.MINCASHE, "");
        if (minCash.length() == 0) {
            minCash = ""+STEP;
        }
    }

    private void initUI() {
        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.smoothCircleBar);
        distanceTopTextView = (TextView)view.findViewById(R.id.textViewDistanceTop);
        titleWhenTopTextView = (TextView)view.findViewById(R.id.textViewTitleOrder);
        titleFromWhereTopTextView = (TextView)view.findViewById(R.id.textViewFromWhereTopOrder);
        whereToTopTextView = (TextView)view.findViewById(R.id.textViewWhereToTopOrder);
        photoImageView = (ImageView)view.findViewById(R.id.imageViewPhotoOrder);
        nickNameTextView = (TextView)view.findViewById(R.id.textViewNicknameOrder);
        ratingTextView = (TextView)view.findViewById(R.id.textViewRatingOrder);
        countPassengerTopTextView = (TextView)view.findViewById(R.id.textViewPassengerOrder);
        commentTextView = (TextView)view.findViewById(R.id.textViewCommentOrder);
        fromWhereTextView = (TextView)view.findViewById(R.id.textViewRouteDotsFromWhere);
        countPassengerTextView = (TextView)view.findViewById(R.id.textViewCountPassengerOrder);
        whenTextView = (TextView)view.findViewById(R.id.textViewWhenOrder);
        smokerImageView = (ImageView)view.findViewById(R.id.imageViewSmoker);
        babySeatImageView = (ImageView)view.findViewById(R.id.imageViewBabySeat);
        conditionerImageView = (ImageView)view.findViewById(R.id.imageViewConditioner);
        reduseCashButton = (Button)view.findViewById(R.id.buttonMinusCash);
        increaseCashButton = (Button)view.findViewById(R.id.buttonPlusCash);
        currentCashEditText = (EditText)view.findViewById(R.id.editTextCashEditField);
        writeLetterButton = (Button)view.findViewById(R.id.buttonWriteLetter);
        refuseButton = (Button)view.findViewById(R.id.buttonRefuse);
        confirmButton = (Button)view.findViewById(R.id.buttonConfirm);
    }

    private void getValueFromArray(Order item) {
        order_uid = item.getOrder_uid() == null ? "" : item.getOrder_uid();
        number = item.getNumber();
        when = item.getWhen() == null ? "" : item.getWhen();
        rating = item.getRating();
        id = item.getId();
        uid_client = item.getUid_client() == null ? "" : item.getUid_client();
        latitude = item.getLatitude() == null ? "" : item.getLatitude();
        longitude = item.getLongitude() == null ? "" : item.getLongitude();
        departure = item.getDeparture() == null ? "" : item.getDeparture();
        cash = item.getCash() == null ? "": item.getCash();
        babyseat = item.getBabyseat();
        ac = item.getAc();
        nonsmoking = item.getNonsmoking();
        quantity = item.getQuantity();
        comment = item.getComment() == null ? "" : item.getComment();
        length = item.getLength() == null ? "" : item.getLength();
        client = item.getClient() == null ? "" : item.getClient();

        dotsListsArray = item.getAdditionalRoutes();
        fromWhereLat = Double.parseDouble(latitude.replace(",", "."));
        fromWhereLon = Double.parseDouble(longitude.replace(",", "."));
    }

    private void getDistance() {
        DistanceCalculation distanceCalculation = new DistanceCalculation(getActivity(), fromWhereLat,
                fromWhereLon);
        distanceCalculation.setTransmissionTextListener(new TransmissionText() {
            @Override
            public void transmissionText(String str) {
                distanceD = str;
                if (!str.equals("")) {
                    if (isVisible()) {
                        distanceTopTextView.setText(distanceD + " " + getString(R.string.km));
                        Utils.hideProgressDialog();
                    }
                }
            }
        });
        Utils.showProgressDialog(getActivity(), getString(R.string.loading));
    }

    private void setDataIntoUI() {
        titleWhenTopTextView.setText(when.length() != 0 ? when : getString(R.string.soon));
        titleFromWhereTopTextView.setText(departure);
        whereToTopTextView.setText(dotsListsArray.get(dotsListsArray.size()-1).getWayaddress());

        nickNameTextView.setText(client);
        ratingTextView.setText("" + rating);
        countPassengerTopTextView.setText("" + quantity);

        if (comment.length() != 0) {
            commentTextView.setVisibility(View.VISIBLE);
            commentTextView.setText(comment);
        }

        fromWhereTextView.setText(departure);

        for (int i = 0; i <= dotsListsArray.size(); i++) {
            ViewGroup inclusionViewGroup = (ViewGroup)view.findViewById(R.id.routeLinearLayout);
            View child1 = LayoutInflater.from(getActivity()).inflate(
                    R.layout.details_order_item_dots, null);
            inclusionViewGroup.addView(child1);
            TextView left = (TextView)child1.findViewById(R.id.textViewLeftImgOrText);
            TextView right = (TextView)child1.findViewById(R.id.textViewRouteDotsFromWhere);
            ImageView imageDoterLine = (ImageView)child1.findViewById(R.id.imageViewDoterLine);

            if (i == dotsListsArray.size()) {
                left.setText("");
                right.setText(length.length() == 0 ? "" : length + " км" );
                right.setTextSize(14);
                right.setTextColor(getResources().getColor(R.color.white_gray));
                imageDoterLine.setVisibility(View.INVISIBLE);

            } else {
                left.setText("" + (i + 1));
                right.setText(dotsListsArray.get(i).getWayaddress());
            }
        }

        countPassengerTextView.setText(quantity + " " + getString(R.string.passengers));
        whenTextView.setText(when.length() != 0 ? when : getString(R.string.soon));

        babySeatImageView.setImageResource(babyseat == 1 ? R.drawable.conditions_babyseat_pressed_details_order : R.drawable.conditions_babyseat_details_order);
        smokerImageView.setImageResource(nonsmoking == 1 ? R.drawable.conditions_smoker_pressed_details_order : R.drawable.conditions_smoker_details_order);
        conditionerImageView.setImageResource(ac == 1 ? R.drawable.conditions_conditioner_pressed_details_order : R.drawable.conditions_conditioner_details_order);
        currentCashEditText.setText(cash.equals("") ? "0" : cash);
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, photoImageView, size);
                                        }
                                    } else {
                                        imageLoader.DisplayImage(urlImage, imageViewLargePhoto, size);
                                        if (isNeedDialog && isVisible()) {
                                            Utils.hideProgressDialog();
                                            dialog.show();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.showToast(getActivity(), "" + errorCode);
            }
        });
        commonRequest.getOtherAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size, uid_client);
    }

    private void refuseOrder() {
        DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (isVisible()) {
                    hideCircularBar();
                    ListOrdersFragment.setIsNeedRefresh(true);
                    getFragmentManager().popBackStack();
                }
            }
            @Override
            public void onMessageError(int errorCode) {
                if (isVisible()) {
                    hideCircularBar();
                    checkErrorCode(errorCode);
                }
            }
        });
        driverRequester.setOffers(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), order_uid,
                currentCashEditText.getText().toString(), REFUSE_CODE,
                writeLetterButton.getText().toString().equals(getString(R.string.write_letter))
                        ? "" : writeLetterButton.getText().toString(), distanceD);
        showCircularBar();
    }


    private void confirmOrder() {
        DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (isVisible()) {
                    hideCircularBar();
                    ListOrdersFragment.setIsNeedRefresh(true);
                    getFragmentManager().popBackStack();
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                if (isVisible()) {
                    hideCircularBar();
                    checkErrorCode(errorCode);
                }
            }
        });

        driverRequester.setOffers(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), order_uid,
                currentCashEditText.getText().toString(), CONFIRM_CODE,
                writeLetterButton.getText().toString().equals(getString(R.string.write_letter)) ? ""
                        : writeLetterButton.getText().toString(), distanceD);
        showCircularBar();
    }

    private void setComment() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View dialog_layout = inflater.inflate(R.layout.dialog_write_letter, (ViewGroup) view.findViewById(R.id.root));
        final EditText editTextLetter = (EditText) dialog_layout.findViewById(R.id.editTextLetter);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.write_letter);
        builder.setView(dialog_layout)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.hideKeyboard(getActivity());
                            }
                        }, 300);
                        String currentText = editTextLetter.getText().toString();
                        writeLetterButton.setText(currentText.length() != 0 ? editTextLetter.getText().toString()
                                : getString(R.string.write_letter));

                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Utils.hideKeyboard(getActivity());
                            }
                        }, 300);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void openLargePhoto() {
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        if ((Integer)imageViewLargePhoto.getTag() != R.drawable.round67) {
            dialog.show();
        } else {
            getAvatarFromServer(Constants.IMG_LARGE_SIZE);
            isNeedDialog = true;
            Utils.showProgressDialog(getActivity(), getString(R.string.loading));
        }
    }

    public void createDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.large_foto_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialog.findViewById(R.id.button_close);
        imageViewLargePhoto = (ImageView) dialog.findViewById(R.id.imageViewLargePhoto);
        imageViewLargePhoto.setTag(R.drawable.round67);
    }

    private void hideCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).progressiveStop();
        circularProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).start();
        circularProgressBar.setVisibility(View.VISIBLE);
    }

    private void checkErrorCode(int errorCode) {
        if (errorCode == Constants.FAILED_TO_WRITE_IN_DB) {
            Utils.showToast(getActivity(), getString(R.string.failed_to_write_in_db));

        } else if (errorCode == Constants.NO_REQUEST_IN_DB) {
            Utils.showToast(getActivity(), getString(R.string.no_request_in_db));

        } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getString(R.string.not_all_fields));

        } else if (errorCode == Constants.PROFILE_IS_NOT_FILLED) {
            Utils.showAlertDialog(getActivity(), getString(R.string.profile_is_not_filled));

        } else if (errorCode == Constants.CASH_EQUALS_ZERO) {
            Utils.showToast(getActivity(), getString(R.string.cash_equals_zero));

        } else if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            Utils.showToast(getActivity(), getString(R.string.not_find_user_with_access_token));

        } else {
            Utils.showToast(getActivity(), "Code: " + errorCode);
        }
    }

    public void setDetailItem(Order item) {
        this.order = item;
    }

    /**
     * Class that increment or decrement cashValue
     * when was longClick(increaseCashButton or reduseCashButton) event
     */
    class RptUpdater implements Runnable {
        private static final long REP_DELAY = 50;

        public void run() {
            if (mAutoIncrement) {

                if (isCorrectCashForInc()) {
                    increment();
                    repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
                } else {
                    currentCashEditText.setText(minCash);
                    Utils.showToast(getActivity(), getString(R.string.min_cash_value) + ": " + minCash);
                }


            } else if (mAutoDecrement) {
                if (isCorrectCashForDec()) {
                    decrement();
                    repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);

                } else {
                    currentCashEditText.setText(minCash);
                    Utils.showToast(getActivity(), getString(R.string.min_cash_value) + ": " + minCash);
                }
            }
        }
    }

    public void decrement() {
        if (cashValue != 0) {
            cashValue -= STEP;
            currentCashEditText.setText("" + cashValue);
        }
    }

    private boolean isCorrectCashForDec() {
        int minash = Integer.parseInt(minCash);
        int currentCash = Integer.parseInt(currentCashEditText.getText().toString());
        return (currentCash - STEP) >= minash;
    }

    private boolean isCorrectCashForInc() {
        int minash = Integer.parseInt(minCash);
        int currentCash = Integer.parseInt(currentCashEditText.getText().toString());
        return (currentCash + STEP) >= minash;
    }

    public void increment() {
        if (cashValue < MAX_CASH) {
            cashValue += STEP;
            currentCashEditText.setText("" + cashValue);
        }
    }
}