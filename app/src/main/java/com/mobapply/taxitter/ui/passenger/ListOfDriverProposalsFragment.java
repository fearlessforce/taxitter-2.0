package com.mobapply.taxitter.ui.passenger;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ListViewSwipeAdapter;
import com.mobapply.taxitter.interfaces.SwipeButtonClickListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Driver;
import com.mobapply.taxitter.model.Proposal;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListOfDriverProposalsFragment extends Fragment implements SwipeButtonClickListener {
    private static boolean isNeedRefreshListOffers = false;
    private ArrayList<AdditionalRoute> additionalRoutesArrayList;
    private ArrayList<Driver> driversArrayList;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private ListViewSwipeAdapter listViewSwipeAdapter;
    private ProgressDialog progressDialog;
    private Proposal proposal;

    private Handler handler;
    private Runnable runnable;
    private SwipeRefreshLayout swipeContainer;
    private TextView fromWhereTopTextView;
    private TextView whereToTopTextView;
    private ListView listViewPassProposals;
    private Button refuseButton, callButton;
    private String id_d;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.passenger_list_of_proposals, container, false);
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        initUI(view);
        driversArrayList = new ArrayList<Driver>();
        driversArrayList = proposal.getItemOfferDrivers();
        fromWhereTopTextView.setText(proposal.getDeparture());
        additionalRoutesArrayList = proposal.getAdditionalRoutes();
        whereToTopTextView.setText(additionalRoutesArrayList.get(additionalRoutesArrayList.size() - 1).getWayaddress());


        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setTimeRefreshForList();
                getListOffers();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        refuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refuseProposal();
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "+" + id_d, null));
                startActivity(intent);
            }
        });

        listViewPassProposals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int itemNumber, long l) {
                Driver item = driversArrayList.get(itemNumber);

                if (!item.getStatus_d().equals(Constants.PASSENGER_DRIVER_ACCEPT_ORDER)
                        && !swipeContainer.isRefreshing()) {
                    DetailsProposal detailsProposal = new DetailsProposal();
                    detailsProposal.setButtonsListener(ListOfDriverProposalsFragment.this);
                    detailsProposal.setDetailsOffer(proposal);
                    detailsProposal.setDetailsDriver(item);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragments_container, detailsProposal, Constants.PASSENGER_DETAILE_OF_PROPOSALS)
                            .addToBackStack(Constants.PASSENGER_DETAILE_OF_PROPOSALS)
                            .commit();
                }
            }
        });

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (isNeedRefreshListOffers) {
                    setTimeRefreshForList();
                    getListOffers();
                    isNeedRefreshListOffers = false;
                }
            }
        });

        return view;
    }

    private void refuseProposal() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (isVisible()) {
                    hideProgressDialog();
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ListOfOwnOrders.setIsNeedRefreshOrders(true);
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                if (isVisible()) {
                    hideProgressDialog();

                    if (errorCode != Constants.NO_NETWORK_CONNECTION) {
                        Utils.showToast(getActivity(), "" + errorCode);
                        backForAuthorization(errorCode);
                    }
                }
            }
        });
        passengerRequester.cancelOrder_p(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                proposal.getOrder_uid());
        showProgressDialog();
    }

    private void initUI(View view) {
        fromWhereTopTextView = (TextView) view.findViewById(R.id.textViewFromWhereTop);
        whereToTopTextView = (TextView) view.findViewById(R.id.textViewWhereToTop);
        listViewPassProposals = (ListView) view.findViewById(R.id.listViewPassProposals);
        refuseButton = (Button) view.findViewById(R.id.buttonRefuse);
        callButton = (Button) view.findViewById(R.id.buttonCall);
    }

    private void setUpAdapter() {
        listViewSwipeAdapter = new ListViewSwipeAdapter(getActivity(), driversArrayList);
        listViewPassProposals.setAdapter(listViewSwipeAdapter);
        listViewSwipeAdapter.setMode(Attributes.Mode.Single);
        listViewSwipeAdapter.setButtonsListener(this);
    }

    public static void setIsNeedRefresh(boolean b) {
        isNeedRefreshListOffers = b;
    }

    private void setTimeRefreshForList() {
        final int delay = 60000;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    getListOffers();
                    handler.postDelayed(this, delay);
                }
            }
        };
        handler.postDelayed(runnable, delay);
    }

    public void getListOffers() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (isVisible()) {
                    swipeContainer.setRefreshing(false);

                    if (!str.equals(Constants.OK)) {
                        try {
                            JSONArray jsonArray = new JSONArray(str);

                            if (jsonArray.length() != 0) {
                                for (int n = 0; n < jsonArray.length(); n++) {
                                    JSONObject object = jsonArray.getJSONObject(n);
                                    Proposal proposal = new Proposal(object);

                                    if (isVisible() && ListOfDriverProposalsFragment.this.proposal.getOrder_uid().equals(proposal.getOrder_uid())) {
                                        for (int i = 0; i < proposal.getItemOfferDrivers().size(); i++) {
                                            if (proposal.getItemOfferDrivers().get(i).getStatus_d().equals(Constants.PASSENGER_DRIVER_ACCEPT_ORDER)) {
                                                id_d = proposal.getItemOfferDrivers().get(i).getId_d();
                                                callButton.setVisibility(View.VISIBLE);
                                            }
                                        }
                                        driversArrayList.clear();
                                        driversArrayList = proposal.getItemOfferDrivers();
                                        setUpAdapter();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                if (isVisible() && (((PassengerFragmentActivity) getActivity()).getTabPosition() == 1)) {
                    swipeContainer.setRefreshing(false);

                    if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
                        MainFragmentActivity.isAccessTokenGone(true);
                        Utils.showToast(getActivity(), getResources().getString(R.string.not_find_user_with_access_token));
                        getActivity().finish();

                    } else if (errorCode == Constants.NO_NEW_ORDERS) {
                        driversArrayList.clear();

                        if (listViewSwipeAdapter != null)
                            setUpAdapter();

                    } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.not_all_fields));

                    } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.json_error));

                    } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
                        Utils.showToast(getActivity(), getResources().getString(R.string.no_network));
                    }
                }
            }
        });
        swipeContainer.setRefreshing(true);
        passengerRequester.getListOfProposals(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));
    }

    private void backForAuthorization(int errorCode) {
        if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            MainFragmentActivity.isAccessTokenGone(true);
            getActivity().finish();
        }
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void setOffersArray(Proposal arrayOffer) {
        this.proposal = arrayOffer;
    }

    public void checkSetOfferErrorCode(int errorCode) {
        if (errorCode == Constants.FAILED_TO_WRITE_IN_DB) {
            Utils.showToast(getActivity(), getResources().getString(R.string.failed_to_write_in_db));

        } else if (errorCode == Constants.NO_REQUEST_IN_DB) {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_request_in_db));

        } else if (errorCode == Constants.NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getResources().getString(R.string.not_all_fields));

        } else if (errorCode == Constants.ERROR_JSON_FORMAT) {
            Utils.showToast(getActivity(), getResources().getString(R.string.json_error));

        } else if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            Utils.showToast(getActivity(), getResources().getString(R.string.not_find_user_with_access_token));
            MainFragmentActivity.isAccessTokenGone(true);
            getActivity().finish();

        } else if (errorCode == Constants.CASH_EQUALS_ZERO) {
            Utils.showToast(getActivity(), getResources().getString(R.string.cash_equals_zero));

        } else if (errorCode == Constants.NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), getResources().getString(R.string.no_network));

        } else if (errorCode == Constants.FAILED_INCORRECT_VALUE_STATUS_P) {
            Utils.showToast(getActivity(), getResources().getString(R.string.incorrect_value_status_p));

        } else {
            Utils.showToast(getActivity(), "Code: " + errorCode);
        }
    }

    @Override
    public void onAcceptClickButton(int position) {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                hideProgressDialog();
                Utils.showToast(getActivity(), getString(R.string.driver_notified));
                getListOffers();
            }

            @Override
            public void onMessageError(int errorCode) {
                hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });

        passengerRequester.changeOffer(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                proposal.getOrder_uid(), driversArrayList.get(position).getUid_d(),
                driversArrayList.get(position).getCash_d(), Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL);
        showProgressDialog();
    }

    @Override
    public void onRejectClickButton(final int position) {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                hideProgressDialog();
                setTimeRefreshForList();
                getListOffers();
            }

            @Override
            public void onMessageError(int errorCode) {
                hideProgressDialog();
                checkSetOfferErrorCode(errorCode);
            }
        });

        passengerRequester.changeOffer(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                proposal.getOrder_uid(), driversArrayList.get(position).getUid_d(),
                driversArrayList.get(position).getCash_d(), Constants.PASSENGER_REJECT_ORDER);
        showProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible() && fragmentManager.findFragmentById(R.id.fragments_container)
                instanceof ListOfDriverProposalsFragment) {
            getListOffers();
            setTimeRefreshForList();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (handler != null && runnable != null)
            handler.removeCallbacks(runnable);
    }
}