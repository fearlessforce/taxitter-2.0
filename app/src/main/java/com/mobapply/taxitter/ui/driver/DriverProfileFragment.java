package com.mobapply.taxitter.ui.driver;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.City;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.DriverRequester;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class DriverProfileFragment extends Fragment implements View.OnKeyListener {
    private final int DISTANCE_UP_TO_FIVE = 5;
    private final int DISTANCE_UP_TO_TEN = 10;
    private final int DISTANCE_UP_TO_TWENTY = 20;
    private final int DISTANCE_NO_LIMIT = 0;

    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences, sharedPref;
    private CircularProgressBar circularProgressBar;
    private Spinner distanceSpinner, citiesSpinner;
    private Uri selectedImageURI;
    private ImageLoader imageLoader;
    private List<String> listCities;
    private ArrayList<City> listCityArray;
    private ArrayAdapter<String> citiesArrayAdapter;

    private EditText brandEditText;
    private EditText modelEditText;
    private EditText typeEditText;
    private EditText yearOfManufactureEditText;
    private EditText colorEditText;
    private EditText stateNumberEditText;
    private EditText nicknameEditText;
    private Button minusSeatsButton, plusSeatsButton;
    private Button saveButton;
    private Button changeUserButton;
    private ToggleButton babySeatToggleButton, conditionerToggleButton, notSmokerToggleButton;
    private ImageView userPhotoImageButton;
    private TextView ratingTextView;
    private TextView quantitySeetsTextView;
    private TextView countSeatsTextView;
    private CheckBox pushNewOrderCheckBox;

    private String cookie;
    private String nickname;
    private String caryear_d, car_d, carmodel_d, cartype_d, carcolor_d, carnumber_d, quantity_d, maxradius_d;
    private String stringBase64Original;
    private String area_and_city;
    private String minCashe;
    private int babyseat_d, nosmoking_d, ac_d;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.driver_profile_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getActivity().getSharedPreferences(getString(R.string.pref_role_key), 0);
        minCashe = sharedPreferences.getString(Constants.MINCASHE, "");

        fragmentManager = getFragmentManager();
        imageLoader = ImageLoader.getInstance();
        listCityArray = new ArrayList<City>();

        setupListener(view);
        initUI(view);
        setListeners();

        if (sharedPreferences.getBoolean(Constants.NEW_ORDER_PUSH, false)) {
            pushNewOrderCheckBox.setChecked(true);
        } else {
            pushNewOrderCheckBox.setChecked(false);
        }

        getCities();
        getPrefData();
        listCities = new ArrayList<String>();
        City city = new City();

        if (area_and_city.equals("null")) {
            listCities.add(0, getString(R.string.choose_city));
            city.setNameCity(getString(R.string.choose_city));
            listCityArray.add(0, city);
        } else {
            listCities.add(0, area_and_city);
            city.setNameCity(area_and_city);
            listCityArray.add(0, city);
        }

        citiesArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, listCities);
        citiesArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        citiesSpinner.setAdapter(citiesArrayAdapter);
        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        setDataIntoUI();
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);

        nicknameEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nicknameEditText.setSelection(nicknameEditText.length());
            }
        });

        minusSeatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCountSeats(false);
                save();
            }
        });

        plusSeatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeCountSeats(true);
                save();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!citiesSpinner.getSelectedItem().toString().equals(getString(R.string.choose_city))) {
                    if (!yearOfManufactureEditText.getText().toString().equals("")) {
                        final DriverRequester driverRequester = new DriverRequester(getActivity());
                        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                            @Override
                            public void onMessageSuccess(String str) {
                                babyseat_d = babySeatToggleButton.isChecked() ? 1 : 0;
                                ac_d = conditionerToggleButton.isChecked() ? 1 : 0;
                                nosmoking_d = notSmokerToggleButton.isChecked() ? 1 : 0;

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Constants.QUANTITY_D, quantity_d);
                                editor.putInt(Constants.BABYSEAT_D, babySeatToggleButton.isChecked() ? 1 : 0);
                                editor.putInt(Constants.AC_D, conditionerToggleButton.isChecked() ? 1 : 0);
                                editor.putInt(Constants.NOSMOKING_D, notSmokerToggleButton.isChecked() ? 1 : 0);
                                editor.apply();

                                hideCircularBar();
                                int count = fragmentManager.getBackStackEntryCount();
                                Fragment fr = fragmentManager.findFragmentById(R.id.menu_container);

                                if (fr instanceof DriverProfileFragment) {
                                    for (int i = 0; i < count; i++) {
                                        String name = fragmentManager.getBackStackEntryAt(i).getName();

                                        if (name.equals(Constants.DRIVER_DETAILE_OF_OFFER_TAG)) {
                                            boolean hasCurrentOrder = false;

                                            for (int c = 0; c < count; c++) {
                                                name = fragmentManager.getBackStackEntryAt(c).getName();

                                                if (name.equals(Constants.CURRENT_ORDER))
                                                    hasCurrentOrder = true;
                                            }

                                            if (hasCurrentOrder)
                                                fragmentManager.popBackStack(Constants.CURRENT_ORDER, 0);
                                            else
                                                fragmentManager.popBackStack(Constants.DRIVER_DETAILE_OF_OFFER_TAG, 0);

                                            break;

                                        } else if (name.equals(Constants.DRIVER_DETAILE_OF_ORDER_TAG)) {
                                            fragmentManager.popBackStack(Constants.DRIVER_DETAILE_OF_ORDER_TAG, 0);
                                            break;

                                        } else if (name.equals(Constants.CURRENT_ORDER)) {
                                            fragmentManager.popBackStack(Constants.CURRENT_ORDER, 0);
                                            break;

                                        } else {
                                            fragmentManager.popBackStack();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onMessageError(int errorCode) {
                                hideCircularBar();
                                Utils.showToast(getActivity(), DriverRequester.otherErrors(getActivity(), errorCode));
                            }
                        });

                        getDataFromField();
                        driverRequester.saveProfile(cookie, nickname, car_d, carmodel_d, cartype_d, carcolor_d, caryear_d, carnumber_d,
                                babySeatToggleButton.isChecked() ? 1 : 0, conditionerToggleButton.isChecked() ? 1 : 0,
                                notSmokerToggleButton.isChecked() ? 1 : 0, quantity_d, maxradius_d, area_and_city);
                        showCircularBar();

                    } else {
                        Utils.showAlertDialog(getActivity(), getString(R.string.year_field_empty));
                    }
                } else {
                    Utils.showAlertDialog(getActivity(), getString(R.string.choose_city_profile));
                }
            }
        });

        userPhotoImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View dialog_layout = inflater.inflate(R.layout.choose_foto_dialog, (ViewGroup) view1.findViewById(R.id.root));
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialog_layout).setCancelable(true);
                final AlertDialog alert = builder.create();
                alert.show();

                ((Button)dialog_layout.findViewById(R.id.buttonCamera)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
                    }
                });

                ((Button)dialog_layout.findViewById(R.id.buttonGallery)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.PICK_FROM_GALLERY);
                    }
                });
            }
        });

        changeUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangeRoleDialog();
            }
        });

        pushNewOrderCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked)
                    sharedPreferences.edit().putBoolean(Constants.NEW_ORDER_PUSH, true).apply();
                else
                    sharedPreferences.edit().putBoolean(Constants.NEW_ORDER_PUSH, false).apply();
            }
        });

        return view;
    }

    private void save(){
        final DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                babyseat_d = babySeatToggleButton.isChecked() ? 1 : 0;
                ac_d = conditionerToggleButton.isChecked() ? 1 : 0;
                nosmoking_d = notSmokerToggleButton.isChecked() ? 1 : 0;

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constants.QUANTITY_D, quantity_d);
                editor.putInt(Constants.BABYSEAT_D, babySeatToggleButton.isChecked() ? 1 : 0);
                editor.putInt(Constants.AC_D, conditionerToggleButton.isChecked() ? 1 : 0);
                editor.putInt(Constants.NOSMOKING_D, notSmokerToggleButton.isChecked() ? 1 : 0);
                editor.apply();

                hideCircularBar();

            }

            @Override
            public void onMessageError(int errorCode) {
                hideCircularBar();
                Utils.showToast(getActivity(), DriverRequester.otherErrors(getActivity(), errorCode));
            }
        });

        getDataFromField();
        driverRequester.saveProfile(cookie, nickname, car_d, carmodel_d, cartype_d, carcolor_d, caryear_d, carnumber_d,
                babySeatToggleButton.isChecked() ? 1 : 0, conditionerToggleButton.isChecked() ? 1 : 0,
                notSmokerToggleButton.isChecked() ? 1 : 0, quantity_d, maxradius_d, area_and_city);
        showCircularBar();

    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, userPhotoImageButton, size);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });
        commonRequest.getOwnAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size);
    }

    private void showChangeRoleDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(getString(R.string.exit_change_role));
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                changeUser();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void changeUser() {
        final DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });

        driverRequester.changeRole(cookie, Constants.DRIVER_ROLE_STATUS);
        sharedPref.edit().putString(Constants.ROLE_SATE, Constants.DRIVER_ROLE).apply();
        MainFragmentActivity.isLogOff(true);
        getActivity().finish();
    }

    private void changeCountSeats(boolean isPlus) {
        String f = countSeatsTextView.getText().toString().substring(0, countSeatsTextView.getText().toString().indexOf(" "));
        int count = Integer.parseInt(f);

        if (isPlus) {
            count++;
        } else {
            count--;
        }

        if (count >= 1) {
            quantitySeetsTextView.setText("" + count);
            quantity_d = String.valueOf(count);
            countSeatsTextView.setText(count + " " + getString(R.string.count_seats));
        }
    }

    private void getCities() {
        DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject.length() != 0) {
                            JSONArray jsonArray = jsonObject.getJSONArray("cities");
                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsObject = jsonArray.getJSONObject(i);
                                City city = new City(jsObject);
                                listCityArray.add(city);

                                if (listCities.size() != 0 && !city.getCityAddress().equals(listCities.get(0)))
                                    listCities.add(city.getCityAddress());
                            }
                            citiesArrayAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Log.d("ta", "e" + errorCode);
            }
        });

        driverRequester.getCities(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Constants.PICK_FROM_CAMERA) {
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + "img.jpg");
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
            sendPhotoToServer(bitmap, Constants.IMG_LARGE_SIZE);

            try {
                cropCapturedImage(Uri.fromFile(file));

            } catch(ActivityNotFoundException aNFE) {
                Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");
            }

        } else if (requestCode == Constants.PIC_CROP && data != null) {
            Bundle extras = data.getExtras();
            Bitmap bitmap = null;
            if (extras == null){
                Uri imageUri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                bitmap = extras.getParcelable("data");
            }

            userPhotoImageButton.setImageBitmap(Utils.getRoundedCornerBitmap(bitmap,
                    Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, getActivity())));
            sendPhotoToServer(bitmap, Constants.IMG_SMALL_SIZE);

        } else if (requestCode == Constants.PICK_FROM_GALLERY && data != null) {

            try {
                selectedImageURI = data.getData();
                File file = new File(Utils.getRealPathFromURI(getActivity(), selectedImageURI));
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageURI);
                sendPhotoToServer(bitmap, Constants.IMG_LARGE_SIZE);
                cropCapturedImage(Uri.fromFile(file));

            } catch(ActivityNotFoundException aNFE) {
                Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");

            } catch (Exception e) {
                Utils.showAlertDialog(getActivity(), "Неподдерживаемый тип документа. Выберите изображение, пожалуйста.");
                e.printStackTrace();
            }
        }
    }

    private void sendPhotoToServer(final Bitmap bitmap, final String type) {
        if (bitmap == null) return;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        bitmap.recycle();
        stringBase64Original = android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT);

        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageLoader.clearCache();
                    }
                }, 10000);
                imageLoader.clearCache();
            }

            @Override
            public void onMessageError(int errorCode) {
                Log.d("TAG", "" + errorCode);
            }
        });
        commonRequest.setAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), type, stringBase64Original);
    }

    public void cropCapturedImage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 100);
        cropIntent.putExtra("outputY", 100);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, Constants.PIC_CROP);
    }

    private void hideCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).progressiveStop();
        circularProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).start();
        circularProgressBar.setVisibility(View.VISIBLE);
    }

    private void setDataIntoUI() {
        nicknameEditText.setText(nickname);
        brandEditText.setText(car_d);
        modelEditText.setText(carmodel_d);
        typeEditText.setText(cartype_d);
        colorEditText.setText(carcolor_d);
        yearOfManufactureEditText.setText(caryear_d.length() == 0 || caryear_d.equals("0") ? "" : caryear_d);
        stateNumberEditText.setText(carnumber_d);
        quantitySeetsTextView.setText(quantity_d);
        countSeatsTextView.setText(quantity_d + " " + getString(R.string.count_seats));

        notSmokerToggleButton.setChecked(nosmoking_d == 1);
        babySeatToggleButton.setChecked(babyseat_d == 1);
        conditionerToggleButton.setChecked(ac_d == 1);

        if (!maxradius_d.equals("")) {
            if (Integer.parseInt(maxradius_d) == DISTANCE_NO_LIMIT) {
                distanceSpinner.setSelection(3);

            } else if (Integer.parseInt(maxradius_d) == DISTANCE_UP_TO_FIVE) {
                distanceSpinner.setSelection(0);

            } else if (Integer.parseInt(maxradius_d) == DISTANCE_UP_TO_TEN) {
                distanceSpinner.setSelection(1);

            } else if (Integer.parseInt(maxradius_d) == DISTANCE_UP_TO_TWENTY) {
                distanceSpinner.setSelection(2);
            }

        } else {
            distanceSpinner.setSelection(3);
        }

        if (area_and_city.equals("null"))
            citiesSpinner.setSelection(0);
    }

    private void getDataFromField() {
        nickname = nicknameEditText.getText().toString();
        car_d = brandEditText.getText().toString();
        carmodel_d = modelEditText.getText().toString();
        cartype_d = typeEditText.getText().toString();
        carcolor_d = colorEditText.getText().toString();
        area_and_city = citiesSpinner.getSelectedItem().toString();
        minCashe = listCityArray.get(citiesSpinner.getSelectedItemPosition()).getMincash();

        if (minCashe != null)
            sharedPreferences.edit().putString(Constants.MINCASHE, minCashe).apply();

        if (!yearOfManufactureEditText.getText().toString().equals(getString(R.string.year_of_manufacture)) &&
                !yearOfManufactureEditText.getText().toString().equals("")) {
            caryear_d = yearOfManufactureEditText.getText().toString();

        } else {
            caryear_d = "0";
        }

        carnumber_d = stateNumberEditText.getText().toString();
        babyseat_d = babySeatToggleButton.isChecked() ? 1 : 0;
        ac_d = conditionerToggleButton.isChecked() ? 1 : 0;
        nosmoking_d = notSmokerToggleButton.isChecked() ? 1 : 0;

        if (distanceSpinner.getSelectedItemPosition() == 0) {
            maxradius_d = "" + DISTANCE_UP_TO_FIVE;

        } else if (distanceSpinner.getSelectedItemPosition() == 1) {
            maxradius_d = "" + DISTANCE_UP_TO_TEN;

        } else if (distanceSpinner.getSelectedItemPosition() == 2) {
            maxradius_d = "" + DISTANCE_UP_TO_TWENTY;

        } else if (distanceSpinner.getSelectedItemPosition() == 3) {
            maxradius_d = "" + DISTANCE_NO_LIMIT;
        }
    }

    private void getPrefData() {
        cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
        nickname = sharedPreferences.getString(Constants.NICKNAME, "");
        car_d = sharedPreferences.getString(Constants.CAR_D, "");
        carmodel_d = sharedPreferences.getString(Constants.CARMODEL_D, "");
        cartype_d = sharedPreferences.getString(Constants.CARTYPE_D, "");
        carcolor_d = sharedPreferences.getString(Constants.CARCOLOR_D, "");
        caryear_d = sharedPreferences.getString(Constants.CARYEAR_D, "");
        carnumber_d = sharedPreferences.getString(Constants.CARNUMBER_D, "");
        babyseat_d = sharedPreferences.getInt(Constants.BABYSEAT_D, 0);
        ac_d = sharedPreferences.getInt(Constants.AC_D, 0);
        nosmoking_d = sharedPreferences.getInt(Constants.NOSMOKING_D, 0);
        quantity_d = sharedPreferences.getString(Constants.QUANTITY_D, "0");
        area_and_city = sharedPreferences.getString(Constants.AREA_AND_CITY, "null");
        maxradius_d = sharedPreferences.getString(Constants.MAXRADIUS_D, "");

        assert maxradius_d != null;
        if (maxradius_d.equals(""))
            maxradius_d = "0";

        if (quantity_d.length() == 0)
            quantity_d = "0";
    }

    private void initUI(View view) {
        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.smoothCircleBar);
        pushNewOrderCheckBox = (CheckBox)view.findViewById(R.id.checkBoxNotificationNewOrder);
        changeUserButton = (Button)view.findViewById(R.id.buttonChangeUser);
        distanceSpinner = (Spinner)view.findViewById(R.id.spinnerDistance);
        citiesSpinner = (Spinner)view.findViewById(R.id.spinnerCities);
        ratingTextView = (TextView)view.findViewById(R.id.textViewRatingProfile);
        quantitySeetsTextView = (TextView)view.findViewById(R.id.textViewQuantitySeets);
        brandEditText = (EditText)view.findViewById(R.id.buttonBrand);
        modelEditText = (EditText)view.findViewById(R.id.buttonModel);
        typeEditText = (EditText)view.findViewById(R.id.buttonType);
        yearOfManufactureEditText = (EditText)view.findViewById(R.id.buttonYearOfManufacture);
        colorEditText = (EditText)view.findViewById(R.id.buttonColor);
        stateNumberEditText = (EditText)view.findViewById(R.id.buttonStateNumber);
        minusSeatsButton = (Button)view.findViewById(R.id.buttonMinusSeatsSettings);
        plusSeatsButton = (Button)view.findViewById(R.id.buttonPlusSeatsSettings);
        babySeatToggleButton = (ToggleButton)view.findViewById(R.id.buttonBabySeatSettings);
        conditionerToggleButton = (ToggleButton)view.findViewById(R.id.buttonConditionerSettings);
        notSmokerToggleButton = (ToggleButton)view.findViewById(R.id.buttonNotSmokerSettings);
        saveButton = (Button)view.findViewById(R.id.buttonSaveSettings);
        nicknameEditText = (EditText)view.findViewById(R.id.editTextNicknameProfile);
        countSeatsTextView = (TextView)view.findViewById(R.id.textViewCountSeatsProfile);
        userPhotoImageButton = (ImageView)view.findViewById(R.id.driverProfileImageButton);

        saveButton.setVisibility(View.GONE);

        babySeatToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        conditionerToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        notSmokerToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    private void setListeners() {
        nicknameEditText.setOnKeyListener(this);
        brandEditText.setOnKeyListener(this);
        modelEditText.setOnKeyListener(this);
        typeEditText.setOnKeyListener(this);
        yearOfManufactureEditText.setOnKeyListener(this);
        colorEditText.setOnKeyListener(this);
        stateNumberEditText.setOnKeyListener(this);
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
            return true;

        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            Utils.hideKeyboard(getActivity());
            view.clearFocus();
            save();
            return true;

        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            Utils.hideKeyboard(getActivity());
            view.clearFocus();
            return true;
        }

        return false;
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }
}