package com.mobapply.taxitter.ui.passenger;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.webconnect.DriverRequester;

public class PassengerSettingFragment extends Fragment{
    private Button exitButton;
    private Button eraseHistoryButton;
    private Button changePasswordButton;
    private SharedPreferences sharedPreferences, sharedPref;
    private String cookie;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getActivity().getSharedPreferences(getString(R.string.pref_role_key), 0);
        cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");

        exitButton = (Button)view.findViewById(R.id.buttonExit);
        eraseHistoryButton = (Button)view.findViewById(R.id.buttonEraseHistory);
        changePasswordButton = (Button)view.findViewById(R.id.buttonChangePassword);

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DriverRequester driverRequester = new DriverRequester(getActivity());
                driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                    }
                });
                driverRequester.changeRole(cookie, Constants.PASSENGER_ROLE);

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(Constants.ROLE_SATE, Constants.PASSENGER_ROLE);
                editor.apply();

                if (PassengerFragmentActivity.class == getActivity().getClass()) {
                    MainFragmentActivity.isLogOff(true);
                    getActivity().finish();

                } else {
                    PassengerFragmentActivity.isLogOff(true);
                    getActivity().finish();
                }
            }
        });

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        eraseHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return view;
    }
}