package com.mobapply.taxitter.ui.passenger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.custom.CustomDateTimePicker;
import com.mobapply.taxitter.custom.TimePeriod;
import com.mobapply.taxitter.model.Address;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OrderTaxiFragment extends Fragment implements View.OnClickListener {
    private final int CONFIRM_REQUEST = 2;
    private final int ORDER_WITH_MAP_REQUEST = 1;
    public static int WHERE_COUNTER = 0;
    private static boolean isAccessTokenGone = false;
    private static boolean isLogOff = false;
    private static boolean isOrderAccept = false;
    private static boolean isChangeRole = false;

    private SharedPreferences sharedPreferences;
    private View view;
    private Handler repeatUpdateHandler = new Handler();
    private Intent intentForConfirmation;
    private List<View> listDotsView;
    private ArrayList<Address> addressArrayList;
    private CustomDateTimePicker customDateTimePicker;

    private LinearLayout linearDots, linearTime;
    private RelativeLayout relConditionOne, relConditionTwo, relConditionThree;
    private Button fromWhereButton, whereToButton;
    private Button whereDotButton;
    private Button selectedViewlButton;
    private Button addViewButton;
    private Button commentButton;
    private ImageButton minusSeatsImgButton, plusSeatsImgButton;
    private ImageView airConditionImgButton, noSmokingImgButton, babySeatImgButton;
    private ImageButton orderButton;
    private TextView counterTextView;
    private TextView timeTextView;
    private TextView setTimeTextView;
    private ImageButton clearTimeButton;
    private TextView conditionOneTextView, conditionTwoTextView, conditionThreeTextView;
    private boolean showingBabyseatFirst = true;
    private boolean showingConditionFirst = true;
    private boolean showingNoSmoningFirst = true;
    private String fromWhere, whereTo;
    private long timeFromPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.passenger_order_taxi_fragment, container, false);
        addressArrayList = new ArrayList<Address>();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);

        initUI();

        listDotsView = new ArrayList<View>();
        fromWhereButton.setOnClickListener(this);
        whereToButton.setOnClickListener(this);
        customDateTimePicker.set24HourFormat(true);
        customDateTimePicker.setDate(Calendar.getInstance());
        customDateTimePicker.setConstraints(new TimePeriod(Calendar.getInstance(), null));

        linearTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                customDateTimePicker.showDialog();
            }
        });

        minusSeatsImgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String f = counterTextView.getText().toString().substring(0, counterTextView.getText().toString().indexOf(" "));
                int count = Integer.parseInt(f);
                count--;

                if (count >= 1)
                    counterTextView.setText(count + " " + getString(R.string.passengers));
            }
        });

        plusSeatsImgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String f = counterTextView.getText().toString().substring(0, counterTextView.getText().toString().indexOf(" "));
                int count = Integer.parseInt(f);
                count++;

                if (count >= 1)
                    counterTextView.setText(count + " " + getString(R.string.passengers));
            }
        });

        /**
         * Add a new field for the new route, and add to the array
         */
        linearDots = (LinearLayout) view.findViewById(R.id.linear);
        addViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!whereToButton.getText().equals(getString(R.string.where_to)) && !fromWhereButton.getText().equals(getString(R.string.from_where))) {
                    final View view = getActivity().getLayoutInflater().inflate(R.layout.passenger_order_dot_edittext_layout, null);
                    ImageButton deleteField = (ImageButton) view.findViewById(R.id.deleteDotImageButton);
                    whereDotButton = (Button) view.findViewById(R.id.whereDotButton);
                    whereDotButton.setTag(WHERE_COUNTER);

                    if (listDotsView.size() >= 1) {
                        RelativeLayout rel = (RelativeLayout) listDotsView.get(listDotsView.size() - 1);
                        String title = ((Button) rel.findViewById(R.id.whereDotButton)).getText().toString();

                        if (!title.equals(getString(R.string.where_to))) {
                            whereDotButton.setOnClickListener(OrderTaxiFragment.this);
                            listDotsView.add(view);

                            deleteField.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        for (int i = 0; i < listDotsView.size(); i++) {

                                            if (view == listDotsView.get(i)) {
                                                ((LinearLayout) view.getParent()).removeView(view);
                                                listDotsView.remove(view);

                                                if (addressArrayList.size() >= i + 2)
                                                    addressArrayList.remove(i + 2);
                                            }
                                        }

                                    } catch (IndexOutOfBoundsException ex) {
                                        ex.printStackTrace();
                                    }
                                    WHERE_COUNTER--;
                                }
                            });

                            linearDots.addView(view);
                            WHERE_COUNTER++;
                        }
                    } else {
                        whereDotButton.setOnClickListener(OrderTaxiFragment.this);
                        listDotsView.add(view);

                        deleteField.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    for (int i = 0; i < listDotsView.size(); i++) {

                                        if (view == listDotsView.get(i)) {
                                            ((LinearLayout) view.getParent()).removeView(view);
                                            listDotsView.remove(view);

                                            if (addressArrayList.size() >= i + 2)
                                                addressArrayList.remove(i + 2);
                                        }
                                    }
                                } catch (IndexOutOfBoundsException ex) {
                                    ex.printStackTrace();
                                }
                                WHERE_COUNTER--;
                            }
                        });

                        linearDots.addView(view);
                        WHERE_COUNTER++;
                    }
                }
            }
        });

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View dialog_layout = inflater.inflate(R.layout.dialog_write_letter,(ViewGroup) view.findViewById(R.id.root));
                final EditText editTextLetter = (EditText)dialog_layout.findViewById(R.id.editTextLetter);

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(R.string.write_letter);
                builder.setView(dialog_layout)
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Utils.hideKeyboard(getActivity());
                                    }
                                }, 300);

                                String currentText = editTextLetter.getText().toString();
                                commentButton.setText(currentText.length() != 0 ? editTextLetter.getText().toString()
                                        : getString(R.string.comment_for_order));

                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Utils.hideKeyboard(getActivity());
                                    }
                                }, 300);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (!getString(R.string.textvie_now).equals(timeTextView.getText().toString())) {

                    if (checkTime()) {
                        saveOrder();

                    } else {
                        Utils.showAlertDialog(getActivity(), getString(R.string.wrong_time));
                    }

                } else {
                    saveOrder();
                }
            }
        });
        return view;
    }

    private void saveOrder() {
        String title = "";

        if (listDotsView.size() > 0) {
            RelativeLayout rel = (RelativeLayout) listDotsView.get(listDotsView.size() - 1);
            title = ((Button) rel.findViewById(R.id.whereDotButton)).getText().toString();
        } else {
            title = "";
        }

        if (!fromWhereButton.getText().toString().equals(getString(R.string.from_where))
                && !whereToButton.getText().toString().equals(getString(R.string.where_to))
                && !title.equals(getString(R.string.where_to))) {
            intentForConfirmation = new Intent(getActivity(), ConfirmationActivity.class);
            intentForConfirmation.putParcelableArrayListExtra(Constants.ADDRESS_ARRAY, addressArrayList);

            intentForConfirmation.putExtra(Constants.BABY_SEAT, !showingBabyseatFirst);
            intentForConfirmation.putExtra(Constants.NO_SMOKING, !showingNoSmoningFirst);
            intentForConfirmation.putExtra(Constants.AIR_CONDITION, !showingConditionFirst);
            intentForConfirmation.putExtra(Constants.TIME, timeTextView.getText().toString());
            intentForConfirmation.putExtra(Constants.PASSENGERS, counterTextView.getText().toString()
                    .substring(0, counterTextView.getText().toString().indexOf(" ")));
            intentForConfirmation.putExtra(Constants.COMMENT, commentButton.getText().toString()
                    .equals(getString(R.string.comment_for_order)) ? "" : commentButton.getText().toString());
            intentForConfirmation.putExtra(Constants.FROM_WHERE, fromWhere);
            intentForConfirmation.putExtra(Constants.WHERE_TO, whereTo);
            startActivityForResult(intentForConfirmation, CONFIRM_REQUEST);

        } else {
            Utils.showAlertDialog(getActivity(), getString(R.string.not_all_fields));
        }
    }

    private boolean checkTime() {
        long time= System.currentTimeMillis();
        return time < timeFromPicker;
    }

    @Override
    public void onClick(View v) {
        selectedViewlButton = (Button) v;
        Intent intent = new Intent(getActivity(), OrderWithMapActivity.class);
        intent.putExtra(Constants.WHERE_TO, true);

        Address address = new Address();

        if (selectedViewlButton.getTag().toString().equals(getString(R.string.tag_from_where))) {
            if (selectedViewlButton.getText().toString().equals(getString(R.string.from_where))) {
                address.setId(0);
                intent.putExtra(Constants.ADDRESS, address);

            } else if (addressArrayList.size() >= 1) {
                putEditAddres(0, intent);

            }

            intent.putExtra(Constants.WHERE_TO, false);

        } else if (selectedViewlButton.getTag().toString().equals(getString(R.string.tag_where_to))) {
            if (selectedViewlButton.getText().toString().equals(getString(R.string.where_to))) {
                address.setId(1);
                intent.putExtra(Constants.ADDRESS, address);

            } else if (((Button)view.findViewById(R.id.fromWhereButton)).getText().toString()
                    .equals(getString(R.string.from_where))) {
                putEditAddres(0, intent);

            } else {
                putEditAddres(1, intent);
            }

        } else if (listDotsView.size() != 0) {
            for (int i = 0; i < listDotsView.size(); i++) {
                View view = listDotsView.get(i);
                if (((Button)view.findViewById(R.id.whereDotButton)).getTag().toString()
                        .equals(selectedViewlButton.getTag().toString())){
                    if (addressArrayList.size() > i + 2) {
                        putEditAddres(i + 2, intent);
                    } else {
                        address.setId(i + 2);
                        intent.putExtra(Constants.ADDRESS, address);
                    }
                    break;
                }
            }
        }

        startActivityForResult(intent, ORDER_WITH_MAP_REQUEST);
    }

    private void putEditAddres(int i, Intent intent) {
        Address addr = addressArrayList.get(i);
        addr.setEdited(1);
        intent.putExtra(Constants.ADDRESS, addr);
    }

    /**
     * Write the full address with city name, for the to get the coordinates, but in textView set only street text.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (intent == null) { return; }

        if (requestCode == ORDER_WITH_MAP_REQUEST) {
            Address address = (Address) intent.getExtras().getParcelable(Constants.ADDRESS);
            String comment = intent.getStringExtra(Constants.COMMENT);
            selectedViewlButton.setText(address.getHalfAddress());

            if (!comment.equals(getString(R.string.comment_for_driver)))
                commentButton.setText(comment);

            if (selectedViewlButton == fromWhereButton) {
                fromWhere = address.getHalfAddress();

                if (address.isEdited() == 1) {
                    addressArrayList.set(address.getId(), address);

                } else {
                    addressArrayList.add(0, address);
                }
                return;

            } else if (selectedViewlButton == whereToButton) {
                whereTo = address.getHalfAddress();

                if (address.isEdited() == 1) {
                    addressArrayList.set(address.getId(), address);

                } else {
                    addressArrayList.add(address);
                }
                return;
            }

            if (address.isEdited() == 1) {
                addressArrayList.set(address.getId(), address);
            } else {
                addressArrayList.add(address);
            }
        } else if (requestCode == CONFIRM_REQUEST) {
            if (intent.hasExtra(Constants.PASSENGER_CONFIRM_ORDER_STATE)) {

                if (intent.getBooleanExtra(Constants.PASSENGER_CONFIRM_ORDER_STATE, false)) {
                    TabHost host = (TabHost) getActivity().findViewById(android.R.id.tabhost);
                    host.setCurrentTab(1);
                }
            }
        }
    }

    private void initUI() {
        linearTime = (LinearLayout) view.findViewById(R.id.linearTime);
        fromWhereButton = (Button) view.findViewById(R.id.fromWhereButton);
        whereToButton = (Button) view.findViewById(R.id.whereToButton);
        commentButton = (Button) view.findViewById(R.id.commentButton);
        minusSeatsImgButton = (ImageButton) view.findViewById(R.id.minusButton);
        plusSeatsImgButton = (ImageButton) view.findViewById(R.id.plusButton);
        airConditionImgButton = (ImageView) view.findViewById(R.id.airconditionBtn);
        noSmokingImgButton = (ImageView) view.findViewById(R.id.noSmokingBtn);
        babySeatImgButton = (ImageView) view.findViewById(R.id.babysetBtn);
        orderButton = (ImageButton) view.findViewById(R.id.orderButton);
        counterTextView = (TextView) view.findViewById(R.id.textViewCounter);
        timeTextView = (TextView) view.findViewById(R.id.timeView);
        setTimeTextView = (TextView) view.findViewById(R.id.setTime);
        clearTimeButton = (ImageButton) view.findViewById(R.id.clearTime);

        addViewButton = (Button) view.findViewById(R.id.addView);

        customDateTimePicker = new CustomDateTimePicker(getActivity(),
            new CustomDateTimePicker.ICustomDateTimeListener() {

                @Override
                public void onSet(Dialog dialog, int year, int month, int date, int hour24, int min, int sec) {
                    timeTextView.setText(date + "." + (month+1) + "." + year + ", " + hour24 + ":" + min);
                    Calendar c = Calendar.getInstance();
                    c.set(year, month, date, hour24, min, sec);
                    timeFromPicker = c.getTimeInMillis();

                    setTimeTextView.setVisibility(View.GONE);
                    clearTimeButton.setVisibility(View.VISIBLE);
                }

                @Override
                public void onNow(Dialog dialog) {
                    timeTextView.setText(R.string.now);
                    timeFromPicker = 0;
                }

                @Override
                public void onCancel() {
                    customDateTimePicker.dismissDialog();
                }
            });

        customDateTimePicker.set24HourFormat(false);
        customDateTimePicker.setDate(Calendar.getInstance());

        clearTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeTextView.setText(R.string.now);
                timeFromPicker = 0;
                setTimeTextView.setVisibility(View.VISIBLE);
                clearTimeButton.setVisibility(View.GONE);
            }
        });

        relConditionOne = (RelativeLayout)view.findViewById(R.id.relConditionOne);
        relConditionTwo = (RelativeLayout)view.findViewById(R.id.relConditionTwo);
        relConditionThree = (RelativeLayout)view.findViewById(R.id.relConditionThree);
        conditionOneTextView = (TextView)view.findViewById(R.id.conditionOneTextView);
        conditionTwoTextView = (TextView)view.findViewById(R.id.conditionTwoTextView);
        conditionThreeTextView = (TextView)view.findViewById(R.id.conditionThreeTextView);

        relConditionOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateForConditionOne();
            }
        });

        relConditionTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateForConditionTwo();
            }
        });

        relConditionThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateForConditionThree();
            }
        });

        conditionOneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateForConditionOne();
            }
        });

        conditionTwoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateForConditionTwo();
            }
        });

        conditionThreeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStateForConditionThree();
            }
        });

    }

    private void setStateForConditionThree() {
        if (showingConditionFirst) {
            airConditionImgButton.setImageResource(R.drawable.conditions_conditioner_pressed_details_order);
            showingConditionFirst = false;

        } else {
            airConditionImgButton.setImageResource(R.drawable.conditions_conditioner_details_order);
            showingConditionFirst = true;
        }
    }

    private void setStateForConditionTwo() {
        if (showingNoSmoningFirst) {
            noSmokingImgButton.setImageResource(R.drawable.conditions_smoker_pressed_details_order);
            showingNoSmoningFirst = false;

        } else {
            noSmokingImgButton.setImageResource(R.drawable.conditions_smoker_details_order);
            showingNoSmoningFirst = true;
        }
    }

    private void setStateForConditionOne() {
        if (showingBabyseatFirst) {
            babySeatImgButton.setImageResource(R.drawable.conditions_babyseat_pressed_details_order);
            showingBabyseatFirst = false;

        } else {
            babySeatImgButton.setImageResource(R.drawable.conditions_babyseat_details_order);
            showingBabyseatFirst = true;
        }
    }

    public static void isAccessTokenGone(boolean state) {
        isAccessTokenGone = state;
    }

    public static void isOrderAccept(boolean state) {
        isOrderAccept = state;
    }

    private void clearAllField() {
        fromWhereButton.setText(getString(R.string.from_where));
        whereToButton.setText(getString(R.string.where_to));
        linearDots.removeAllViews();
        counterTextView.setText("1 " + getString(R.string.passengers));
        timeTextView.setText(getString(R.string.textvie_now));

        babySeatImgButton.setImageResource(R.drawable.conditions_babyseat_details_order);
        showingBabyseatFirst = true;

        noSmokingImgButton.setImageResource(R.drawable.conditions_smoker_details_order);
        showingNoSmoningFirst = true;

        airConditionImgButton.setImageResource(R.drawable.conditions_conditioner_details_order);
        showingConditionFirst = true;
        commentButton.setText(getString(R.string.comment_for_order));
        addressArrayList.clear();

        listDotsView.clear();
        WHERE_COUNTER = 0;
    }

    /** If no accsess_token back to the screen authentication, if log off back to the first screen */
    @Override
    public void onResume() {
        super.onResume();

        if (isAccessTokenGone) {
            MainFragmentActivity.isAccessTokenGone(true);
            isAccessTokenGone = false;
            getActivity().finish();
        }

        if (isLogOff) {
            MainFragmentActivity.isLogOff(true);
            isLogOff = false;
            getActivity().finish();
        }

        if (isChangeRole){
            getActivity().finish();
            isChangeRole = false;
        }

        if (isOrderAccept) {
            clearAllField();
            ((ScrollView)view.findViewById(R.id.scrollView2)).fullScroll(View.FOCUS_UP);
            isOrderAccept = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WHERE_COUNTER = 0;
    }
}