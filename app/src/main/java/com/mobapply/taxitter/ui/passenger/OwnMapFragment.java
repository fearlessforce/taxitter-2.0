package com.mobapply.taxitter.ui.passenger;

import android.app.Fragment;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.utils.Utils;

/**
 * Created by Misha Nester on 30.03.2015.
 */
public class OwnMapFragment extends Fragment {
    private GoogleMap googleMap;
    private MapFragment fragment;
    private LatLng location;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.map_fragment, container, false);

        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initilizeMap();

        if (googleMap == null){
            return;
        }

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12));
        MarkerOptions marker = new MarkerOptions().position(location).title(getString(R.string.you_here));
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        googleMap.addMarker(marker);
        fragment.getView().setVisibility(View.VISIBLE);
    }

    /** Set map visible and initialize */
    private void initilizeMap() {
        if (googleMap == null) {
            fragment  =(MapFragment) getFragmentManager().findFragmentById(R.id.map);
            if(fragment  == null && android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN) {
                fragment  =(MapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            }
            if(fragment != null) {
                fragment.getView().setVisibility(View.GONE);
                googleMap = fragment.getMap();
                if (googleMap == null)
                    Utils.showToast(getActivity(), "Sorry! unable to create maps");
            }


        }
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }
}