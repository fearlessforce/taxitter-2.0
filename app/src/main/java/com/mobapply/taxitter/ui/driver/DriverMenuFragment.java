package com.mobapply.taxitter.ui.driver;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.custom.OnSwipeTouchListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

/**
 * Fragment that appears when there was a click of menuButton on actionBar
 */
public class DriverMenuFragment extends Fragment {
    public static final String DRIVER_PROFILE_FRAGMENT = "driver_profile_fragment";
    private SharedPreferences sharedPreferences;
    private ImageLoader imageLoader;
    private Uri selectedImageURI;
    private TextView userNameTextView;
    private ImageView userPhotoImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu, container, false);
        setupListener(view);
        imageLoader = ImageLoader.getInstance();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        userNameTextView = (TextView)view.findViewById(R.id.textViewMenuUserName);
        userPhotoImageView = (ImageView)view.findViewById(R.id.imageViewMenuUserPhoto);

        getAvatarFromServer(Constants.IMG_SMALL_SIZE);
        userNameTextView.setText(sharedPreferences.getString(Constants.NICKNAME, ""));

        userPhotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View dialog_layout = inflater.inflate(R.layout.choose_foto_dialog, (ViewGroup) view1.findViewById(R.id.root));
                Button buttonCamera = (Button) dialog_layout.findViewById(R.id.buttonCamera);
                Button buttonGallery = (Button) dialog_layout.findViewById(R.id.buttonGallery);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialog_layout)
                        .setCancelable(true);
                final AlertDialog alert = builder.create();
                alert.show();

                buttonCamera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
                    }
                });

                buttonGallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.PICK_FROM_GALLERY);
                    }
                });
            }
        });

        return view;
    }

    private void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
                @Override
                public void onSwipeTop() {
                    getFragmentManager().popBackStack();
                    ((DriverFragmentActivity) getActivity()).setMenuVisible(false);
                    ((DriverFragmentActivity) getActivity()).setActionBarYellow();
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, userPhotoImageView, size);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });

        commonRequest.getOwnAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Constants.PICK_FROM_CAMERA) {
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + "img.jpg");
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
            sendPhotoToServer(bitmap, Constants.IMG_LARGE_SIZE);

            try {
                cropCapturedImage(Uri.fromFile(file));

            } catch(ActivityNotFoundException aNFE) {
                Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");
            }

        } else if (requestCode == Constants.PIC_CROP && data != null) {
            Bundle extras = data.getExtras();
            Bitmap bitmap = null;
            if (extras == null){
                Uri imageUri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                bitmap = extras.getParcelable(Constants.DATA);
            }

            userPhotoImageView.setImageBitmap(Utils.getRoundedCornerBitmap(bitmap,
                    Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, getActivity())));
            sendPhotoToServer(bitmap, Constants.IMG_SMALL_SIZE);

        } else if (requestCode == Constants.PICK_FROM_GALLERY && data != null) {

            try {
                selectedImageURI = data.getData();
                File file = new File(Utils.getRealPathFromURI(getActivity(), selectedImageURI));
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageURI);
                sendPhotoToServer(bitmap, Constants.IMG_LARGE_SIZE);
                cropCapturedImage(Uri.fromFile(file));

            } catch(ActivityNotFoundException aNFE) {
                Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");

            } catch (Exception e) {
                Utils.showAlertDialog(getActivity(), "Неподдерживаемый тип документа. Выберите изображение, пожалуйста.");
                e.printStackTrace();
            }
        }
    }

    private void sendPhotoToServer(final Bitmap bitmap, final String type) {
        if (bitmap == null) return;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        String stringBase64Original = android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT);
        bitmap.recycle();

        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageLoader.clearCache();
                    }
                }, 10000);
                imageLoader.clearCache();
            }

            @Override
            public void onMessageError(int errorCode) {
                Log.d("TAG", "" + errorCode);
            }
        });
        commonRequest.setAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), type, stringBase64Original);
    }

    public void cropCapturedImage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 100);
        cropIntent.putExtra("outputY", 100);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, Constants.PIC_CROP);
    }
}