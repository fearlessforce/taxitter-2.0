package com.mobapply.taxitter.ui.passenger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.interfaces.TransmissionText;
import com.mobapply.taxitter.model.Address;
import com.mobapply.taxitter.model.CordsUtils;
import com.mobapply.taxitter.ui.BaseActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.DistanceCalculation;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ConfirmationActivity extends BaseActivity {
    private final int BD_ERROR = 1;
    private final int CORDS_ARE_EMPTY = 11;
    private final int NOT_ALL_FIELDS_ARE_FULL = 12;
    private final int JSON_ERROR = 13;
    private final int WRONG_ACCESS_TOKEN = 41;
    private final int CANNOT_CREATE_REQUEST = 500;

    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private View mCustomView;

    private Button menuButton;
    private TextView fromWhereTextView;
    private TextView whereTextView;
    private TextView passagersCountTextView;
    private TextView timeTextView;
    private TextView distanceTextView;
    private ImageButton babySeatButton, airConditionButton, noSmokingButton;
    private ImageButton refuseButton, confirmButton;

    private ArrayList<CordsUtils> cordsUtilsArrayList;
    private ArrayList<Address> addressArrayList;
    private double latitudeA = 0.0;
    private double longitudeA = 0.0;
    private double latitudeB = 0.0;
    private double longitudeB = 0.0;
    private boolean isMenuVisible;
    private boolean isLoadDistance = false;
    private int textViewCounter = 1;
    private int babyseat = 0;
    private int nosmoking = 0;
    private int ac = 0;
    private String passengers;
    private String fromWhere;
    private String whereTo;
    private String time;
    private String comment;
    private String distance;
    double allDistance = 0;
    private int requestCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passenger_confirm_order_activity);
        fragmentManager = getFragmentManager();
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        cordsUtilsArrayList = new ArrayList<CordsUtils>();
        addressArrayList = new ArrayList<Address>();
        final Intent intent = getIntent();
        initUI();
        ActionBar mActionBar = getActionBar();

        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.action_bar_custom, null);
        menuButton = (Button) mCustomView.findViewById(R.id.buttonMenu);

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isMenuVisible == false) {
                    openMenuFragment();
                    setActionBarDark();

                } else {
                    fragmentManager.popBackStack();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setActionBarYellow();
                        }
                    }, 600);
                }

                isMenuVisible = !isMenuVisible;
            }
        });

        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            mActionBar.setCustomView(mCustomView);
            mActionBar.setDisplayShowCustomEnabled(true);
        }

        if(intent != null) {
            fromWhere = intent.getStringExtra(Constants.FROM_WHERE);
            whereTo = intent.getStringExtra(Constants.WHERE_TO);
            passengers = intent.getStringExtra(Constants.PASSENGERS);
            time = intent.getStringExtra(Constants.TIME);
            comment = intent.getStringExtra(Constants.COMMENT);
            addressArrayList = intent.getParcelableArrayListExtra(Constants.ADDRESS_ARRAY);

            if (addressArrayList != null && addressArrayList.size() >= 2) {
                latitudeA = addressArrayList.get(0).getLatitude();
                longitudeA = addressArrayList.get(0).getLongitude();
                latitudeB = addressArrayList.get(addressArrayList.size() - 1).getLatitude();
                longitudeB = addressArrayList.get(addressArrayList.size() - 1).getLongitude();

                for (int i = 0; i < addressArrayList.size(); i++) {
                    CordsUtils coords = new CordsUtils();
                    coords.setLatitude(addressArrayList.get(i).getLatitude());
                    coords.setLongitude(addressArrayList.get(i).getLongitude());
                    coords.setAddress(addressArrayList.get(i).getHalfAddressForServer());
                    cordsUtilsArrayList.add(coords);
                }

                calculateDistance(addressArrayList);
            }

            fromWhereTextView.setText(fromWhere);
            whereTextView.setText(whereTo);
            passagersCountTextView.setText(passengers + " " + getString(R.string.passengers));
            timeTextView.setText(time);

            if (intent.getBooleanExtra(Constants.AIR_CONDITION, false)) {
                airConditionButton.setImageResource(R.drawable.conditions_conditioner_pressed_details_order);
                ac = 1;

            } else {
                airConditionButton.setImageResource(R.drawable.conditions_conditioner_details_order);
                ac = 0;
            }

            if (intent.getBooleanExtra(Constants.NO_SMOKING, false)) {
                noSmokingButton.setImageResource(R.drawable.conditions_smoker_pressed_details_order);
                nosmoking = 1;

            } else {
                noSmokingButton.setImageResource(R.drawable.conditions_smoker_details_order);
                nosmoking = 0;
            }

            if (intent.getBooleanExtra(Constants.BABY_SEAT, false)) {
                babySeatButton.setImageResource(R.drawable.conditions_babyseat_pressed_details_order);
                babyseat = 1;

            } else {
                babySeatButton.setImageResource(R.drawable.conditions_babyseat_details_order);
                babyseat = 0;
            }
        }

        refuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Constants.PASSENGER_CONFIRM_ORDER_STATE, false);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLoadDistance) {
                    PassengerRequester passengerRequester = new PassengerRequester(ConfirmationActivity.this);
                    passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                        @Override
                        public void onMessageSuccess(String str) {
                            Utils.hideProgressDialog();
                            isLoadDistance = false;
                            OrderTaxiFragment.isOrderAccept(true);
                            Intent intent = new Intent();
                            intent.putExtra(Constants.PASSENGER_CONFIRM_ORDER_STATE, true);
                            setResult(RESULT_OK, intent);
                            finish();
                        }

                        @Override
                        public void onMessageError(int errorCode) {
                            Utils.hideProgressDialog();
                            checkOrderErrorCode(errorCode);
                        }
                    });

                    String access_token = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
                    String city = sharedPreferences.getString(Constants.CITY_D, "");
                    String date = convertStringToDate(timeTextView.getText().toString());
                    String latitudeAStr = String.valueOf(latitudeA);
                    String longitudeAStr = String.valueOf(longitudeA);
                    String latitudeBStr = String.valueOf(latitudeB);
                    String longitudeBStr = String.valueOf(longitudeB);

                    passengerRequester.sendOrder(access_token, date, latitudeAStr, longitudeAStr, fromWhere, city,
                            "0", nosmoking, babyseat, ac, passengers, cordsUtilsArrayList, latitudeBStr, longitudeBStr, comment,
                            String.valueOf(allDistance));
                    Utils.showProgressDialog(ConfirmationActivity.this, getString(R.string.loading));

                } else {
                    Utils.showAlertDialog(ConfirmationActivity.this, getString(R.string.query_is_executed));
                }
            }
        });

        LinearLayout linear = (LinearLayout) findViewById(R.id.linearDots);

//      Add fields for additional points
        for (int i = 0; i < OrderTaxiFragment.WHERE_COUNTER; i++) {
            textViewCounter++;
            View view = getLayoutInflater().inflate(R.layout.passenger_additional_dotview_layout, null);
            ((TextView) view.findViewById(R.id.counter)).setText(String.valueOf(textViewCounter));
            ((TextView)view.findViewById(R.id.whereText)).setText(addressArrayList.get(i + 2).getHalfAddress());
            linear.addView(view);
        }
    }

    private void initUI() {
        fromWhereTextView = (TextView) findViewById(R.id.fromWhereTextView);
        whereTextView = (TextView) findViewById(R.id.whereTextView);
        passagersCountTextView = (TextView) findViewById(R.id.textViewCounter);
        timeTextView = (TextView) findViewById(R.id.setTime);
        distanceTextView = (TextView) findViewById(R.id.distanceTextView);
        airConditionButton = (ImageButton) findViewById(R.id.airconditionB);
        babySeatButton = (ImageButton) findViewById(R.id.babyseatB);
        noSmokingButton = (ImageButton) findViewById(R.id.noSmokingB);
        refuseButton = (ImageButton) findViewById(R.id.backBtn);
        confirmButton = (ImageButton) findViewById(R.id.orderButton);
    }

    private void openMenuFragment() {
        PassengerMenuFragment passengerMenuFragment = new PassengerMenuFragment();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close,
                        R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close)
                .add(R.id.menu_container, passengerMenuFragment, Constants.DRIVER_MENU_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    public void setMenuVisible(boolean state) {
        isMenuVisible = state;
    }

    protected void setActionBarDark() {
        menuButton.setBackgroundResource(R.drawable.ic_action_menu_yellow);
        mCustomView.setBackgroundResource(R.drawable.action_bar_dark);
    }

    protected void setActionBarYellow() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                menuButton.setBackgroundResource(R.drawable.ic_action_menu);
                mCustomView.setBackgroundResource(R.drawable.action_bar);
            }
        }, 600);
    }

    private void calculateDistance(ArrayList<Address> addressArray) {
        requestCounter = 2;

        for(int i = 0; i < addressArray.size() -1; i++) {
            LatLng latLngA = new LatLng(addressArray.get(i).getLatitude(), addressArray.get(i).getLongitude());
            LatLng latLngB = new LatLng(addressArray.get(i + 1).getLatitude(), addressArray.get(i + 1).getLongitude());

            DistanceCalculation distanceCalculation = new DistanceCalculation(this, latLngA, latLngB, new TransmissionText() {
                @Override
                public void transmissionText(String str) {
                    distance = str.replace(",", ".");
                    if (distance.length() != 0)
                        allDistance += Double.parseDouble(distance);

                    if (addressArrayList != null && requestCounter == addressArrayList.size()) {
                        String distanceStr = new DecimalFormat("##.#").format(allDistance);
                        distanceTextView.setText(distanceStr + " км");
                        isLoadDistance = true;
                    }
                    requestCounter++;
                }
            });
        }
    }

    private String convertStringToDate(String str) {
        if (!str.equals(getString(R.string.now))) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
            Date myDate = null;

            try {
                myDate = dateFormat.parse(str);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMddHHmm");
            return timeFormat.format(myDate);

        } else {
            return "";
        }
    }

    private void checkOrderErrorCode(int errorCode) {
        if (errorCode == BD_ERROR) {
            Utils.showToast(ConfirmationActivity.this, getString(R.string.db_error));

        } else if (errorCode == CORDS_ARE_EMPTY) {
            Utils.showToast(ConfirmationActivity.this, getString(R.string.cords_are_empty));

        } else if (errorCode == NOT_ALL_FIELDS_ARE_FULL) {
            Utils.showToast(ConfirmationActivity.this, getString(R.string.not_all_fields));

        } else if(errorCode == JSON_ERROR) {
            Utils.showToast(ConfirmationActivity.this, getString(R.string.json_error));

        } else if(errorCode == WRONG_ACCESS_TOKEN) {
            PassengerFragmentActivity.isAccessTokenGone(true);
            finish();

        } else if(errorCode == CANNOT_CREATE_REQUEST) {
            Utils.showToast(ConfirmationActivity.this, getString(R.string.cant_create_request));
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fr = fragmentManager.findFragmentById(R.id.menu_container);

        if (fr instanceof PassengerMenuFragment) {
            isMenuVisible = !isMenuVisible;
            fragmentManager.popBackStack();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setActionBarYellow();
                }
            }, 600);

        } else if (fr instanceof PassengerProfileFragment || fr instanceof PassengerSettingFragment
                || fr instanceof FavoriteAddressFragment) {
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else {
            super.onBackPressed();
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            finish();
        }
    }
}