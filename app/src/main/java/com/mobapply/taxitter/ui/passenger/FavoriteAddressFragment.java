package com.mobapply.taxitter.ui.passenger;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.FavoriteAddressArrayAdapter;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.FavoriteAddress;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Misha Nester on 29.04.15.
 */
public class FavoriteAddressFragment extends Fragment {
    private static final int REQUEST_EDITOR = 51215;
    private SharedPreferences sharedPreferences;
    private ArrayList<FavoriteAddress> favoriteAddressArray;
    private FavoriteAddressArrayAdapter favoriteAddressArrayAdapter;
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_layout, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        favoriteAddressArray = new ArrayList<FavoriteAddress>();
        listView = (ListView) view.findViewById(R.id.listView);


        favoriteAddressArrayAdapter = new FavoriteAddressArrayAdapter(
                getActivity(), favoriteAddressArray);
        listView.setAdapter(favoriteAddressArrayAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListAddress();
            }
        });

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), EditFavaddressActivity.class);
                intent.putExtra(Constants.ADDRESS, favoriteAddressArray.get(position));
                startActivityForResult(intent, REQUEST_EDITOR);
            }
        });

        getListAddress();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (intent == null) {
            return;
        }

        if (requestCode == REQUEST_EDITOR) {
            if (isVisible())
                getListAddress();
        }
    }

    private void getListAddress() {
        PassengerRequester passengerRequester = new PassengerRequester(getActivity());
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                swipeRefreshLayout.setRefreshing(false);
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            JSONArray jsonArray = jsonObject.getJSONArray("address");

                            if (jsonArray.length() != 0)
                                favoriteAddressArrayAdapter.clear();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsObject = jsonArray.getJSONObject(i);
                                FavoriteAddress favoriteAddress = new FavoriteAddress(jsObject);
                                favoriteAddress.setFullAddress(Constants.PROFILE_COUNTRY
                                        + sharedPreferences.getString(Constants.AREA_AND_CITY, "")
                                        + ", " + favoriteAddress.getHalfAddress());
                                favoriteAddressArray.add(favoriteAddress);
                            }

                            if (isVisible())
                                favoriteAddressArrayAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                swipeRefreshLayout.setRefreshing(false);
                Utils.showToast(getActivity(), "errer: " + errorCode);
            }
        });
        passengerRequester.getFavoriteAddress(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));
        swipeRefreshLayout.setRefreshing(true);
    }
}