package com.mobapply.taxitter.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.utils.Constants;

public class LaunchingFragment extends Fragment{
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.launching_fragment, container, false);
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        String cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");

        if (!MainFragmentActivity.getAccessTokenState() && !MainFragmentActivity.getIsLogOff() && !cookie.equals("")) {
            ChoiceOfRolesFragment choiceOfRolesFragment = new ChoiceOfRolesFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.container, choiceOfRolesFragment, Constants.CHOICE_ROLE_TAG)
                    .addToBackStack(Constants.CHOICE_ROLE_TAG)
                    .commit();

        } else {
            sharedPreferences.edit().putBoolean(Constants.NEW_ORDER_PUSH, true).apply();
        }

        ((Button)view.findViewById(R.id.buttonRegistration)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreRegisterFragment preRegisterFragment = new PreRegisterFragment();
                fragmentManager.beginTransaction()
                        .add(R.id.container, preRegisterFragment, Constants.REGISTER_TAG)
                        .addToBackStack(Constants.REGISTER_STACK)
                        .commit();
            }
        });

        ((Button)view.findViewById(R.id.buttonEnter)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EnterFragment enterFragment = new EnterFragment();
                fragmentManager.beginTransaction()
                        .add(R.id.container, enterFragment, Constants.ENTER_TAG)
                        .addToBackStack(Constants.ENTER_STACK)
                        .commit();
            }
        });

        return view;
    }
}