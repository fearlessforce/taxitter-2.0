package com.mobapply.taxitter.ui.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.AdditionalRoute;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.service.GPSService;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.DriverRequester;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CurrentDriverOrder extends Fragment {
    private final String STATUS_ON_THE_PLACE = "1";
    private final String STATUS_PASS_IN_THE_CAR = "2";
    private final String STATUS_RIDE_OVER = "3";

    private View view;
    private Order order;
    private ArrayList<AdditionalRoute> dotsListsArray;
    private SharedPreferences sharedPreferences;
    private ImageLoader imageLoader;
    private Dialog dialogPhoto;

    private ToggleButton seetInCarButton;
    private ImageView photoImageView;
    private TextView whenGoTextView;
    private TextView fromWhereTopTextView;
    private TextView whereToTopTextView;
    private TextView nickNameTextView;
    private TextView ratingTextView;
    private TextView countPassengerTopTextView, countPassengerTextView;
    private TextView commentTopTextView;
    private TextView streetTextView, houseTextView, entranceTextView;
    private Button onThePlaceButton;
    private Button passengerRefuseButton;
    private Button rideOverButton;
    private Button closeButton;
    private ImageView imageViewLargePhoto;

    private String number;
    private String id;
    private String order_uid;
    private String cash_d;
    private String length;
    private String latitude, longitude;
    private String when, departure;
    private String comment;
    private String uid_client;
    private String client;
    private int nonsmoking, babyseat, ac;
    private int rating, quantity;
    private boolean isImgLoaded;
    private boolean isNeedDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.driver_current_order_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        imageLoader = ImageLoader.getInstance();
        initUI();
        getValueFromArray(order);
        createDialogPhoto();
        setDataIntoUI(order);
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);

        sharedPreferences.edit().putBoolean(Constants.CURRENT_ORDER_ACCEPT, true).apply();
        sharedPreferences.edit().putString(Constants.CURRENT_ORDER_UID, order.getOrder_uid()).apply();

        if (sharedPreferences.getBoolean(Constants.PASS_SEET_IN_CAR, false))
            setSeetInCarButtonStatus();

        onThePlaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!sharedPreferences.getBoolean(Constants.PASS_SEET_IN_CAR, false)) {
                    DriverRequester driverRequester = new DriverRequester(getActivity());
                    driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                        @Override
                        public void onMessageSuccess(String str) {
                            Utils.hideProgressDialog();
                            if (isVisible())
                                Utils.showAlertDialog(getActivity(), getString(R.string.client_notified));
                        }

                        @Override
                        public void onMessageError(int errorCode) {
                            Utils.hideProgressDialog();

                            if (errorCode != Constants.NO_NETWORK_CONNECTION) {
//                              ErrorCode 14 unknown, but should be
                                if (errorCode != 14 && isVisible()) {
                                    Utils.showToast(getActivity(), "" + errorCode);
                                    backForAuthorization(errorCode);
                                }
                            }
                        }
                    });
                    driverRequester.currentOrderStatus(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                            order_uid, STATUS_ON_THE_PLACE, cash_d);
                    Utils.showProgressDialog(getActivity(), getString(R.string.loading));
                }
            }
        });


        seetInCarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!sharedPreferences.getBoolean(Constants.PASS_SEET_IN_CAR, false)) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setMessage(getString(R.string.passenger_seat_in_car));
                    alertDialog.setPositiveButton(getString(R.string.accept_on_the_place), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            getActivity().stopService(new Intent(getActivity(), GPSService.class));
                            DriverRequester driverRequester = new DriverRequester(getActivity());
                            driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                                @Override
                                public void onMessageSuccess(String str) {
                                    Utils.hideProgressDialog();
                                    sharedPreferences.edit().putBoolean(Constants.PASS_SEET_IN_CAR, true).apply();
                                    setSeetInCarButtonStatus();
                                }

                                @Override
                                public void onMessageError(int errorCode) {
                                    Utils.hideProgressDialog();

                                    if (errorCode != Constants.NO_NETWORK_CONNECTION) {
                                        Utils.showToast(getActivity(), "" + errorCode);
                                        backForAuthorization(errorCode);
                                    }
                                }
                            });
                            driverRequester.currentOrderStatus(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                                    order_uid, STATUS_PASS_IN_THE_CAR, cash_d);
                            Utils.showProgressDialog(getActivity(), getString(R.string.loading));
                        }
                    });

                    alertDialog.setNegativeButton(getString(R.string.reject_on_the_place), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                }
            }
        });

        passengerRefuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DriverRequester driverRequester = new DriverRequester(getActivity());
                driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        Utils.hideProgressDialog();
                        backOnListProposals();
                        sharedPreferences.edit().putBoolean(Constants.PASS_SEET_IN_CAR, false).apply();
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        Utils.hideProgressDialog();
                        isNoNetworkConnection(errorCode);
                    }
                });
                driverRequester.cancelOrder(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), order_uid);
                Utils.showProgressDialog(getActivity(), getString(R.string.loading));
            }
        });

        rideOverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().stopService(new Intent(getActivity(), GPSService.class));
                DriverRequester driverRequester = new DriverRequester(getActivity());
                driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        Utils.hideProgressDialog();
                        backOnListProposals();
                        sharedPreferences.edit().putBoolean(Constants.PASS_SEET_IN_CAR, false).apply();
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        Utils.hideProgressDialog();
                        isNoNetworkConnection(errorCode);
                    }
                });
                driverRequester.currentOrderStatus(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                        order_uid, STATUS_RIDE_OVER, cash_d);
                Utils.showProgressDialog(getActivity(), getString(R.string.loading));
            }
        });

        photoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLargePhoto();
            }
        });

        return view;
    }

    private void openLargePhoto() {
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPhoto.cancel();
            }
        });

        if ((Integer)imageViewLargePhoto.getTag() != R.drawable.round67) {
            dialogPhoto.show();
        } else {
            getAvatarFromServer(Constants.IMG_LARGE_SIZE);
            isNeedDialog = true;
            Utils.showProgressDialog(getActivity(), getString(R.string.loading));
        }
    }

    public void createDialogPhoto() {
        dialogPhoto = new Dialog(getActivity());
        dialogPhoto.setCanceledOnTouchOutside(true);
        dialogPhoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPhoto.setContentView(R.layout.large_foto_dialog);
        dialogPhoto.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialogPhoto.findViewById(R.id.button_close);
        imageViewLargePhoto = (ImageView) dialogPhoto.findViewById(R.id.imageViewLargePhoto);
        imageViewLargePhoto.setTag(R.drawable.round67);
    }

    private void setSeetInCarButtonStatus() {
        seetInCarButton.setBackgroundResource(R.drawable.pass_seet_in_car_pressed);
        seetInCarButton.setClickable(false);
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, photoImageView, size);
                                        }
                                    } else {
                                        imageLoader.DisplayImage(urlImage, imageViewLargePhoto, size);
                                        if (isNeedDialog && isVisible()) {
                                            Utils.hideProgressDialog();
                                            dialogPhoto.show();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.showToast(getActivity(), "" + errorCode);
            }
        });
        commonRequest.getOtherAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size, uid_client);
    }

    private void backOnListProposals() {
        removeOrderPref();
        Activity activity = getActivity();
        if (activity != null){
           FragmentManager fragmentManager = activity.getFragmentManager();
            if (fragmentManager != null){
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    private void isNoNetworkConnection(int errorCode) {
        if (errorCode != Constants.NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), "" + errorCode);
            backForAuthorization(errorCode);
        }
    }

    private void removeOrderPref() {
        sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
        sharedPreferences.edit().remove(Constants.CURRENT_ORDER_UID).apply();
    }

    private void backForAuthorization(int errorCode) {
        if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
            MainFragmentActivity.isAccessTokenGone(true);
            getActivity().finish();
        }
    }

    private void initUI() {
        onThePlaceButton = (Button)view.findViewById(R.id.buttonOnThePlace);
        seetInCarButton = (ToggleButton)view.findViewById(R.id.buttonPassengerSeetInCar);
        passengerRefuseButton = (Button)view.findViewById(R.id.buttonPassengerRefuse);
        rideOverButton = (Button)view.findViewById(R.id.buttonRideOver);

        fromWhereTopTextView = (TextView)view.findViewById(R.id.textViewFromWhereTop);
        whereToTopTextView = (TextView)view.findViewById(R.id.textViewWhereToTop);
        photoImageView = (ImageView)view.findViewById(R.id.imageViewPhoto);
        nickNameTextView = (TextView)view.findViewById(R.id.textViewNicknameOrder);
//        ratingTextView = (TextView)view.findViewById(R.id.textViewRating);
        countPassengerTopTextView = (TextView)view.findViewById(R.id.textViewPassengerOrder);
        commentTopTextView = (TextView)view.findViewById(R.id.textViewCommentOrder);
        streetTextView = (TextView)view.findViewById(R.id.textViewStreet);
        houseTextView = (TextView)view.findViewById(R.id.editTextHouse);
        entranceTextView = (TextView)view.findViewById(R.id.editTextEntrance);
        countPassengerTextView = (TextView)view.findViewById(R.id.textViewCountPassengerOffer);
        whenGoTextView = (TextView)view.findViewById(R.id.textViewWhenOffer);
    }

    private void getValueFromArray(Order item) {
        order_uid = item.getOrder_uid() == null ? "" : item.getOrder_uid();
        when = item.getWhen() == null ? "" : item.getWhen();
        latitude = item.getLatitude() == null ? "" : item.getLatitude();
        longitude = item.getLongitude() == null ? "" : item.getLongitude();
        departure = item.getDeparture() == null ? "" : item.getDeparture();
        cash_d = item.getCash_d() == null ? "": item.getCash_d();
        comment = item.getComment() == null ? "" : item.getComment();
        length = item.getLength() == null ? "" : item.getLength();
        uid_client = item.getUid_client() == null ? "" : item.getUid_client();
        client = item.getClient() == null ? "" : item.getClient();
        number = item.getNumber();
        rating = item.getRating();
        id = item.getId();
        babyseat = item.getBabyseat();
        ac = item.getAc();
        nonsmoking = item.getNonsmoking();
        quantity = item.getQuantity();
        dotsListsArray = item.getAdditionalRoutes();
    }

    private void setDataIntoUI(Order item) {
        fromWhereTopTextView.setText(departure);
        String[] addressArray = departure.split(", ");

        if (addressArray.length >= 1)
            streetTextView.setText(addressArray[0]);

        if (addressArray.length >= 2)
            houseTextView.setText(addressArray[1]);

        if (addressArray.length >= 3)
            entranceTextView.setText(addressArray[2]);

        if (streetTextView.getText().toString().equals(""))
            streetTextView.setText("-");

        if (houseTextView.getText().toString().equals(""))
            houseTextView.setText("-");

        if (entranceTextView.getText().toString().equals(""))
            entranceTextView.setText("-");

        for (int i = 0; i < dotsListsArray.size(); i++) {
            whereToTopTextView.setText(dotsListsArray.get(i).getWayaddress());
        }

        nickNameTextView.setText(client);
//        ratingTextView.setText("" + rating);

        countPassengerTopTextView.setText("" + quantity);

        if (comment.length() != 0) {
            commentTopTextView.setVisibility(View.VISIBLE);
            commentTopTextView.setText(comment);
        }

        for (int i = 0; i <= dotsListsArray.size(); i++) {
            ViewGroup inclusionViewGroup = (ViewGroup)view.findViewById(R.id.linerDotsOffer);
            View child1 = LayoutInflater.from(getActivity()).inflate(R.layout.offer_dot_item, null);
            inclusionViewGroup.addView(child1);
            TextView left = (TextView)child1.findViewById(R.id.textViewDotLeftNumber);
            TextView right = (TextView)child1.findViewById(R.id.textViewDotText);
            ImageView imageDoterLine = (ImageView)child1.findViewById(R.id.imageViewDoterLineOffer);

            if (i == dotsListsArray.size()) {
                left.setText("");
                right.setText(length.length() == 0 ? "" : length + " км" );
                right.setTextSize(14);
                right.setTextColor(getResources().getColor(R.color.white_gray));
                imageDoterLine.setVisibility(View.INVISIBLE);

            } else {
                left.setText("" + (i + 1));
                right.setText(dotsListsArray.get(i).getWayaddress());
            }
        }
        countPassengerTextView.setText(quantity + " " + getString(R.string.passengers));
        whenGoTextView.setText(when.length() != 0 ? when : getString(R.string.soon));
    }

    public void setDetailItem(Order item) {
        this.order = item;
    }
}