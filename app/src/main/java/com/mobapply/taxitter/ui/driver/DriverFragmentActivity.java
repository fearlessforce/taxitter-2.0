package com.mobapply.taxitter.ui.driver;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ViewPagerAdapter;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.gcm.GcmIntentService;
import com.mobapply.taxitter.interfaces.DetailsOrderModuleListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.interfaces.TransmissionText;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.ui.BaseFragmentActivity;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.DistanceCalculation;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.DriverRequester;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * FragmentActivity that controls the driver layers.
 * Create two tabs programmatically for basic ListOrderFragment and ListOrdersOnTabTwoFragment.
 */
public class DriverFragmentActivity extends BaseFragmentActivity implements DetailsOrderModuleListener, TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    public static boolean ACTIVE = false;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences, sharedPref;
    private TabHost mTabHost;
    private ViewPager mViewPager;
    private ViewPagerAdapter mPagerAdapter;
    private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, DriverFragmentActivity.TabInfo>();
    private ImageLoader imageLoader;

    private View mCustomView;
    private Button menuButton;
    private TextView ordersTabTextView, offersTabTextView;

    private int tabPosition;
    private boolean isMenuVisible;
    private boolean isResumed = false;
    private boolean isCreated;
    private String cash;
    private double longitude;
    private double latitude;
    private String length;
    private String whereTo;
    private String fromWhere;

    @Override
    public void openDetailsOrder(Order item) {
        DetailsOfOrderFragment detailOfOrder = new DetailsOfOrderFragment();
        detailOfOrder.setDetailItem(item);
        fragmentManager.beginTransaction()
                .add(R.id.fragments_container, detailOfOrder, Constants.DRIVER_DETAILE_OF_ORDER_TAG)
                .addToBackStack(Constants.DRIVER_DETAILE_OF_ORDER_TAG)
                .commit();
    }

    private class TabInfo {
        private String tag;
        private ListOrdersFragment listOrdersFragment;
        private ListOrdersOnTabTwoFragment listOrdersOnTabTwoFragment;

        public TabInfo(String tab1, ListOrdersFragment listOrdersFragment) {
            this.tag = tab1;
            this.listOrdersFragment = listOrdersFragment;
        }

        public TabInfo(String tab2, ListOrdersOnTabTwoFragment listOrdersOnTabTwoFragment) {
            this.tag = tab2;
            this.listOrdersOnTabTwoFragment = listOrdersOnTabTwoFragment;
        }
    }

    class TabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_swipe_layout);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getSharedPreferences(getString(R.string.pref_role_key), 0);
        registerReceiver(receiverDriverFragmentActivity, new IntentFilter(Constants.BROADCAST_ACTION));

        imageLoader = ImageLoader.getInstance();
        fragmentManager = getFragmentManager();
        ActionBar mActionBar = getActionBar();

        LayoutInflater mInflater = LayoutInflater.from(this);
        this.initTabHost();

        final DriverRequester driverRequester = new DriverRequester(this);
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                Log.d("TAG", "SUCCESS_ROLE: 1");
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });
        driverRequester.changeRole(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), Constants.PASSENGER_ROLE);

        String roleState = sharedPref.getString(Constants.ROLE_SATE, "");
        assert roleState != null;
        if (roleState.equals(Constants.FIRST_ENTER)) {
            sharedPreferences.edit().putBoolean(Constants.NEW_ORDER_PUSH, true).apply();
            openProfileFragment();
            Utils.showToast(this, getString(R.string.fill_profile));
        }

        sharedPref.edit().putString(Constants.ROLE_SATE, Constants.PASSENGER_ROLE).apply();
        this.initViewPager();
        mCustomView = mInflater.inflate(R.layout.action_bar_custom, null);
        menuButton = (Button) mCustomView.findViewById(R.id.buttonMenu);

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (!isMenuVisible) {
                    openMenuFragment();
                    setActionBarDark();

                } else {
                    fragmentManager.popBackStack();
                    setActionBarYellow();
                }
                isMenuVisible = !isMenuVisible;
            }
        });

        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            mActionBar.setCustomView(mCustomView);
            mActionBar.setDisplayShowCustomEnabled(true);
        }

        if (sharedPreferences.getBoolean(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL, false)) {
            mTabHost.setCurrentTab(1);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((ListOrdersOnTabTwoFragment) mPagerAdapter.getItem(1)).reloadList();
                }
            }, 200);
            sharedPreferences.edit().remove(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL).apply();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (sharedPreferences.getBoolean(Constants.NEW_ORDER, false)) {
            sharedPreferences.edit().remove(Constants.NEW_ORDER).apply();
            notificationManager.cancelAll();
        }
    }

    private void openProfileFragment() {
        DriverProfileFragment driverProfileFragment = new DriverProfileFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.menu_container, driverProfileFragment, DriverMenuFragment.DRIVER_PROFILE_FRAGMENT)
                .addToBackStack(DriverMenuFragment.DRIVER_PROFILE_FRAGMENT)
                .commit();
    }

    private BroadcastReceiver receiverDriverFragmentActivity = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isResumed) {
                if (intent.hasExtra(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL) && intent.getBooleanExtra(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL, false)) {
                    if (mTabHost.getCurrentTab() == 1) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DriverFragmentActivity.this);
                        alertDialog.setMessage(getString(R.string.passenger_accept_driver_proposal));
                        alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                ((ListOrdersOnTabTwoFragment) mPagerAdapter.getItem(1)).reloadList();
                                sharedPreferences.edit().remove(Constants.PASSENGER_ACCEPT_DRIVER_PROPOSAL).apply();
                            }
                        });
                        alertDialog.show();

                    } else {
                        Utils.sendNotification(DriverFragmentActivity.this, getString(R.string.passenger_accept_driver_proposal), "");
                    }

                } else if (intent.hasExtra(Constants.PASSENGER_CANCEL_ORDER) && intent.getBooleanExtra(Constants.PASSENGER_CANCEL_ORDER, false)) {
                    if (mTabHost.getCurrentTab() == 1) {
                        mTabHost.setCurrentTab(0);
                        sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER).apply();
                        sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
                        ((ListOrdersOnTabTwoFragment)mPagerAdapter.getItem(1)).reloadList();
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                        if (!ACTIVE) {
                            Intent intentM = new Intent(DriverFragmentActivity.this, MainFragmentActivity.class);
                            intentM.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intentM.putExtra(Constants.EXIT, true);
                            startActivity(intentM);
                        }
                    }
                }
            }
        }
    };

    private void sendPush() {
        DistanceCalculation distanceCalculation = new DistanceCalculation(getApplicationContext(), latitude, longitude);
        distanceCalculation.setTransmissionTextListener(new TransmissionText() {
            @Override
            public void transmissionText(String distance) {
                sharedPreferences.edit().putBoolean(Constants.NEW_ORDER, true).apply();
                double d = Double.parseDouble(distance);
                distance = new DecimalFormat("##.##").format(d);
                GcmIntentService.sendNotification(getApplicationContext(), distance, fromWhere, whereTo, length, cash);
            }
        });
    }

    protected void setActionBarDark() {
        menuButton.setBackgroundResource(R.drawable.ic_action_menu_yellow);
        mCustomView.setBackgroundResource(R.drawable.action_bar_dark);
    }

    protected void setActionBarYellow() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                menuButton.setBackgroundResource(R.drawable.ic_action_menu);
                mCustomView.setBackgroundResource(R.drawable.action_bar);
            }
        }, 600);
    }

    public void setMenuVisible(boolean state) {
        isMenuVisible = state;
    }

    private void openMenuFragment() {
        DriverMenuFragment driverMenuFragment = new DriverMenuFragment();
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close,
                        R.anim.anim_fragment_open,
                        R.anim.anim_fragment_close)
                .add(R.id.menu_container, driverMenuFragment, Constants.DRIVER_MENU_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    private void initViewPager() {
        List<Fragment> fragments = new Vector<Fragment>();
        fragments.add(Fragment.instantiate(this, ListOrdersFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, ListOrdersOnTabTwoFragment.class.getName()));
        mPagerAdapter  = new ViewPagerAdapter(super.getFragmentManager(), fragments);
        mViewPager = (ViewPager)super.findViewById(R.id.viewpager);
        mViewPager.setAdapter(this.mPagerAdapter);
        mViewPager.setOnPageChangeListener(this);
    }

    private void initTabHost() {
        mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup();
        TabInfo tabInfo = null;

        ListOrdersFragment listOrdersFragment = new ListOrdersFragment();
        ListOrdersOnTabTwoFragment listOrdersOnTabTwoFragment = new ListOrdersOnTabTwoFragment();

        DriverFragmentActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab1").setIndicator(getString(R.string.orders)),
                (tabInfo = new TabInfo("Tab1", listOrdersFragment)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_active);

        ordersTabTextView = (TextView) mTabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
        ordersTabTextView.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        ordersTabTextView.setTextColor(getResources().getColor(R.color.tab_title_brown));
        ordersTabTextView.setGravity(Gravity.CENTER_VERTICAL);
        ordersTabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_new_order_icon_pressed, 0, 0, 0);
        ordersTabTextView.setCompoundDrawablePadding(10);

        DriverFragmentActivity.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec("Tab2").setIndicator(getString(R.string.proposals)),
                (tabInfo = new TabInfo("Tab2", listOrdersOnTabTwoFragment)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_not_active);

        offersTabTextView = (TextView) mTabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        offersTabTextView.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
        offersTabTextView.setTextColor(getResources().getColor(R.color.white));
        offersTabTextView.setGravity(Gravity.CENTER_VERTICAL);
        offersTabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.start_tab, 0, 0, 0);
        offersTabTextView.setCompoundDrawablePadding(10);

        mTabHost.setOnTabChangedListener(this);
    }

    private static void AddTab(DriverFragmentActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        tabSpec.setContent(activity.new TabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    public void onTabChanged(String tag) {
        tabPosition = this.mTabHost.getCurrentTab();
        if (mViewPager == null) return;
        mViewPager.setCurrentItem(tabPosition);

        if (tabPosition == 1) {
            mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_not_active);
            mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_active);
            offersTabTextView.setTextColor(getResources().getColor(R.color.tab_title_brown));
            ordersTabTextView.setTextColor(getResources().getColor(R.color.white));
            ordersTabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_new_order_icon, 0, 0, 0);

        } else {
            mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_active);
            mTabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_not_active);
            offersTabTextView.setTextColor(getResources().getColor(R.color.white));
            ordersTabTextView.setTextColor(getResources().getColor(R.color.tab_title_brown));
            ordersTabTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tab_new_order_icon_pressed, 0, 0, 0);
        }
    }

    public int getTabPosition() {
        return tabPosition;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        this.mTabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings)
            return true;

        return super.onOptionsItemSelected(item);
    }

    public void onHideAppClick(View v) {
        fragmentManager.popBackStack();
        setMenuVisible(false);
        setActionBarYellow();
        openHomeScreen();
        sharedPreferences.edit().putBoolean(Constants.NEW_ORDER_PUSH, true).apply();
    }

    public void onCloseAppClick(View v) {
        if (sharedPreferences.getBoolean(Constants.NEW_ORDER_PUSH, false)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setMessage(getString(R.string.cancel_push_new_order));
            alertDialog.setPositiveButton(getString(R.string.fine), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    closeApp();
                    sharedPreferences.edit().putBoolean(Constants.NEW_ORDER_PUSH, false).apply();
                }
            });

            alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();

        } else {
            closeApp();
        }
    }

    private void closeApp() {
        Intent intent = new Intent(getApplicationContext(), MainFragmentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.EXIT, true);
        startActivity(intent);
    }

    private void openHomeScreen() {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        this.startActivity(i);
    }

    public void onProfileClick(View v) {
        fragmentManager.popBackStack();
        DriverProfileFragment driverProfileFragment = new DriverProfileFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.menu_container, driverProfileFragment, DriverMenuFragment.DRIVER_PROFILE_FRAGMENT)
                .addToBackStack(DriverMenuFragment.DRIVER_PROFILE_FRAGMENT)
                .commit();
        setMenuVisible(false);
        setActionBarYellow();
    }

    public void onChangeRoleClick(View v) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(getString(R.string.change_role));
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                sharedPref.edit().putString(Constants.ROLE_SATE, Constants.CHANGE_ROLE_STATE).apply();
                finish();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void onRelativeBottomClick(View v) {
        fragmentManager.popBackStack();
        setMenuVisible(false);
        setActionBarYellow();
    }


    @Override
    public void onBackPressed() {
        int count = fragmentManager.getBackStackEntryCount();
        Fragment fr = fragmentManager.findFragmentById(R.id.menu_container);

        if (fr instanceof DriverMenuFragment) {
            isMenuVisible = !isMenuVisible;
            fragmentManager.popBackStack();
            setActionBarYellow();

        } else if (fr instanceof DriverProfileFragment) {
            for (int i = 0; i < count; i++) {
                String name = fragmentManager.getBackStackEntryAt(i).getName();

                if (name.equals(Constants.DRIVER_DETAILE_OF_OFFER_TAG)) {
                    boolean hasCurrentOrder = false;

                    for (int c = 0; c < count; c++) {
                        name = fragmentManager.getBackStackEntryAt(c).getName();

                        if (name.equals(Constants.CURRENT_ORDER)) {
                            hasCurrentOrder = true;
                        }
                    }

                    if (hasCurrentOrder)
                        fragmentManager.popBackStack(Constants.CURRENT_ORDER, 0);
                    else
                        fragmentManager.popBackStack(Constants.DRIVER_DETAILE_OF_OFFER_TAG, 0);

                    break;

                } else if (name.equals(Constants.DRIVER_DETAILE_OF_ORDER_TAG)) {
                    fragmentManager.popBackStack(Constants.DRIVER_DETAILE_OF_ORDER_TAG, 0);
                    break;

                } else if (name.equals(Constants.CURRENT_ORDER)) {
                    fragmentManager.popBackStack(Constants.CURRENT_ORDER, 0);
                    break;

                } else {
                    fragmentManager.popBackStack();
                }
            }

        } else if (fragmentManager.findFragmentById(R.id.fragments_container) instanceof DetailsOfOrderFragment) {
            fragmentManager.popBackStack();
            ListOrdersFragment.setIsNeedRefresh(true);

        } else if (fragmentManager.findFragmentById(R.id.fragments_container) instanceof DetailsOffersItemFragment) {
            fragmentManager.popBackStack();
            ListOrdersOnTabTwoFragment.setIsNeedRefresh(true);

        } else if (fragmentManager.findFragmentById(R.id.fragments_container) instanceof CurrentDriverOrder) {
            ListOrdersOnTabTwoFragment.setIsNeedRefresh(true);
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        } else {
            super.onBackPressed();
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER).apply();
            sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
            sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER_DIALOG).apply();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager notificationManager = Utils.getNotificationManager(this);
        notificationManager.cancelAll();

        ACTIVE = true;
        isResumed = true;
        sharedPreferences.edit().putBoolean(Constants.APP_OPENED, true).apply();

        if (isCreated) {
            if (sharedPreferences.getBoolean(Constants.PASSENGER_CANCEL_ORDER, false)) {
                if (mTabHost.getCurrentTab() == 1) {
                    mTabHost.setCurrentTab(0);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mTabHost.setCurrentTab(1);
                            ListOrdersOnTabTwoFragment.setIsNeedRefresh(true);
                            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            sharedPreferences.edit().remove(Constants.PASSENGER_CANCEL_ORDER).apply();
                            sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
                        }
                    }, 300);
                }
            }
        }
        isCreated = true;
        registerReceiver(receiverDriverFragmentActivity, new IntentFilter(Constants.BROADCAST_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        isResumed = false;
        sharedPreferences.edit().putBoolean(Constants.APP_OPENED, false).apply();
    }

    @Override
    public void onStop() {
        super.onStop();
        ACTIVE = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isCreated = false;
        imageLoader.clearCache();
        unregisterReceiver(receiverDriverFragmentActivity);
    }
}