package com.mobapply.taxitter.ui.passenger;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.City;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;
import com.mobapply.taxitter.webconnect.DriverRequester;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.castorflex.android.circularprogressbar.CircularProgressBar;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class PassengerProfileFragment extends Fragment implements View.OnKeyListener {
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences, sharedPref;
    private CircularProgressBar circularProgressBar;
    private List<String> listCities;
    private ArrayList<City> listCityArray;
    private ArrayAdapter<String> citiesSpinnerAdapter;
    private Spinner citiesSpinner;
    private Uri selectedImageURI;
    private ImageLoader imageLoader;

    private Button saveButton;
    private Button changeUserButton;
    private ImageView loadAvatarImageView;
    private EditText nicknameProfileEditText;
    private TextView ratingProfileTextView;
    private TextView passengerProfileTextView;

    private String stringBase64Original;
    private String cookie;
    private String nickname;
    private String quantity_d;
    private String areaAndCity;
    private String minCashe;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.passenger_profile_fragment, container, false);
        setupListener(view);
        fragmentManager = getFragmentManager();
        sharedPref = getActivity().getSharedPreferences(getString(R.string.pref_role_key), 0);
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        minCashe = sharedPreferences.getString(Constants.MINCASHE, "");

        listCityArray = new ArrayList<City>();
        listCities = new ArrayList<String>();
        imageLoader = ImageLoader.getInstance();

        initUI(view);
        nicknameProfileEditText.setOnKeyListener(this);
        getCities();
        getPrefData();
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);

        City city = new City();
        if (areaAndCity.equals("null")) {
            listCities.add(0, getString(R.string.choose_city));
            city.setNameCity(getString(R.string.choose_city));
            listCityArray.add(0, city);
        } else {
            listCities.add(0, areaAndCity);
            city.setNameCity(areaAndCity);
            listCityArray.add(0, city);
        }

        citiesSpinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, listCities);
        citiesSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        citiesSpinner.setAdapter(citiesSpinnerAdapter);
        citiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //saveButton.performClick();
                save();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setTextField();

        ((RelativeLayout)view.findViewById(R.id.relativeHak)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        nicknameProfileEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(getActivity());
                        nicknameProfileEditText.clearFocus();
                        saveButton.requestFocus();
                        //saveButton.performClick();
                        save();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Utils.hideKeyboard(getActivity());
                        nicknameProfileEditText.clearFocus();
                        saveButton.requestFocus();
                        //saveButton.performClick();
                        save();
                        break;
                }
                return false;
            }
        });

        nicknameProfileEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nicknameProfileEditText.setSelection(nicknameProfileEditText.length());
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!citiesSpinner.getSelectedItem().toString().equals(getString(R.string.choose_city))) {
                    PassengerRequester passengerRequester = new PassengerRequester(getActivity());
                    passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                        @Override
                        public void onMessageSuccess(String str) {
                            hideCircularBar();
                            int count = fragmentManager.getBackStackEntryCount();
                            Fragment fr = fragmentManager.findFragmentById(R.id.menu_container);

                            if (fr instanceof PassengerProfileFragment) {

                                for (int i = 0; i < count; i++) {
                                    String name = fragmentManager.getBackStackEntryAt(i).getName();
                                    boolean hasDetaileOffer = false;

                                    if (name.equals(Constants.PASSENGER_LIST_OF_DRIVER_PROPOSALS)) {

                                        for (int c = 0; c < count; c++) {
                                            name = fragmentManager.getBackStackEntryAt(c).getName();

                                            if (name.equals(Constants.PASSENGER_DETAILE_OF_PROPOSALS))
                                                hasDetaileOffer = true;
                                        }

                                        if (hasDetaileOffer) {
                                            fragmentManager.popBackStack(Constants.PASSENGER_DETAILE_OF_PROPOSALS, 0);
                                        } else {
                                            fragmentManager.popBackStack(Constants.PASSENGER_LIST_OF_DRIVER_PROPOSALS, 0);
                                        }
                                        break;

                                    } else {
                                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                        break;
                                    }
                                }
                            }
                        }

                        @Override
                        public void onMessageError(int errorCode) {
                            hideCircularBar();
                            Utils.showToast(getActivity(), DriverRequester.otherErrors(getActivity(), errorCode));
                        }
                    });

                    getDataFromField();
                    passengerRequester.savePassengerProfile(cookie, nickname, quantity_d, areaAndCity);
                    showCircularBar();

                } else {
                    Utils.showAlertDialog(getActivity(), getString(R.string.choose_city_profile));
                }
            }
        });

        loadAvatarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View dialog_layout = inflater.inflate(R.layout.choose_foto_dialog, (ViewGroup) view1.findViewById(R.id.root));

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialog_layout)
                        .setCancelable(true);
                final AlertDialog alert = builder.create();
                alert.show();

                ((Button)dialog_layout.findViewById(R.id.buttonCamera)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
                    }
                });

                ((Button)dialog_layout.findViewById(R.id.buttonGallery)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.PICK_FROM_GALLERY);
                    }
                });
            }
        });

        changeUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangeRoleDialog();
            }
        });

        return view;
    }

    private void save(){
            PassengerRequester passengerRequester = new PassengerRequester(getActivity());
            passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                @Override
                public void onMessageSuccess(String str) {
                    hideCircularBar();
                }

                @Override
                public void onMessageError(int errorCode) {
                    hideCircularBar();
                    Utils.showToast(getActivity(), DriverRequester.otherErrors(getActivity(), errorCode));
                }
            });

            getDataFromField();
            passengerRequester.savePassengerProfile(cookie, nickname, quantity_d, areaAndCity);
            showCircularBar();

    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, loadAvatarImageView, size);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });
        commonRequest.getOwnAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size);
    }

    private void showChangeRoleDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(getString(R.string.exit_change_role));
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                changeUser();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void changeUser() {
        final DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                int i =1;

            }

            @Override
            public void onMessageError(int errorCode) {
                int i =1;
            }
        });
        driverRequester.changeRole(cookie, Constants.PASSENGER_ROLE);
        sharedPref.edit().putString(Constants.ROLE_SATE, Constants.PASSENGER_ROLE).apply();

        if (PassengerFragmentActivity.class == getActivity().getClass()) {
            MainFragmentActivity.isLogOff(true);
            getActivity().finish();

        } else {
            PassengerFragmentActivity.isLogOff(true);
            getActivity().finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Constants.PICK_FROM_CAMERA) {
            File file = new File(Environment.getExternalStorageDirectory()+File.separator + "img.jpg");
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
            sendAvatar(bitmap, Constants.IMG_LARGE_SIZE);

            try {
                cropCapturedImage(Uri.fromFile(file));
            } catch(ActivityNotFoundException aNFE) {
                Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");
            }

        } else if (requestCode == Constants.PIC_CROP && data != null) {
            Bundle extras = data.getExtras();
            Bitmap bitmap = null;
            if (extras == null){
                Uri imageUri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                bitmap = extras.getParcelable("data");
            }
            loadAvatarImageView.setImageBitmap(Utils.getRoundedCornerBitmap(bitmap,
                    Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, getActivity())));;
            sendAvatar(bitmap, Constants.IMG_SMALL_SIZE);

        } else if (requestCode == Constants.PICK_FROM_GALLERY && data != null) {

            try {
                selectedImageURI = data.getData();
                File file = new File(Utils.getRealPathFromURI(getActivity(), selectedImageURI));

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageURI);
                sendAvatar(bitmap, Constants.IMG_LARGE_SIZE);
                cropCapturedImage(Uri.fromFile(file));

            } catch(ActivityNotFoundException aNFE) {
                Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");

            } catch (Exception e) {
                Utils.showAlertDialog(getActivity(), "Неподдерживаемый тип документа. Выберите изображение, пожалуйста.");
                e.printStackTrace();
            }
        }
    }

    private void getCities() {
        DriverRequester driverRequester = new DriverRequester(getActivity());
        driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject.length() != 0) {
                            JSONArray jsonArray = jsonObject.getJSONArray("cities");
                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsObject = jsonArray.getJSONObject(i);
                                City city = new City(jsObject);
                                listCityArray.add(city);

                                if (listCities.size() != 0 && !city.getCityAddress().equals(listCities.get(0)))
                                    listCities.add(city.getCityAddress());
                            }
                            citiesSpinnerAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
                Log.d("ta", "e" + errorCode);
            }
        });

        driverRequester.getCities(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""));
    }

    private void sendAvatar(final Bitmap bitmap, final String type) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        stringBase64Original = android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT);
        bitmap.recycle();
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageLoader.clearCache();
                    }
                }, 10000);
                imageLoader.clearCache();
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });
        commonRequest.setAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), type, stringBase64Original);
    }

    public void cropCapturedImage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 100);
        cropIntent.putExtra("outputY", 100);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, Constants.PIC_CROP);
    }

    private void hideCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).progressiveStop();
        circularProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showCircularBar() {
        ((CircularProgressDrawable)circularProgressBar.getIndeterminateDrawable()).start();
        circularProgressBar.setVisibility(View.VISIBLE);
    }

    private void setTextField() {
        nicknameProfileEditText.setText(nickname);
        passengerProfileTextView.setText(quantity_d);

        if (areaAndCity.equals("null"))
            citiesSpinner.setSelection(0);
    }

    private void getDataFromField() {
        nickname = nicknameProfileEditText.getText().toString();
        areaAndCity = citiesSpinner.getSelectedItem().toString();
        minCashe = listCityArray.get(citiesSpinner.getSelectedItemPosition()).getMincash();

        if (minCashe != null)
            sharedPreferences.edit().putString(Constants.MINCASHE, minCashe).apply();
    }

    private void getPrefData() {
        cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
        nickname = sharedPreferences.getString(Constants.NICKNAME, "");
        quantity_d = sharedPreferences.getString(Constants.QUANTITY_D, "0");
        areaAndCity = sharedPreferences.getString(Constants.AREA_AND_CITY, "null");

        if (quantity_d.length() == 0)
            quantity_d = "0";
    }

    private void initUI(View view) {
        circularProgressBar = (CircularProgressBar) view.findViewById(R.id.smoothCircleBar);
        changeUserButton = (Button)view.findViewById(R.id.buttonChangeUser);
        citiesSpinner = (Spinner)view.findViewById(R.id.spinnerCities);
        ratingProfileTextView = (TextView)view.findViewById(R.id.textViewRatingProfile);
        saveButton = (Button)view.findViewById(R.id.buttonSaveSettings);
        nicknameProfileEditText = (EditText)view.findViewById(R.id.editTextNicknameProfile);
        passengerProfileTextView = (TextView)view.findViewById(R.id.textViewPassengerProfile);
        loadAvatarImageView = (ImageView)view.findViewById(R.id.passProfileImageButton);
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
            return true;

        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            Utils.hideKeyboard(getActivity());
            view.clearFocus();
            return true;

        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            Utils.hideKeyboard(getActivity());
            view.clearFocus();
            return true;
        }

        return false;
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(getActivity());
                    nicknameProfileEditText.clearFocus();
                    saveButton.requestFocus();
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }
}