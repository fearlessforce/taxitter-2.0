package com.mobapply.taxitter.ui.passenger;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.cacheutils.ImageLoader;
import com.mobapply.taxitter.custom.OnSwipeTouchListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.CommonRequest;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

/**
 * Fragment that appears when there was a click of menuButton on actionBar
 */
public class PassengerMenuFragment extends Fragment {
    private final String FAVORITE_ADDRESS = "favorite_address";
    private final String PROFILE_FRAGMENT = "driver_profile_fragment";
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences, sharedPref;
    private Button chooseRoleButton;
    private Button favoriteButton;
    private Button profileButton;
    private Button hideAppButton;
    private Button closeAppButton;
    private TextView nicknameTextView;
    private String nickname;
    private ImageView userPhotoImageView;
    private String stringBase64Original;
    private Uri selectedImageURI;
    private ImageLoader imageLoader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_passenger, container, false);
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        sharedPref = getActivity().getSharedPreferences(getString(R.string.pref_role_key), 0);
        nickname = sharedPreferences.getString(Constants.NICKNAME, "");
        imageLoader = ImageLoader.getInstance();

        setupListener(view);
        initUI(view);
        getAvatarFromServer(Constants.IMG_SMALL_SIZE);
        nicknameTextView.setText(nickname);

        ((RelativeLayout)view.findViewById(R.id.relativeLayout)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        ((RelativeLayout)view.findViewById(R.id.relativeMenuBottom)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager.popBackStack();
                setActionBarYellow();
            }
        });

        chooseRoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage(getString(R.string.change_role));
                alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sharedPref.edit().putString(Constants.ROLE_SATE, Constants.CHANGE_ROLE_STATE).apply();

                        if (OrderWithMapActivity.class == getActivity().getClass() || ConfirmationActivity.class == getActivity().getClass()) {
                            PassengerFragmentActivity.isChangeRole(true);
                            getActivity().finish();

                        } else if (PassengerFragmentActivity.class == getActivity().getClass()) {
                            getActivity().finish();
                        }
                    }
                });

                alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager.popBackStack();
                PassengerProfileFragment passengerProfileFragment = new PassengerProfileFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.menu_container, passengerProfileFragment, PROFILE_FRAGMENT)
                        .addToBackStack(PROFILE_FRAGMENT)
                        .commit();

                setActionBarYellow();
            }
        });

        userPhotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View dialog_layout = inflater.inflate(R.layout.choose_foto_dialog, (ViewGroup) view1.findViewById(R.id.root));

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialog_layout)
                        .setCancelable(true);
                final AlertDialog alert = builder.create();
                alert.show();

                ((Button)dialog_layout.findViewById(R.id.buttonCamera)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
                    }
                });

                ((Button)dialog_layout.findViewById(R.id.buttonGallery)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alert.cancel();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.PICK_FROM_GALLERY);
                    }
                });
            }
        });

        hideAppButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager.popBackStack();
                setActionBarYellow();
                openHomeScreen();
            }
        });

        closeAppButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeApp();
            }
        });

        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.popBackStack();
                FavoriteAddressFragment favoriteAddressFragment = new FavoriteAddressFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.menu_container, favoriteAddressFragment, FAVORITE_ADDRESS)
                        .addToBackStack(FAVORITE_ADDRESS)
                        .commit();

                setActionBarYellow();
            }
        });

        return view;
    }

    private void closeApp() {
        Intent intent = new Intent(getActivity().getApplicationContext(), MainFragmentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.EXIT, true);
        startActivity(intent);
    }

    private void initUI(View view) {
        favoriteButton = (Button)view.findViewById(R.id.buttonMenuFavorite);
        chooseRoleButton = (Button)view.findViewById(R.id.buttonDriverPassengerRole);
        profileButton = (Button)view.findViewById(R.id.buttonMenuProfile);
        hideAppButton = (Button)view.findViewById(R.id.buttonMenuHide);
        closeAppButton = (Button)view.findViewById(R.id.buttonClose);
        nicknameTextView = (TextView)view.findViewById(R.id.textViewMenuUserName);
        userPhotoImageView = (ImageView)view.findViewById(R.id.imageViewMenuUserPhoto);
    }

    private void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
                @Override
                public void onSwipeTop() {
                    getFragmentManager().popBackStack();
                    setActionBarYellow();
                }
            });
        }

        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }

    private void openHomeScreen() {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        this.startActivity(i);
    }

    private void setActionBarYellow() {
        if (PassengerFragmentActivity.class == getActivity().getClass()) {
            ((PassengerFragmentActivity) getActivity()).setMenuVisible(false);
            ((PassengerFragmentActivity) getActivity()).setActionBarYellow();

        } else if (OrderWithMapActivity.class == getActivity().getClass()) {
            ((OrderWithMapActivity) getActivity()).setMenuVisible(false);
            ((OrderWithMapActivity) getActivity()).setActionBarYellow();

        } else if (ConfirmationActivity.class == getActivity().getClass()) {
            ((ConfirmationActivity) getActivity()).setMenuVisible(false);
            ((ConfirmationActivity) getActivity()).setActionBarYellow();
        }
    }

    private void getAvatarFromServer(final String size) {
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                if (!str.equals(Constants.OK)) {
                    try {
                        JSONObject jsonObject = new JSONObject(str);

                        if (jsonObject != null) {
                            String urlImage = jsonObject.getString("url");

                            if (urlImage.length() != 0) {
                                if (isVisible()) {
                                    if (size.equals(Constants.IMG_SMALL_SIZE)) {
                                        if (urlImage.length() != 0) {
                                            imageLoader.DisplayImage(urlImage, userPhotoImageView, size);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });
        commonRequest.getOwnAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), size);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == Constants.PICK_FROM_CAMERA) {
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);
                sendAvatar(bitmap, Constants.IMG_LARGE_SIZE);

                try {
                    cropCapturedImage(Uri.fromFile(file));

                } catch (ActivityNotFoundException aNFE) {
                    Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");
                }

            } else if (requestCode == Constants.PIC_CROP && data != null) {
                Bundle extras = data.getExtras();
                Bitmap bitmap = null;
                if (extras == null){
                    Uri imageUri = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    bitmap = extras.getParcelable("data");
                }

                userPhotoImageView.setImageBitmap(Utils.getRoundedCornerBitmap(bitmap,
                        Utils.calculateDpToPixel(Constants.DP_IMAGE_SIZE, getActivity())));
                sendAvatar(bitmap, Constants.IMG_SMALL_SIZE);

            } else if (requestCode == Constants.PICK_FROM_GALLERY && data != null) {

                try {
                    selectedImageURI = data.getData();
                    File file = new File(Utils.getRealPathFromURI(getActivity(), selectedImageURI));

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageURI);
                    sendAvatar(bitmap, Constants.IMG_LARGE_SIZE);
                    cropCapturedImage(Uri.fromFile(file));

                } catch (ActivityNotFoundException aNFE) {
                    Utils.showToast(getActivity(), "Sorry - your device doesn't support the crop action!");

                } catch (Exception e) {
                    Utils.showAlertDialog(getActivity(), "Неподдерживаемый тип документа. Выберите изображение, пожалуйста.");
                    e.printStackTrace();
                }
            }
    }

    private void sendAvatar(final Bitmap bitmap, final String type) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        stringBase64Original = android.util.Base64.encodeToString(byteArray, android.util.Base64.DEFAULT);
        bitmap.recycle();
        CommonRequest commonRequest = new CommonRequest(getActivity());
        commonRequest.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imageLoader.clearCache();
                    }
                }, 10000);
                imageLoader.clearCache();
            }

            @Override
            public void onMessageError(int errorCode) {
            }
        });
        commonRequest.setAvatar(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""), type, stringBase64Original);
    }

    public void cropCapturedImage(Uri picUri) {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");
        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 100);
        cropIntent.putExtra("outputY", 100);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, Constants.PIC_CROP);
    }
}