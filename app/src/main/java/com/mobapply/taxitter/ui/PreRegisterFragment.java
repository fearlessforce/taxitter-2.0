package com.mobapply.taxitter.ui;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.custom.ExtendedEditText;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.AuthenticationRequester;

public class PreRegisterFragment extends Fragment{
    private final String ENTER_CODE_TAG = "enter_code";
    private final int NO_NETWORK_CONNECTION = 1019;
    private final int PRE_REGISTER_ALREADY_HAVE_AN_RECORD = 1;
    private final int PRE_REGISTER_ALREADY_HAVE_SUCH_A_USER = 2;
    private final int FAILED_TO_CREATE_A_REQUEST = 11;
    private final int NOT_ALL_REQUIRED_FIELDS_ARE_FILLED = 12;
    private final int ERROR_JSON_FORMAT = 13;
    private final int PHONE_LENGTH = 27;
    private final int MAX_PHONE_LENGTH = 26;

    private FragmentManager fragmentManager;
    private InputFilter[] inputFilterArray;
    private ExtendedEditText phoneNumberEditText;
    private Button getCodeButton;
    private int maxLength;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pre_register_fragment, container, false);
        setupListener(view);
        fragmentManager = getFragmentManager();

        phoneNumberEditText = (ExtendedEditText)view.findViewById(R.id.editTextPhoneNumber);
        getCodeButton = (Button)view.findViewById(R.id.buttonGetCode);

        phoneNumberEditText.setText("+7 (_ _ _) _ _ _  _ _  _ _");
        phoneNumberEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        maxLength = MAX_PHONE_LENGTH;
        inputFilterArray = new InputFilter[1];
        inputFilterArray[0] = new InputFilter.LengthFilter(maxLength);

        phoneNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;

                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        clearKeyboardAndFocus();
                        break;

                    case EditorInfo.IME_ACTION_NEXT:
                        clearKeyboardAndFocus();
                        break;
                }
                return false;
            }
        });

        phoneNumberEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocused) {
                if (isFocused)
                    setSelecionForPhoneNumber();
            }
        });

        phoneNumberEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelecionForPhoneNumber();
            }
        });

        phoneNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (phoneNumberEditText.toString().contains("_")) {
                    maxLength = PHONE_LENGTH;
                } else {
                    maxLength = MAX_PHONE_LENGTH;
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!phoneNumberEditText.getText().toString().contains("_")) {
                    maxLength = MAX_PHONE_LENGTH;
                    setFilter();

                } else {
                    maxLength = PHONE_LENGTH;
                    setFilter();
                }

//              Replacement symbol on number
                int p = 0;
                if (s.length() >= PHONE_LENGTH) {
                    String edittext = s.toString();

                    while (p < s.length()) {
                        if (s.charAt(p) == '_') {
                            if (p == 9 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 2);

                            } else if (p == 12 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 1);

                            } else if (p == 16 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 2);

                            } else if (p == 21 && edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);
                                phoneNumberEditText.setSelection(p + 2);

                            } else if (edittext.substring(p, p + 1).equals("_")) {
                                s.delete(p, p + 1);

                                if (p != 26) {
                                    phoneNumberEditText.setSelection(p + 1);
                                }
                            }

                            if (phoneNumberEditText.getText().toString().contains("_"))
                                phoneNumberEditText.setSelection(phoneNumberEditText.getText().toString().indexOf("_"));

                            break;
                        } else {
                            p++;
                        }
                    }
                }
            }
        });

        phoneNumberEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

//              Replacement number on symbol if was KEYCODE_DEL
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    int selection = phoneNumberEditText.getSelectionEnd();

                    if (selection >= 5) {
                        String phoneNumber = phoneNumberEditText.getText().toString();
                        String str1 = phoneNumber.substring(0, selection - 1);
                        String subChar = phoneNumber.substring(selection - 1, selection);
                        String str2 = phoneNumber.substring(selection);

                        if (isDigit(subChar)) {
                            phoneNumberEditText.setText(str1 + "_" + str2);
                            phoneNumberEditText.setSelection(str1.length());

                        } else if (subChar.equals(" ")) {

                            if (isDigit(phoneNumber.substring(selection - 2, selection - 1))) {
                                phoneNumberEditText.setText(phoneNumber.substring(0, selection - 2) + "_ " + str2);
                                phoneNumberEditText.setSelection(phoneNumber.substring(0, selection - 2).length());

                            } else if (phoneNumber.substring(selection - 2, selection - 1).equals(" ")) {
                                phoneNumberEditText.setText(phoneNumber.substring(0, selection - 3) + "_  " + str2);
                                phoneNumberEditText.setSelection(phoneNumber.substring(0, selection - 3).length());

                            } else if (phoneNumber.substring(selection - 2, selection - 1).equals(")")) {
                                phoneNumberEditText.setText(phoneNumber.substring(0, selection - 3) + "_) " + str2);
                                phoneNumberEditText.setSelection(phoneNumber.substring(0, selection - 3).length());
                            }
                        }

                        if (phoneNumberEditText.getText().toString().contains("_")) {
                            maxLength = PHONE_LENGTH;
                            setFilter();
                        }
                    }

                    if (phoneNumberEditText.getText().toString().contains("_"))
                        phoneNumberEditText.setSelection(phoneNumberEditText.getText().toString().indexOf("_"));

                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    clearKeyboardAndFocus();
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    clearKeyboardAndFocus();
                    return true;
                }
                return false;
            }
        });

        getCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String phoneNumber = phoneNumberEditText.getText().toString();
                AuthenticationRequester authenticationRequester = new AuthenticationRequester(getActivity());
                authenticationRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                    @Override
                    public void onMessageSuccess(String str) {
                        Utils.hideProgressDialog();
                        RegisterFragment registerFragment = new RegisterFragment();
                        registerFragment.setPhoneNumber(phoneNumber);
                        fragmentManager.beginTransaction()
                                .add(R.id.container, registerFragment, ENTER_CODE_TAG)
                                .addToBackStack(Constants.REGISTER_STACK)
                                .commit();
                    }

                    @Override
                    public void onMessageError(int errorCode) {
                        checkPreRegistrationErrorCode(errorCode);
                    }
                });

                authenticationRequester.preRegister(getPhoneNumber(phoneNumber));
                Utils.showProgressDialog(getActivity(), getString(R.string.loading));
            }
        });

        return view;
    }

    private void setSelecionForPhoneNumber() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (phoneNumberEditText.getText().toString().contains("_"))
                    phoneNumberEditText.setSelection(phoneNumberEditText.getText().toString().indexOf("_"));
            }
        }, 200);
    }

    /**
     * Get phone number with trim all space and other symbols
     * @param stringFromEditText
     * @return phone number
     */
    private String getPhoneNumber(String stringFromEditText) {
        return stringFromEditText.replaceAll("[^a-zA-Z0-9]","");
    }

    private boolean isDigit(String subChar) {
        for (int i = 0; i < 10; i++) {
            if (subChar.equals(String.valueOf(i))){
                return true;
            }
        }
        return false;
    }

    private void checkPreRegistrationErrorCode(final int errorCode) {
        Utils.hideProgressDialog();
        if (isVisible()) {
            if (errorCode == PRE_REGISTER_ALREADY_HAVE_AN_RECORD) {
                Utils.showToast(getActivity(), getString(R.string.already_have_an_record_pre_register));

            } else if (errorCode == PRE_REGISTER_ALREADY_HAVE_SUCH_A_USER) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.need_authorization))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                EnterFragment enterFragment = new EnterFragment();
                                enterFragment.setPhone(phoneNumberEditText.getText().toString());
                                fragmentManager.beginTransaction()
                                        .replace(R.id.container, enterFragment, Constants.ENTER_TAG)
                                        .addToBackStack(Constants.ENTER_STACK)
                                        .commit();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            } else if (errorCode == FAILED_TO_CREATE_A_REQUEST) {
                Utils.showToast(getActivity(), getString(R.string.failed_to_create_a_request));

            } else {
                othersError(errorCode);
            }
        }
    }

    private void othersError(int errorCode) {
        if (errorCode == NOT_ALL_REQUIRED_FIELDS_ARE_FILLED) {
            Utils.showToast(getActivity(), getString(R.string.not_corect_phone_number));

        } else if (errorCode == ERROR_JSON_FORMAT) {
            Utils.showToast(getActivity(), getString(R.string.json_error));

        } else if (errorCode == NO_NETWORK_CONNECTION) {
            Utils.showToast(getActivity(), getString(R.string.no_network));

        } else {
            Utils.showToast(getActivity(), getString(R.string.code) + errorCode);
        }
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    clearKeyboardAndFocus();
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }

    private void clearKeyboardAndFocus() {
        Utils.hideKeyboard(getActivity());
        phoneNumberEditText.clearFocus();
        getCodeButton.requestFocus();
    }

    private void setFilter() {
        inputFilterArray = new InputFilter[1];
        inputFilterArray[0] = new InputFilter.LengthFilter(maxLength);
        phoneNumberEditText.setFilters(inputFilterArray);
    }
}