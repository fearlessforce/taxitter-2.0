package com.mobapply.taxitter.ui.driver;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.ListOrderArrayAdapter;
import com.mobapply.taxitter.interfaces.DetailsOrderModuleListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.Order;
import com.mobapply.taxitter.ui.MainFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.DriverRequester;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Fragment contain list of orders.
 */
public class ListOrdersFragment extends Fragment {
    private final String STATUS_OFFER_NEW_AND_TRADES = "1";
    private static boolean isNeedRefreshListOrders = false;

    private SharedPreferences sharedPreferences;
    private FragmentManager fragmentManager;
    private ListView listViewOrders;
    private ArrayList<Order> arrayList;
    private ListOrderArrayAdapter listOrderArrayAdapter;

    private boolean isCreated = false;
    private ArrayList<Order> arrayOrdersList;
    private SwipeRefreshLayout swipeContainer;
    private Handler handler;
    private Runnable runnable;
    private String cookie;
    private String city_d;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_order_with_refresh, container, false);
        fragmentManager = getFragmentManager();
        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.preference_file_key), 0);
        cookie = sharedPreferences.getString(Constants.ACCESS_TOKEN, "");
        city_d = sharedPreferences.getString(Constants.CITY_D, "");
        arrayOrdersList = new ArrayList<Order>();

        if (arrayList == null)
            arrayList = new ArrayList<Order>();

        listViewOrders = (ListView)view.findViewById(R.id.lvItems);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListOrders();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        listViewOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int itemNumber, long l) {
                if (!sharedPreferences.getBoolean(Constants.CURRENT_ORDER_ACCEPT, false)) {

                    if (!swipeContainer.isRefreshing()) {
                        Order item = arrayList.get(itemNumber);
                        ((DetailsOrderModuleListener) getActivity()).openDetailsOrder(item);
                    }

                } else {
                    final DriverRequester driverRequester = new DriverRequester(getActivity().getApplicationContext());
                    driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                        @Override
                        public void onMessageSuccess(String str) {
                            swipeContainer.setRefreshing(false);

                            if (!str.equals(Constants.OK)) {
                                try {
                                    JSONArray jsonArray = new JSONArray(str);

                                    if (jsonArray.length() != 0) {
                                        arrayOrdersList.clear();

                                        for (int n = 0; n < jsonArray.length(); n++) {
                                            JSONObject object = jsonArray.getJSONObject(n);
                                            Order order = new Order(object);
                                            arrayOrdersList.add(order);
                                        }

                                        if (arrayOrdersList.size() != 0)
                                            Utils.showAlertDialog(getActivity(), getString(R.string.complete_old_order));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onMessageError(int errorCode) {
                            if (isVisible() && (((DriverFragmentActivity) getActivity()).getTabPosition() == 0)) {
                                swipeContainer.setRefreshing(false);

                                if (errorCode == Constants.NO_NEW_ORDERS) {
                                    arrayOrdersList.clear();
                                    sharedPreferences.edit().remove(Constants.CURRENT_ORDER_ACCEPT).apply();
                                    Order item = arrayList.get(itemNumber);
                                    ((DetailsOrderModuleListener) getActivity()).openDetailsOrder(item);
                                }
                            }
                        }
                    });

                    driverRequester.getListOrders(cookie, ListOrdersOnTabTwoFragment.ORDERS_CONFIRM_AND_ACTIVELY, city_d);
                    swipeContainer.setRefreshing(true);
                }
            }
        });

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (isNeedRefreshListOrders) {
                    setTimeRefreshForList();
                    getListOrders();
                    isNeedRefreshListOrders = false;
                }
            }
        });

        return view;
    }

    private void setTimeRefreshForList() {
        final int delay = 60000;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isVisible() && ((DriverFragmentActivity)getActivity()).getTabPosition() == 0) {
                    getListOrders();
                    handler.postDelayed(this, delay);
                }
            }
        };
        handler.postDelayed(runnable, delay);
    }

    public static void setIsNeedRefresh(boolean b) {
        isNeedRefreshListOrders = b;
    }

    private void getListOrders() {
        if (fragmentManager.findFragmentById(R.id.fragments_container) == null
                && fragmentManager.findFragmentById(R.id.menu_container) == null) {
            final DriverRequester driverRequester = new DriverRequester(getActivity().getApplicationContext());
            driverRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                @Override
                public void onMessageSuccess(String str) {
                    swipeContainer.setRefreshing(false);

                    if (!str.equals(Constants.OK)) {
                        try {
                            JSONArray jsonArray = new JSONArray(str);

                            if (jsonArray.length() != 0) {
                                arrayList.clear();

                                for (int n = 0; n < jsonArray.length(); n++) {
                                    JSONObject object = jsonArray.getJSONObject(n);
                                    Order order = new Order(object);
                                    arrayList.add(order);
                                }
                            }

                            if (isVisible()) {
                                listOrderArrayAdapter = new ListOrderArrayAdapter(getActivity(), arrayList);
                                listViewOrders.setAdapter(listOrderArrayAdapter);
                                listOrderArrayAdapter.notifyDataSetChanged();
                                listViewOrders.invalidate();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onMessageError(int errorCode) {
                    if (isVisible() && (((DriverFragmentActivity) getActivity()).getTabPosition() == 0)) {
                        swipeContainer.setRefreshing(false);

                        if (errorCode == Constants.NOT_FIND_USER_WITH_ACCESS_TOKEN) {
                            MainFragmentActivity.isAccessTokenGone(true);
                            getActivity().finish();

                        } else if (errorCode == Constants.NO_NEW_ORDERS) {
                            arrayList.clear();

                            if (listOrderArrayAdapter != null)
                                listOrderArrayAdapter.notifyDataSetChanged();
                        }

                        if (getFragmentManager().findFragmentById(R.id.menu_container) == null)
                            if (DriverRequester.checkGetListStatementsErrorCode(getActivity(), errorCode).length() != 0)
                                Utils.showToast(getActivity(), DriverRequester.checkGetListStatementsErrorCode(getActivity(), errorCode));
                    }
                }
            });

            driverRequester.getListOrders(cookie, STATUS_OFFER_NEW_AND_TRADES, city_d);
            swipeContainer.setRefreshing(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setTimeRefreshForList();

        if (isCreated) {
            if (!(fragmentManager.findFragmentById(R.id.menu_container) instanceof DriverProfileFragment))
                getListOrders();
        }
        getListOrders();

        isCreated = true;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {

            if (isCreated) {
                setTimeRefreshForList();
                getListOrders();
            }
        } else {
            if (isCreated)
                handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isCreated = false;
    }
}