package com.mobapply.taxitter.ui.passenger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.mobapply.taxitter.R;
import com.mobapply.taxitter.adapters.PlacesAutoCompleteAdapter;
import com.mobapply.taxitter.interfaces.CountCoordinatesListener;
import com.mobapply.taxitter.interfaces.TransmissionResponseCode;
import com.mobapply.taxitter.model.FavoriteAddress;
import com.mobapply.taxitter.service.GPSTracker;
import com.mobapply.taxitter.ui.BaseActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.GetCoordinates;
import com.mobapply.taxitter.utils.Utils;
import com.mobapply.taxitter.webconnect.PassengerRequester;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import se.walkercrou.places.Place;

/**
 * Created by Misha Nester on 30.04.15.
 */
public class EditFavaddressActivity extends BaseActivity {
    private SharedPreferences sharedPreferences;
    private GPSTracker gps;
    private View mCustomView;
    private RelativeLayout entranceRelativeLayout;
    private TextView getMyLocation;
    private TextView streetTextView, houseTextView, entranceTextView, nameAddrTextView;
    private AutoCompleteTextView streetAutoCompleteText, entranceEditText, houseEditText, nameAddressEdiText;
    private ImageButton saveButton;
    private ImageButton deleteButton;
    private Button menuButton;
    private FavoriteAddress favaddress;

    private String halfAddress;
    private String fullAddress;
    private String longitude;
    private String latitude;
    private String id;
    private String name = "";
    private String street = "";
    private String house = "";
    private String door = "";
    private Place addressListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_favaddress_layout);
        sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), 0);

        ActionBar mActionBar = getActionBar();
        setupListener(findViewById(R.id.main));
        initUI();

        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.action_bar_custom, null);
        menuButton = (Button) mCustomView.findViewById(R.id.buttonMenu);
        menuButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_action_back));

        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            mActionBar.setCustomView(mCustomView);
            mActionBar.setDisplayShowCustomEnabled(true);
        }

        streetAutoCompleteText.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item));
        streetAutoCompleteText.setThreshold(2);

        final Intent intent = getIntent();

        if(intent != null) {
            favaddress = (FavoriteAddress) intent.getExtras().getParcelable(Constants.ADDRESS);

            if (favaddress != null) {
                getDataFromAddress();
                setTextIntoField();
            }
        }

        streetAutoCompleteText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                houseEditText.requestFocus();
            }
        });

        nameAddressEdiText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    nameAddressEdiText.clearFocus();
                    streetAutoCompleteText.requestFocus();
                    streetAutoCompleteText.setSelection(streetAutoCompleteText.getText().toString().length());
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    nameAddressEdiText.clearFocus();
                    return true;
                }

                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                return false;
            }
        });

        streetAutoCompleteText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    streetAutoCompleteText.clearFocus();
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    streetAutoCompleteText.clearFocus();
                    return true;
                }

                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                return false;
            }
        });

        houseEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    entranceEditText.requestFocus();
                    entranceEditText.setSelection(entranceEditText.getText().toString().length());
                    return true;

                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    houseEditText.clearFocus();
                    return true;
                }
                return false;
            }
        });

        entranceEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    entranceEditText.clearFocus();
                    return true;
                }
                return false;
            }
        });

        getMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.checkGPSConnection(EditFavaddressActivity.this)) {
                    Utils.gpsDialog(EditFavaddressActivity.this);

                } else {
                    gps = new GPSTracker(EditFavaddressActivity.this);
                    LatLng location = new LatLng(gps.getLatitude(), gps.getLongitude());

                    FragmentManager fragMan = getFragmentManager();
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();
                    OwnMapFragment ownMapFragment = new OwnMapFragment();
                    ownMapFragment.setLocation(location);

                    if (fragMan.findFragmentById(R.id.linerMap) == null )
                        fragTransaction.add(R.id.linerMap, ownMapFragment, Constants.OWN_MAP_FRAGMENT).commit();

                    GeoAsyncTask geoAsyncTask = new GeoAsyncTask(EditFavaddressActivity.this, String.valueOf(location.latitude),
                            String.valueOf(location.longitude));
                    geoAsyncTask.execute();
                }
            }
        });

    }

    private void setTextIntoField() {
        nameAddressEdiText.setText(favaddress.getName());
        streetAutoCompleteText.setText(favaddress.getStreet());
        houseEditText.setText(favaddress.getHouse_num());
        entranceEditText.setText(favaddress.getDoor());
    }

    private void getDataFromAddress() {
        id = favaddress.getId();
        name = favaddress.getName();
        street = favaddress.getStreet();
        house = favaddress.getHouse_num();
        door = favaddress.getDoor();
        longitude = favaddress.getLongitude();
        latitude = favaddress.getLatitude();
        halfAddress = favaddress.getHalfAddress();
        fullAddress = favaddress.getFullAddress();
    }

    private void deleteFavaddress() {
        getDataFromAddress();
        Utils.showProgressDialog(EditFavaddressActivity.this, getString(R.string.deleting));
        PassengerRequester passengerRequester = new PassengerRequester(EditFavaddressActivity.this);
        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
            @Override
            public void onMessageSuccess(String str) {
                Utils.hideProgressDialog();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }

            @Override
            public void onMessageError(int errorCode) {
                Utils.hideProgressDialog();
                Utils.showToast(EditFavaddressActivity.this, getString(R.string.error));
            }
        });
        passengerRequester.setFavoriteAddress(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                id, name, street, house, door, longitude, latitude, "1");
    }

    private void getDataFromField() {
        name = nameAddressEdiText.getText().toString().replace(",", "");
        street = streetAutoCompleteText.getText().toString().replace(",", "");
        house = houseEditText.getText().toString().replace(",", "");
        door = entranceEditText.getText().toString().replace(",", "");
        halfAddress = getHalfAddress();
        fullAddress = Constants.PROFILE_COUNTRY
                            + sharedPreferences.getString(Constants.AREA_AND_CITY, "")
                            + ", " + getHalfAddress();
    }

    private String getHalfAddress() {
        String halfAddress;
        if (house.equals("")) {
            halfAddress = street;

        } else if (!door.equals("")) {
            halfAddress = street + ", " + house + ", " + door;

        } else {
            halfAddress = street + ", " + house;
        }

        return halfAddress;
    }

    public void onNameAddrClick(View view) {
        setFocusForView(nameAddressEdiText);
    }

    public void onStreetClick(View view) {
        setFocusForView(streetAutoCompleteText);
    }

    public void onHouseClick(View view) {
        setFocusForView(houseEditText);
    }

    public void onEntranceClick(View view) {
        setFocusForView(entranceEditText);
    }

    public void onDeleteAddressClick(View view) {
        deleteFavaddress();
    }

    public void onSaveAddressClick(View view) {
        getDataFromField();
        if (street.equals("")) {
            Utils.showAlertDialog(EditFavaddressActivity.this, getString(R.string.not_all_fields));

        } else if (name.equals("")) {
            Utils.showAlertDialog(EditFavaddressActivity.this, getString(R.string.name_for_address_null));

        } else if (!name.equals(favaddress.getName()) || !getHalfAddress().equals(favaddress.getHalfAddress())) {
            GetCoordinates getCoordinates = new GetCoordinates(EditFavaddressActivity.this);
            getCoordinates.setTransmissionMessage(new CountCoordinatesListener() {
                @Override
                public void onCountCoordinatesListener(final Place addresses) {
                    if (addresses != null) {
                        addressListener = addresses;
                        PassengerRequester passengerRequester = new PassengerRequester(EditFavaddressActivity.this);
                        passengerRequester.setTransmissionResponseCode(new TransmissionResponseCode() {
                            @Override
                            public void onMessageSuccess(String str) {
                                Utils.hideProgressDialog();
                                favaddress.setName(name);
                                favaddress.setStreet(street);
                                favaddress.setHouse_num(house);
                                favaddress.setDoor(door);
                                favaddress.setLongitude(String.valueOf(addresses.getLongitude()));
                                favaddress.setLatitude(String.valueOf(addresses.getLatitude()));

                                Intent intent = new Intent();
                                intent.putExtra(Constants.ADDRESS, favaddress);
                                setResult(RESULT_OK, intent);
                                finish();
                            }

                            @Override
                            public void onMessageError(int errorCode) {
                                Utils.hideProgressDialog();
                            }
                        });
                        passengerRequester.setFavoriteAddress(sharedPreferences.getString(Constants.ACCESS_TOKEN, ""),
                                id, name, street, house, door, String.valueOf(addresses.getLongitude()),
                                String.valueOf(addresses.getLatitude()), "");
                    } else {
                        Utils.showAlertDialog(EditFavaddressActivity.this, getString(R.string.address_find_error));
                    }
                }
            });

            getCoordinates.execute(Constants.PROFILE_COUNTRY
                    + sharedPreferences.getString(Constants.AREA_AND_CITY, "")
                    + ", " + getHalfAddress());
            Utils.showProgressDialog(EditFavaddressActivity.this, getString(R.string.saving));

        } else {
            finish();
        }
    }

    private void setTimer() {
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (addressListener == null) {
                    Log.d("TAG", "NULL");
                }
            }

            public void onFinish() {
                if (addressListener == null) {
                    Utils.hideProgressDialog();
                    Utils.showToast(EditFavaddressActivity.this, getString(R.string.address_find_error));
                }
            }
        }.start();
}

    private void setFocusForView(AutoCompleteTextView editView) {
        editView.requestFocus();
        editView.setSelection(editView.getText().length());
        Utils.showKeyboard(EditFavaddressActivity.this, editView);
    }

    public class GeoAsyncTask extends AsyncTask<String, String, Integer> {
        private Context context;
        private String latitude;
        private String longitude;
        boolean isConnectionAvailable;
        String responseText = null;

        public GeoAsyncTask(Context context, String latitude, String longitude) {
            super();
            this.context = context;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public GeoAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isConnectionAvailable = Utils.isConnectionAvailable(context);
        }

        @Override
        protected Integer doInBackground(String... urls) {
            if (isConnectionAvailable) {
                StringBuilder sb = new StringBuilder(Constants.MAPS_GEOCODE);

                sb.append("sensor=false");
                sb.append("&types=street_address");
                sb.append("&latlng=").append(latitude).append(",").append(longitude);
                sb.append("&language=ru");

                try {
                    DefaultHttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(sb.toString());
                    HttpResponse httpresponse = httpclient.execute(httppost);
                    HttpEntity httpentity = httpresponse.getEntity();
                    InputStream is = httpentity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sba = new StringBuilder();
                    String line = null;

                    while((line = reader.readLine()) != null) {
                        sba.append(line + "\n");
                    }
                    is.close();
                    responseText = sba.toString();
                }
                catch(ClientProtocolException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer res) {
            super.onPostExecute(res);
            try {
                if (responseText != null) {
                    JSONObject object = new JSONObject(responseText);

                    if (object.has("results")) {
                        JSONArray jsonArrayResults = object.getJSONArray("results");
                        if (jsonArrayResults.length() != 0) {
                            JSONObject objectResult = jsonArrayResults.getJSONObject(0);

                            if (objectResult.has("address_components")) {
                                JSONArray jsonArrayAddrComponents = objectResult.getJSONArray("address_components");
                                JSONObject objectHouse = jsonArrayAddrComponents.getJSONObject(0);
                                String house = objectHouse.getString("long_name");

                                JSONObject objectStreet = jsonArrayAddrComponents.getJSONObject(1);
                                String street = objectStreet.getString("long_name");

                                if (street.length() == 0 && house.length() == 0) {
                                    Utils.showAlertDialog(EditFavaddressActivity.this, getString(R.string.address_filling_error));
                                } else {
                                    streetAutoCompleteText.setText(street);
                                    houseEditText.setText(house);
                                }
                            }
                        } else {
                            Utils.showAlertDialog(EditFavaddressActivity.this, getString(R.string.address_filling_error));
                        }
                    }
                } else {
                    Utils.showAlertDialog(EditFavaddressActivity.this, getString(R.string.address_filling_error));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initUI() {
        nameAddressEdiText = (AutoCompleteTextView)findViewById(R.id.nameAddrEditText);
        getMyLocation = (TextView)findViewById(R.id.getMyLocationTextView);
        streetAutoCompleteText = (AutoCompleteTextView)findViewById(R.id.streetAutoCompleteText);
        entranceEditText = (AutoCompleteTextView)findViewById(R.id.entranceEditText);
        houseEditText = (AutoCompleteTextView)findViewById(R.id.housEditText);
        saveButton = (ImageButton)findViewById(R.id.saveBtn);
        deleteButton = (ImageButton)findViewById(R.id.deleteBtn);
        entranceRelativeLayout = (RelativeLayout)findViewById(R.id.entranceRelativeLayout);
    }

    public void setupListener(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideKeyboard(EditFavaddressActivity.this);
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupListener(innerView);
            }
        }
    }
}