package com.mobapply.taxitter.ui;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.mobapply.taxitter.R;
import com.mobapply.taxitter.ui.driver.DriverFragmentActivity;
import com.mobapply.taxitter.ui.passenger.PassengerFragmentActivity;
import com.mobapply.taxitter.utils.Constants;
import com.mobapply.taxitter.utils.Utils;

/**
 * Screen branching parts of the program, passenger or driver.
 */
public class ChoiceOfRolesFragment extends Fragment {
    private SharedPreferences sharedPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choice_role_fragment, container, false);
        sharedPref = getActivity().getSharedPreferences(getString(R.string.pref_role_key), 0);

        if (!Utils.checkGPSConnection(getActivity())) {
            Utils.gpsDialog(getActivity());
        }

        ((RelativeLayout)view.findViewById(R.id.rel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        ((Button)view.findViewById(R.id.buttonDriver)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.checkGPSConnection(getActivity())) {
                    Utils.gpsDialog(getActivity());

                } else {
                    if (Utils.isConnectionAvailable(getActivity())) {
                        setNextModuleActivity(DriverFragmentActivity.class, Constants.PASSENGER_ROLE);

                    } else {
                        Utils.showToast(getActivity(), getString(R.string.no_network));
                    }
                }
            }
        });

        ((Button)view.findViewById(R.id.buttonPassenger)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utils.checkGPSConnection(getActivity())) {
                    Utils.gpsDialog(getActivity());

                } else {
                    if (Utils.isConnectionAvailable(getActivity())) {
                        setNextModuleActivity(PassengerFragmentActivity.class, Constants.DRIVER_ROLE);

                    } else {
                        Utils.showToast(getActivity(), getString(R.string.no_network));
                    }
                }
            }
        });

        MainFragmentActivity.isAccessTokenGone(false);
        MainFragmentActivity.isLogOff(false);

        return view;
    }

    private void setNextModuleActivity(final Class activityClass, String role) {
        if (sharedPref.getString(Constants.ROLE_SATE, "").equals(Constants.FIRST_ENTER) ||
                sharedPref.getString(Constants.ROLE_SATE, "").equals(role) ||
                sharedPref.getString(Constants.ROLE_SATE, "").equals(Constants.CHANGE_ROLE_STATE)) {
            startActivity(new Intent(getActivity(), activityClass));

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage(getString(R.string.change_role));
            alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    startActivity(new Intent(getActivity(), activityClass));
                }
            });

            alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }
}